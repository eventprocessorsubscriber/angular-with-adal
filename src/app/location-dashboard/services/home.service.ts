import { AdalService } from "src/app/shared/services/adal.service";
import { HttpClient } from "@angular/common/http";
import { APP_CONFIG, AppConfig } from "../../app.config";
import { Injectable, Inject } from "@angular/core";
import { Observable } from "rxjs";
import { HttpHeaders } from "@angular/common/http";
import * as uuid from 'uuid';

let httpClient: any;
@Injectable()
export class HomeService {
  constructor(
    private httpClient: HttpClient,
    @Inject(APP_CONFIG) public config: AppConfig,
    public adalService: AdalService
  ) {}

  public newGuid() {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
      var r = (Math.random() * 16) | 0,
        v = c == "x" ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    });
  }

  GetSessionToken(item: any): Observable<any> | any {
    let customHeaders: HttpHeaders = new HttpHeaders();
    customHeaders = customHeaders.append(
      "Content-Type",
      "application/json; charset=utf-8"
    );

    var location = {
      LocationId: item,
      AirlineApplicationCode: "gjk"
    };
    return this.httpClient.post<any>(
      "https://sita-bda-eastus-app-deviceservice.azurewebsites.net/api/v3/session/start/terminal",
      location,
      { headers: customHeaders }
    );
  }

  GetCapabilities(sessionToken: any, id: string) {
    let customHeaders: HttpHeaders = new HttpHeaders();
    customHeaders = customHeaders.append(
      "Content-Type",
      "application/json; charset=utf-8"
    );
    var body = {
      LocationId: id,
      AirlineApplicationCode: "gjk",
      sessionToken: sessionToken,
      //"WebhookUrl": "https://rbabdawebhook.azurewebsites.net/api/webhook1?code=hgUA1HLJi4abwCRr/HsEOyAFwwKLBo/JOPJw1h4DsnyKU5TXF/qnyQ=="
      WebhookUrl: "https://howtoappwebhook.azurewebsites.net/api/message"
    };
    return this.httpClient.post<any>(
      "https://sita-bda-eastus-app-deviceservice.azurewebsites.net/api/v1/location/subscribe",
      body,
      { headers: customHeaders }
    );
  }

  GetAccessToken() {
    return this.adalService.acquireTestTokenResilient(this.config.resource);
  }

  Release(sessionToken: any, locationId: any) {
    let customHeaders: HttpHeaders = new HttpHeaders();
    customHeaders = customHeaders.append("Content-Type", "application/json");
    var body = {
      Locator: locationId,
      SessionToken: sessionToken
    };
    return this.httpClient.post<any>(
      "https://sita-bda-eastus-app-deviceservice.azurewebsites.net/api/v1/peripheral/release ",
      JSON.stringify(body),
      { headers: customHeaders }
    );
  }
  
    ReleaseAll(sessionToken: any)
    {
      console.log("in DoAcquire" + sessionToken);
      let customHeaders: HttpHeaders = new HttpHeaders();
      customHeaders = customHeaders.append('Content-Type', 'application/json');   
      var body = {      
        "SessionToken": sessionToken       
      }
      return this.httpClient.post<any>('https://sita-bda-eastus-app-deviceservice.azurewebsites.net/api/v1/peripheral/release', JSON.stringify(body), { headers: customHeaders });

    }

    EnableExclusive(sessionToken: any, locationId: any,ScanCapabilities:any)
    {   
      let customHeaders: HttpHeaders = new HttpHeaders();
      var myTransactionId = uuid.v4();
      customHeaders = customHeaders.append('Content-Type', 'application/json');   
      var body = {
        "Locator": locationId,
        "TransactionId":myTransactionId,
        "SessionToken": sessionToken,
        "exclusiveAccess":true       
      }
      return this.httpClient.post<any>(this.GenerateEnableAPIURL(locationId, ScanCapabilities), JSON.stringify(body), { headers: customHeaders });
  
    }

    EnableShared(sessionToken: any, locationId: any, ScanCapabilities:any)
    {   
      let customHeaders: HttpHeaders = new HttpHeaders();
      var  myTransactionId = uuid.v4();
      customHeaders = customHeaders.append('Content-Type', 'application/json');   
      var body = {
        "Locator": locationId,
        "TransactionId":myTransactionId,
        "SessionToken": sessionToken,
        "exclusiveAccess":false       
      }
      return this.httpClient.post<any>(this.GenerateEnableAPIURL(locationId, ScanCapabilities), JSON.stringify(body), { headers: customHeaders });
  
    }

  Disable(sessionToken: any, locationId: any) {
    let customHeaders: HttpHeaders = new HttpHeaders();
    var myTransactionId = uuid.v4();
    customHeaders = customHeaders.append('Content-Type', 'application/json');

    var body = {
      "Locator": locationId,
      "TransactionId": myTransactionId,
      "SessionToken": sessionToken,
      "release": true
    }

    return this.httpClient.post<any>("https://sita-bda-eastus-app-deviceservice.azurewebsites.net/api/v1/peripheral/disable", JSON.stringify(body), { headers: customHeaders });
  }

  DoAcquire(sessionToken: any, locationId: any, accessType: number) {
    console.log("in DoAcquire" + sessionToken);
    let customHeaders: HttpHeaders = new HttpHeaders();
    customHeaders = customHeaders.append("Content-Type", "application/json");
    var body = {
      LocatorList: locationId,
      SessionToken: sessionToken,
      accessType: accessType
    };

    return this.httpClient.post<any>(
      "https://sita-bda-eastus-app-deviceservice.azurewebsites.net/api/v1/peripheral/acquire",
      JSON.stringify(body),
      { headers: customHeaders }
    );
  }

  Print(
    sessionToken: any,
    deviceId: any,
    documentType: any,
    payLoadType: any,
    payLoadBase64: any
  ) {
    console.log("Print" + sessionToken);
    let customHeaders: HttpHeaders = new HttpHeaders();
    customHeaders = customHeaders.append("Content-Type", "application/json");
    var body = {
      Locator: deviceId,
      SessionToken: sessionToken,
      RequestId: this.newGuid(),
      TransactionId: this.newGuid(),
      payloadBase64: payLoadBase64
    };

    return this.httpClient.post<any>(
      `https://sita-bda-eastus-app-deviceservice.azurewebsites.net/api/v1/peripheral/print/${documentType}/${payLoadType}`,
      JSON.stringify(body),
      { headers: customHeaders }
    );
  }

  GenerateEnableAPIURL(locator:any, scanCapabilities: any){
  var Url:string;
  var ApiName:string;
  for (let device of scanCapabilities) {
    if(device.locator==locator){
      ApiName = device.EnableAPITag;
    } 
   }
  Url =   "https://sita-bda-eastus-app-deviceservice.azurewebsites.net/api/v1/peripheral/enable/"+ApiName;
return Url;  
}
 
}
