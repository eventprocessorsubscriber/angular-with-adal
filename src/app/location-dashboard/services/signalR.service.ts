import { APP_CONFIG, AppConfig } from "../../app.config";
import { Injectable, Inject } from "@angular/core";
import { Observable, of, Subject } from "rxjs";
import { HubConnection, HubConnectionBuilder } from "@aspnet/signalr";
import { SignalRMessage } from "src/app/Models/signalRMessage";

const WAIT_UNTIL_ASPNETCORE_IS_READY_DELAY_IN_MS = 2000;

@Injectable()
export class SignalRService {
  signalRData = new Subject<SignalRMessage>();
  isConnectionEstablished = new Subject<Boolean>();
  private hubConnection: HubConnection;

  constructor(
    // @Inject(APP_CONFIG) public config: AppConfig
  ) {
    this.createConnection();
    this.registerOnMessageEvent();
    this.startConnection();
  }

  /**
  * Builds the SignalR hubConnection
  */
  private createConnection() {
    this.hubConnection = new HubConnectionBuilder()
      .withUrl("https://howtoappwebhook.azurewebsites.net/messagehub")
      .build();
  }

  /**
  * Establishes the SignalR hubConnection
  */
  private startConnection() {
    setTimeout(() => {
      this.hubConnection
        .start()
        .then(() => {
          console.log('SignalR hub connection established @service');
          this.isConnectionEstablished.next(true);
        })
        .catch((err) => console.error(err.toString()))
    }, WAIT_UNTIL_ASPNETCORE_IS_READY_DELAY_IN_MS);
  }

  /**
  * registers the event listener for signalR messages
  */
  private registerOnMessageEvent() {
    this.hubConnection.on('send', (response: SignalRMessage) => {
      console.log("SignalR raw data: ");
      console.log(response);
      this.signalRData.next(response);
    });
  }
}