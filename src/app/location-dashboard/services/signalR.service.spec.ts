import { APP_CONFIG, AppConfig } from "../../app.config";
// import { Injectable, Inject } from "@angular/core";
// import { Observable, of, Subject } from "rxjs";
// import { HubConnection, HubConnectionBuilder } from "@aspnet/signalr";
// import { SignalRMessage } from "src/app/Models/signalRMessage";
import { TestBed, inject } from '@angular/core/testing';
import { SignalRService } from './signalR.service';

describe('SignalRService', () => {

  describe('Manually instantiating service', () => {
    let service: SignalRService;
    // @Inject(APP_CONFIG) config: AppConfig;

    beforeEach(() => {
      service = new SignalRService();
    });

    afterEach(() => {
      service = null;
    });

    it('should have a service instance', () => {
      expect(service).toBeDefined();
    });

    it('should return the signalR connection status to be true', () => {
      service.isConnectionEstablished.subscribe((data: Boolean) => {
        expect(data).toBe(true);
      });
    });


    // it('should return the json', () => {
    //   service.fetchViaHttp().subscribe(data => {
    //     expect(data.name).toBe('Juri');
    //   });

    //   const req = httpMock.expectOne('/someendpoint/people.json', 'call to api');
    //   expect(req.request.method).toBe('GET');

    //   req.flush({
    //     name: 'Juri'
    //   });
    // });
  });

});