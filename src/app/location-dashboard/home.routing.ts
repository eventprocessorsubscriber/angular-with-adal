import { RouterModule } from "@angular/router";
import { HomeComponent } from "./components/home.component";
import { AdalAccessGuard } from "../shared/guards/adal-access.guard";

export const routes = RouterModule.forChild([{
  path: "locationId",
  component: HomeComponent,
  canActivate: [AdalAccessGuard]
}]);
