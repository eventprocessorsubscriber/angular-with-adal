import { HomeService } from './../services/home.service';
import { Component, ViewChild } from "@angular/core";
import { Capabilities } from 'src/app/Models/capabilities';
import { ModalManager } from 'ngb-modal';
import { Router, ActivatedRoute } from "@angular/router";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import * as signalR from "@aspnet/signalr";
import { debug } from "util";
import { SignalRService } from '../services/signalR.service';
import { SignalRMessage } from 'src/app/Models/signalRMessage';
import { empty } from 'rxjs';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent {
  @ViewChild('myModal') myModal;
  private modalRef;

  printCapabilities: Capabilities[] = [];
  scanCapabilities: Capabilities[] = [];
  currentCapabilities: Capabilities[] = [];
  status: any = [];
  checkedList: any = [];
  printFilterArgs = ["Printer"];
  scanFilterArgs = ["Scanner", "Reader"];

  selectedTab: string = "print";
  locationid: string;
  deviceStatus: string;
  sessionToken: string;
  errorDetail: string;
  releasedDevices: any = [];
  // AcquiredDevice: any=[];
  connection: signalR.HubConnection;
  // signalRData: any = { PayloadType: "" };
  closeResult: string;
  data: string[] = [];
  displayPrintDiv: string;
  noOfPrints: any;
  deviceId: any;
  scanedData: any = {};

  deviceMappingData: any = {
    ATB1: {
      payLoadType: "pdf",
      documentType: "BoardingPass"
    },
    BTP1: {
      payLoadType: "pdf",
      documentType: "BagTag"
    },
    DCP1: {
      payLoadType: "text",
      documentType: "BoardingReceipt"
    },
    LSR1: {
      payLoadType: "text",
      documentType: "ReadBarCode"
    },
    MSR1: {
      payLoadType: "text",
      documentType: "ReadMagStripe"
    },
    "BGR1.BGR1": {
      payLoadType: "text",
      documentType: "Gate"
    },
    OCR1: {
      payLoadType: "text",
      documentType: "ReadTravelDocument"
    }
  };
  statusMessage: string = "";
  printType: string = "";
  printStatus: number = 0;

  constructor(
    private DashBoardService: HomeService,
    private route: ActivatedRoute,
    private modal: ModalManager,
    private signalRService: SignalRService
  ) {
    this.route.queryParams.subscribe(params => {
      this.locationid = params["id"];
      console.log("location fetched " + this.locationid);
    })

    this.GetSessionToken(this.locationid);
  }

  ngOnInit() {
    // first: subscribe to messges 
    this.subscribeToMessages();
    // then connect to SignalR
    this.signalRService.isConnectionEstablished.subscribe(() => {
      console.log("SignalR connection is Active @Component");
    });
  }

  GetPrintResponseMessgae(printType: string): string {
    switch (printType) {
      case "DCP":
        return "Print OK (Text)";
      default:
        return "Print OK (PDF)";
    }
  }


  private subscribeToMessages(): void {
    this.signalRService.signalRData.subscribe((response: SignalRMessage) => {
      // if signalR message is srigified JSON, then parse it as JSON object
      if (typeof (response) === "string") {
        response = JSON.parse(response);
      }
      var payload = JSON.parse(response.PayloadAsJson);
      console.log("Payload:");
      console.log(payload);

      if (response.PayloadType == "HandshakeMessage") {
        console.log("SignalR Handshake success");
      }
      // Print success/Error message processing
      if (response.PayloadType === "PrintOKMessage") {
        this.statusMessage = this.GetPrintResponseMessgae(this.printType);
        this.printStatus = 1;
      } else if (response.PayloadType === "ErrorMessage" && payload.ErrorCode === 'NGPAG2001') {
        this.statusMessage = "Print Failed!";
        this.printStatus = 2;
      }
      // Shows the status onclick of device 
      else if (response.PayloadType === "StatusMessage" && payload.DeviceStatusList) {
        let capabilities: Capabilities[] = [];
        capabilities = this.selectedTab === 'print' ? this.printCapabilities : this.scanCapabilities;

        payload.DeviceStatusList.forEach(element => {
          console.log(element);
          if (capabilities) {
            capabilities.forEach(capElement => {
              if (element.Locator == capElement.locator) {
                if (element.Status == "Online") capElement.status = "Online";
                else capElement.status = "PoweredOff";
              }
            });
          }
        });
      } else if (response.PayloadType == "BarCodeMessage") {
        if (payload.AEAData === null)
          payload.AEAData = "null"
        this.scanedData = payload;
      }
    });
  }

  openModal() {
    //this.modalRef = this.modal.open(this.myModal, {
    //  windowClass: 'my-class',
    //  size: "md",
    //  modalClass: 'mymodal',
    //  hideCloseButton: false,
    //  centered: false,
    //  backdrop: true,
    //  animation: false,
    //  keyboard: false,
    //  closeOnOutsideClick: false,
    //  backdropClass: "modal-backdrop"
    //})
  }
  closeModal() {
    this.modal.close(this.modalRef);
    //or this.modalRef.close();
  }

  public Disable(location: any) {
    for (let device of location) {
      console.log("release to" + device);
      this.DashBoardService.Disable(this.sessionToken, device).subscribe(res1 => {
        console.log(res1);
        if (res1 === null) {

          // this.releasedDevices.push(device);
          // this.UpdateStatus(this.releasedDevices,-1);
          this.errorDetail = "Successfully disabled selected devices";
        }
        console.log(this.releasedDevices);
      })
    }
    this.checkedList = [];
  }

  private filteDeviceCapabilities(capabilities: Capabilities[], filters: string[]): Capabilities[] {
    let printDevices: Capabilities[] = [];

    printDevices = capabilities.filter(
      (item) => {
        let isMatch: boolean = false;
        filters.forEach(filter => {
          isMatch = isMatch || (item.logicalType.indexOf(filter) !== -1);
        });
        return isMatch;
      }
    );
    return printDevices;
  }

  public GetSessionToken(locationID: string) {
    this.DashBoardService.GetSessionToken(locationID)
      .subscribe(res => {
        if (res) {
          console.log(res);
          console.log(res.sessionToken);
          this.sessionToken = res.sessionToken;
          this.DashBoardService.GetCapabilities(
            res.sessionToken,
            locationID
          ).subscribe(result => {
            if (result) {

              this.printCapabilities = this.filteDeviceCapabilities(
                result.locationCapabilityList,
                this.printFilterArgs
              );
              this.scanCapabilities = this.filteDeviceCapabilities(
                result.locationCapabilityList,
                this.scanFilterArgs
              );

              this.UpdateDeviceDefaultStatus(
                this.printCapabilities,
                this.scanCapabilities
              );
              console.log(result);
              console.log(this.printCapabilities);
              console.log(this.scanCapabilities);
            }
          });
        } else {
          console.log("Error occured");
        }
      },
        error => { }
      );
  }

  onTabChange(e) {
    this.displayPrintDiv = null;
    this.checkedList = [];
    this.ResetCheckBox();
    this.selectedTab = e.tabTitle == "PRINT CAPABILITIES" ? "print" : "scan";
  }

  change(e, type) {
    this.displayPrintDiv = null;
    this.RefreshModel();
    if (e.target.checked) {
      type.checked = true;
      this.checkedList.push(type.locator);
    } else {
      let index = this.checkedList.indexOf(type.locator);
      this.checkedList.splice(index, 1);
      // clearing the curresponding print section
      this.displayPrintDiv = '';
    }
    let capabilities: Capabilities[] = [];
    capabilities = this.selectedTab === 'print' ? this.printCapabilities : this.scanCapabilities;

    var checkedIndex = capabilities.findIndex(
      x => x.locator == type.locator
    );
    console.log("checked Item Index: ", checkedIndex);
    capabilities[checkedIndex].checked = e.target.checked;
    console.log(this.checkedList);
  }

  onClickAcquireStandard(selectedCheckList: any) {
    if (selectedCheckList.length > 0) {
      this.RefreshModel();
      this.doAcquire(this.checkedList, 1);
      this.ResetCheckBox();
    } else {
      this.errorDetail = "No Device Selected";
    }
  }
  onClickAcquireExclusive(selectedCheckList: any) {
    if (selectedCheckList.length > 0) {
      this.RefreshModel();
      this.doAcquire(this.checkedList, 0);
      this.ResetCheckBox();
    } else {
      this.errorDetail = "No Device Selected";
    }
  }
  onClickRelease(selectedCheckList: any) {
    if (selectedCheckList.length > 0) {
      this.RefreshModel();
      this.Release(this.checkedList);
      this.ResetCheckBox();
    } else {
      this.errorDetail = "No Device Selected";
    }
  }

  onClickReleaseAll(selectedCheckList: any) {
    this.RefreshModel();
    this.ReleaseAll();
    this.ResetCheckBox();
  }

  onClickEnable(selectedCheckList: any) {
    if (selectedCheckList.length > 1) {
      this.errorDetail = "Select one device at a time to enable";
      return;
    }
    this.RefreshModel();
    this.EnableExclusive(this.checkedList);
    this.ResetCheckBox();
  }

  onClickEnableShared(selectedCheckList: any) {
    if (selectedCheckList.length > 1) {
      this.errorDetail = "Select one device at a time to enable";
      return;
    }
    this.RefreshModel();
    this.EnableShared(this.checkedList);
    this.ResetCheckBox();
  }

  onClickDisable(selectedCheckList: any) {
    if (selectedCheckList.length > 1) {
      this.errorDetail = "Select one device at a time to enable";
      return;
    }
    this.RefreshModel();
    this.Disable(this.checkedList);
    this.ResetCheckBox();
  }

  onClickPrint(selectedCheckList: any) {

    if (this.checkedList.length == 0) {
      this.errorDetail = "Please select atleast one device";
      return;
    }

    if (this.checkedList.length > 1) {
      this.errorDetail = "Select only one Device";
      return;
    }

    // splits selected capability with dot(.) and return the last part after dot
    let deviceType = this.checkedList[0].split(".").pop();
    // let deviceType = this.checkedList[0].slice(
    //   this.checkedList[0].indexOf(".") + 1,
    //   this.checkedList[0].length
    // );
    console.log("device type" + deviceType);
    this.displayPrintDiv = this.deviceMappingData[deviceType].documentType;
    console.log(this.displayPrintDiv);
    this.RefreshModel();
    // this.checkedList = [];
    // this.ResetCheckBox();
  }

  findIndexToUpdate(type) {
    return type.isChecked === this;
  }

  public EnableShared(location: any) {
    console.log("enable exclusive to" + location);
    for (let device of location) {
      this.DashBoardService.EnableShared(this.sessionToken, device, this.scanCapabilities).subscribe(res1 => {
        console.log("enable exclusive to" + device);
        console.log("enable exclusive seesion" + this.sessionToken);
        console.log("response = " + res1);
        if (res1 === null) {
          this.errorDetail = "Selected device  enabled successfully!";
        } else
          if (res1.errorCode === null) {
            this.errorDetail = "Internal error occured!";
          }
          else if (res1.errorCode === "NGPAG0004") {
            this.UpdateStatus(this.releasedDevices, -1);
            this.errorDetail = "This device is unavailable";
          }
          else {
            console.log("In response Else Condition");
            this.errorDetail = res1.userMessage;
          }
      },
        error => {
          if (error.errorCode === "NGPAG0004") {
            console.log("In if Condition");
            this.errorDetail = "This device is unavailable";

          }
          else if (error.exceptionMessage != null && error.exceptionMessage != "undefined") {
            console.log("In if Else Condition");
            this.errorDetail = error.exceptionMessage;

          }
          else {
            // this.UpdateStatus(this.releasedDevices,-1);
            console.log("In Else Condition");
            //console.log(error.errorCode);
            console.log(error);
            this.errorDetail = error.error.exceptionMessage;
            // console.log(error.exceptionMessage);
          }
        })
      this.checkedList = [];

    }
  }

  public EnableExclusive(location: any) {
    console.log("enable exclusive to" + location);
    for (let device of location) {
      this.DashBoardService.EnableExclusive(this.sessionToken, device, this.scanCapabilities).subscribe(res1 => {
        console.log("enable exclusive to" + device);
        console.log("enable exclusive seesion" + this.sessionToken);
        console.log("response = " + res1);
        if (res1 === null) {
          this.errorDetail = "Selected device  enabled successfully!";
        } else
          if (res1.errorCode === null) {
            this.errorDetail = "Internal error occured!";
          }
          else if (res1.errorCode === "NGPAG0004") {
            this.UpdateStatus(this.releasedDevices, -1);
            this.errorDetail = "This device is unavailable";
          }
          else {
            console.log("In response Else Condition");
            this.errorDetail = res1.userMessage;
          }
      },
        error => {
          if (error.errorCode === "NGPAG0004") {
            console.log("In if Condition");
            this.errorDetail = "This device is unavailable";

          }
          else if (error.exceptionMessage != null && error.exceptionMessage != "undefined") {
            console.log("In if Else Condition");
            this.errorDetail = error.exceptionMessage;

          }
          else {
            // this.UpdateStatus(this.releasedDevices,-1);
            console.log("In Else Condition");
            //console.log(error.errorCode);
            console.log(error);
            this.errorDetail = error.error.exceptionMessage;
            // console.log(error.exceptionMessage);
          }
        })
      this.checkedList = [];

    }
  }

  public Release(location: any) {
    for (let device of location) {
      console.log("release to" + device);
      this.DashBoardService.Release(this.sessionToken, device).subscribe(
        res1 => {
          console.log(res1);
          if (res1 === null) {
            this.releasedDevices.push(device);
            this.UpdateStatus(this.releasedDevices, -1);
            this.errorDetail = "Successfully released selected devices";
          }
          console.log(this.releasedDevices);
        }
      );
    }
  }

  public ReleaseAll() {
    this.DashBoardService.ReleaseAll(this.sessionToken).subscribe(res1 => {
      if (res1 === null) {
        this.errorDetail = "Successfully released All the devices";
      }
    });
    this.UpdateStatus(this.checkedList, -1);
  }


  //NGPAG1001 Card error: The magstripe was not readable.
  //NGPAG1002 Device error: The magstripe was not read.
  //NGPAG2001 Device error: The document was not printed.
  //NGPAG3001 Barcode error: The barco was not readable.
  //NGPAG3002 Device error: The barcode was not read.
  //NGPAG4002 Device error: The gate is inoperative.
  //NGPAG0001 The application did not acknowledge the configuration changed message.
  //NGPAG0002 The peripheral has not been acquired by the application.
  //NGPAG0003 The peripheral is presently acquired for exclusive access by another application.
  //NGPAG0004 The peripheral is unavailable.
  public doAcquire(location: any, accessType: number) {
    if (this.sessionToken) {
      this.DashBoardService.DoAcquire(
        this.sessionToken,
        location,
        accessType
      ).subscribe(res1 => {
        if (res1 === null) {
          this.UpdateStatus(location, accessType);
        } else {
          if (res1.errorCode === "NGPAG0004") {
            this.errorDetail =
              res1.errorDetail + " The peripheral is unavailable";
          }
          if (res1.errorCode === "NGPAG0003") {
            this.errorDetail =
              res1.errorDetail +
              " The peripheral is presently acquired for exclusive access by another application";
          }
          if (res1.errorCode === "NGPAG0002") {
            this.errorDetail =
              res1.errorDetail +
              " The peripheral has not been acquired by the application";
          }
          if (res1.errorCode === "NGPAG0001") {
            this.errorDetail =
              res1.errorDetail +
              " The application did not acknowledge the configuration changed message";
          }
          if (res1.errorCode === "NGPAG4002") {
            this.errorDetail =
              res1.errorDetail + " Device error: The gate is inoperative";
          }
          if (res1.errorCode === "NGPAG3002") {
            this.errorDetail =
              res1.errorDetail + " Device error: The barcode was not read";
          }
          if (res1.errorCode === "NGPAG3001") {
            this.errorDetail =
              res1.errorDetail + " Barcode error: The barcode was not readable";
          }
          if (res1.errorCode === "NGPAG2001") {
            this.errorDetail =
              res1.errorDetail + " Device error: The document was not printed";
          }
          if (res1.errorCode === "NGPAG1002") {
            this.errorDetail =
              res1.errorDetail + " Device error: The magstripe was not read";
          }
          if (res1.errorCode === "NGPAG1001") {
            this.errorDetail =
              res1.errorDetail + " Card error: The magstripe was not readable";
          }

          this.checkedList = [];
          console.log("else condition" + res1);
        }
      });
    } else {
      console.log("Error occured");
    }
  }

  public UpdateStatus(checkedDevices: any, status: number) {
    console.log(checkedDevices);

    if (
      checkedDevices.length == 0 ||
      checkedDevices == null ||
      checkedDevices == "undefined"
    ) {
      for (let device of this.printCapabilities) {
        if (status === -1) {
          device.ReserveStatus = "";
        }
      }
    } else {
      this.UpdateDeviceDefaultStatus(
        this.printCapabilities,
        this.scanCapabilities
      );
      for (let obj of checkedDevices) {
        //Json.this.printCapabilities.foreach(obj => {
        for (let device of this.printCapabilities) {
          // this.status.foreach(statusObj => {
          if (obj === device.locator) {
            if (status === 1) {
              device.ReserveStatus = "Acquired Shared";
            }
            if (status === 0) {
              device.ReserveStatus = "Acquired Exclusive";
            }
            if (status === -1) {
              device.ReserveStatus = "";
            }
          }
        }

        for (let device of this.scanCapabilities) {
          if (obj === device.locator) {
            if (status === 1) {
              device.ReserveStatus = "Acquired Shared";
            }
            if (status === 0) {
              device.ReserveStatus = "Acquired Exclusive";
            }
          }
        }
        // console.log(res);
        let capabilities: Capabilities[] = [];
        capabilities = this.selectedTab === 'print' ? this.printCapabilities : this.scanCapabilities;
        console.log(capabilities);
      }
    }
    // });
    this.checkedList = [];
  }

  private UpdateDeviceDefaultStatus(printCapabilities: any, scanCapabilities: any) {
    for (let device of printCapabilities) {
      device.EnableAPITag = this.AddAPITag(device.locator);
    }
    for (let device of scanCapabilities) {
      device.EnableAPITag = this.AddAPITag(device.locator);
    }
  }

  private AddAPITag(locatorName: string) {
    var deviceName: string;
    deviceName = this.trimLocationName(locatorName);
    var tagAPI: string;
    deviceName = deviceName.toLowerCase();
    if (deviceName == "lsr1") {
      deviceName = "lsrorbgr";
    }
    if (deviceName == "bgr1") {
      deviceName = "lsrorbgr";
    }
    switch (deviceName) {
      case "lsrorbgr": {
        tagAPI = "READBARCODE";
        break;
      }
      case "msr1": {
        tagAPI = "READMAGSTRIPE";
        break;
      }
      case "ocr1": {
        tagAPI = "READTRAVELDOC";
        break;
      }
      default: {
        tagAPI = "";
      }
    }
    return tagAPI;
  }

  public trimLocationName(str: any) {
    var n = str.lastIndexOf(".");
    return str.substring(n + 1);
  }

  public ResetCheckBox() {
    console.log("reseting checkbox");

    let capabilities: Capabilities[] = [];
    capabilities = this.selectedTab === 'print' ? this.printCapabilities : this.scanCapabilities;

    capabilities.forEach(item => {
      console.log(item.checked);
      item.checked = false;
    });
  }

  public RefreshModel() {
    console.log("refreshing model");
    this.errorDetail = "";
    this.statusMessage = "";
    this.printType = "";
  }

  private printDocument(payLoadBase64: any) {

    let deviceType = this.checkedList[0].slice(
      this.checkedList[0].indexOf(".") + 1,
      this.checkedList[0].length
    );

    this.DashBoardService.Print(
      this.sessionToken,
      this.checkedList[0],
      this.deviceMappingData[deviceType].documentType,
      this.deviceMappingData[deviceType].payLoadType,
      payLoadBase64
    ).subscribe(
      (res) => {
        this.printStatus = 0;
        this.statusMessage = "Print req sent";
        console.log("Print req sent successfully");
      },
      (err) => {
        console.log("print req failed");
      }
    );
  }

  public changeNoOfPrints(printValue: any) {
    this.noOfPrints = printValue;
  }

  // TODO : move to service
  private getPrintTypePayload(printType: string): string {
    let payLoadBase64BTP =
      "JVBERi0xLjUNCiXi48/TDQoxIDAgb2JqDQo8PA0KL1R5cGUgL0NhdGFsb2cNCi9QYWdlcyAyIDAgUg0KPj4NCmVuZG9iag0KNSAwIG9iag0KPDwNCi9GaWx0ZXIgL0ZsYXRlRGVjb2RlDQovTGVuZ3RoIDQxNzENCj4+DQpzdHJlYW0NCnhe7V1PbyU5br8b8HeoS5DNoWhRJEXpOD1jLDYIspO2b8Ee7Ofn/YPZ6WSmkUXy6UOW/f6Nx9P2eyq42dVud7X9uqpE/kRSpERR/31+9k/Dt3+5+fHP62H1Yfzw091ff7z5uB5+/t+fP67/Pnz8MPywvv948fHDf8H5GUJKAzz+3f6ClJL/O6z+7i+7/unmx59/8Hc8fd+z79g+b/T8x3S1b0wKNWfd3pFrAhHUIRO0WkmHUVsCrfXh4Ytb4Xazrnm8Sbweua5lvCW+HeVurXdrWa8k1eG7D1Mb22bspbXsNwKkTG2gBiUJyzCyABkh5bEVqqK4uivjTbvBkdPt7Xhze9NGQlrjCpvq/e2TVhAVqGxbIYRSh5yhGcllGFEaZGr42IR9dodS7sZKd83ar2m8vSEa7bdbTe2+rcrNXhMX363/56+r9fvfvxu+vTr4dfXz+dl//mlq8u4R/H+cn6Xhb375V7/4n/e/Pz/b/PzTn/1nhVRLQR7GLAKIFQejCw2JVpn2Pv3h/Ox2v4ndg5i4QsV2+OD20+cfJGLgWvPhg9tPn3+wlQbM9Rctbj99/kFGgiqVDh/cfvr8g9XEL2nVwwe3nz48+CIBkCSQimwlmTInwfV4c8c0smoaa0YTgGpN39f1vcnyy2QMBaQlMTGuNKlPfmyirm9ptbq/HVe3jU2My2ps67v1SLd3ZDJ8g/VAxraaiaaUppo7jbEPTIFKPtTkp4qupmiZs1GSSSGjCcF057i5dfpqw7j341MLcToRilxdsFlBWcoxRFzc39HtOiUZK96vRiZZjbdNy4i8qnfr+1WVVRmqmSal1obr+/Ozd9euYdc/nZ/5W65Xj//+w+3cZD+v/+/ho2yyY+ZGh2vXWOsyKkbE9b+dn/2u/cs/n59dXu93+bE4FIGsVZYOg5ndyhnL4nEw45rE7PnCcbDBycYL1i8Bhy5GG9mp5OpekA2HdTNCfcKPO73Vav1gjTIBScYXNXoRXvxyAiIb+pfHuICWJnVxjJuTlpM0Wh7jBQp7dBeR8UPTyubPCh4QnYk/iQCmbE14fGlCkLDVY4aaDjSw6NQLaNEBRRrv0q9I45E4NJNGebC74WDAjjCgS6TmkDjkfjjUAmbUPC4PiAN1xCFBqZkoJA7cDwe1aBnJw6OAOEg/HIqFy5J9MikgDqUjDgykTWOOF9oPB5nCZYyJQ+2IAwG3yjHdqNYPB66QGsccLr7pCEMGZQrqVb/rh4NP82dpIWH4doaIr5i1rNRMLqqPpJryc3cevOjEVhUSS/F1KWlAVORFjb4R7B1jOszmrCQLq0Mw3jGKQzU501JiMN4xbMsE1ISDiHrHOC3XaRpOYjDeMTAjhlybaAzGO0Zinp0iqQYR9Y6hFwtgdnZDMN4z1vIXT+kTERjvGVwVu3LLMRjvGE0VezK1FMSB6Rg/lQIqrWAMxjsGTJoh67TaHoHxOUIkb13YJ5Q8IbJUfbjxdSFjBxq4FOsF6wTVJEetzL1Rp1z3k8Ym3oyvUAbE4bIfDrWBNIvbQ+Jw1REHBhRVWbpeqBFXmHBYuF6ohdtVaky9eN8PB8+lTaoxx4uhIw4Z2GIzHRZuH0QhYW08LNw+CJorR60ufdxkzyYh4i9BL/avRwOydfKLjSBa3ijzzWiQXIbRhrGGSQP1SceI0318Sj5lHg+GjjMOtUATQokIQ8cR3Dz8bGTLwpXCHfyMHvDpkpVCExRmxGHZSlEUKlcNCUNHr9a9exswNSIMPZ1aGylqopDS0NGndd+ems+RanSl2L8ejcfWpVUFQU1v5tKmKb2oQpIUKczoqKGPLi1HhOHf+8EwzVqzcxcPho7Dlru0KU3JUHXBSqGeIZRzSKXoOFXrU9aNOaSJ/L4fDObSNm5TUZC6YGmYJqxLTNvwh34w7FzaeDB0dmlr9j3oNfpIsX89Go+tS9sQim8KPQKOHiRMk7S+URlbaoH6pOOE1G6WNiAOPZ25Bpo8ISUgDB2H72qWCj32DghDx4m5h2laHhauFO7T5mmzekAcOoY4Pk8rMU1kRy9ml4SB4bXi0I35Bf9PEHrOh/AJuVazsUkNqOgGkA0A/AjAdNn79OC9JxKR3aMk4QcimCUfQ8TF52OrTseBGSrVEgiHy1lwkASEFAmHq3lwEFDhr3pRELCWvHh5KA1y0RoIh2EWHBShqXIgHN7Ng4MCtxbJTn4zCw5VQLR81YtaIUso/+G7WXBoBNWnhuLg8Md5cLDxgrd5CBFw+M39c0cCgmSITJMiqgXYS3xPd244txe0hy9DY/PT3n/uv/1UUrJZ7OpVcCZS0qby/DGkvFEPdfTw9tHwDzaeTSA0LmdBo9h4RjkeGlfzoGHNt/xVUx7QEIZSEg5fZcPR4GKe3ykG/Y3QGOZBw/yeUls4NN7NgoYN+8RFw6HxzTxoJMilUTg05tEU+0mqlnBofDcPGtYiYzx/44+zoIHFTzmLN6b8RuxkT0m2YXJDpv1u9CQ9pPspKiWDtTWlTwvUWvnR73jdytnJNKgY035smXVmRjqGhjfqk6c+4LEwiIKXoW8hYbjsB4MfpjGVXw8Iw1U3GMzvZUYdlq0T7NdWePgChKGHuUYCRPWTc4QElDM/d+fBi05vtImfFyfZq35s5p8/0ehFdOHDai5kDcJ2PwOcGVIhCcJ3P4ubG0gyx3JclpQTQ6PiJTYlXnfvX53G4yKDXbJU8xN+ZeMDw+Og8qL1lBOJ2F/UMSLSpgri64h4o17pOL2zwyFb8yXXSItb1/PgINBEIi36fj8LDsiA9lAgHIZ5cChmsfWrXuQMRIiBcLicBweFmkokHK5mwYEI8nY5IAQO8+iFBc2UI+XOzWMm2eIYCpVjPI8bxRbXiETKnZtHLYR9YjGSefiFG7V/PRqQvVypnEAJT1gwPJWUvfUpJ6V9Xdd+XJ/ykzc13rr29SxopAa1pXhofD8PGgiaOBwYwxxglFahbSesAqFxPQ8aBIISL43uchY0vJ4Fp3hoXM2DRgbZbiQJhMY8mqIMkjOFQ2MeK6oIFXO8AXYW56uUArmleMlB82hKSdAKxbMbvxE6HbtQtkuVwkyguRxVK+lkGh5TpdBPgSwbOX0dDRefi4QeC8MuVSogDP1W6nepUgFh6Ldwv02VwiXrhCfDe+XykDAM/WAQkNKWLgsJUEViqkQ/+5gZNPnK2Lho+4gNsgV/+iUoRQ9HbpdEyU2hJWrP3XnwotMbfUiiNM8aSDczE59o9CK6KdomUUZgu6vpeUyijMB3P1uzTaLkRUk5+W4z9DOMI/Ddz9vi5GeZ1bq0/mbxM+0oL82sWcSZWTzw5kWZNSlQqrQcUs73r9Pk5sNs3fRXAKsxJod0/4pd94knP0LAqxMkOmXu6xQSihfNGIs9QtvqYK8i4Y165OlU9bEooJoGIlJEFK77oYDQkLFFROH7GbQTUtbi6omteH7Ay/ZwnNwotumQGawFUNPLwqiL6Bporh5j9g0zEfjuqHPZa49gkP6eJXduu1EoSzIwjtuLcCIR+xmNCRq1SBuFOi5S7uNQgCi3QDhczoJDQahJI+FwNQ8O7iOFqpI5Sw34baZvKQxtW9pjw/lrVvFPJWUvocBISSVepm9Hy7WHhjQbUSheOuflPGj4jo0aL/XmahY0uIK0gBUzZ1ix2Uu9sTFOyvYkic2t09en4r2Tadik3qg/s6nC9zoaLj4X63UsDHupN/Fg6DoRukm9iQdDv3nRXepNPBT66YRXKeI6ndgXD4Z+OkECpbQaUyfe94MhAVJpMnwBpqHH4L2XbuGFnou8ZnLylEYf0y18/yttal9+otGL6KZol24RgO1+pmcv3SIA3/3G3126BS9Jyomh2o08LEzMOUG2+4KIeb8hlQVqqo1Dqvf+tcCJy++lQUtpU8Rgc+OLPIqTKSiqTkJNwMQcqRLn04Lr5ZTldz+xLyIMf+gHA5oYimdWB4Th/Qz6uVuArwic9DU+7vGNooWdfiK2JnCIXtTmRXQVVH9tI47Bd0edy8Zs0SB8/4aSQS2aFfcnx7Gl+ouVuF8BoIj5D2qBNYlZHZSjsn5OpWFaSrNOoIL75SlfRcPF/X29vUs3eSz3qzbyGnG8uaP12PL6Tssa60pwaEDCvq79yj7JZn5yfugThIxibD3jgB8NgxhxiBIShsuOMBCYR4ohYbjqBwMr+Km147J1gs0cJsGl6wTZAJdbTNPwviMMvjXCxugxvmnoMnj7XszqXCoziOhrTi44pdGKvj6lRGBhyovavIhuibJ5R8I+TRSA7X6WJxfzSYqE4Lrf4Es26lSVNixLxklBS/Idd7okGWfyIMu3fwRgu99oykZbQ81DQN3evxq7BwR7veryKeY92i/FEzk5+eaDDfevcyVOIcDYU82+zIQVVDZJaa8j4GW9ob595dW94RXMas449UaDTOpR2TMm5ygMSoGGEhSDy04Y5Gmut4TE4KoPBuKEZW7DgnVBMjSVVIcFywErVMoppj0YOmFQoLbcypJ1gbM5ZMmnO3ix44JvNJGEbcn2wN1Sb2JYsC5k37GQKg5fgBzsX48CY+e1V18zMufxuTsPXnRaiyzi0z5VCFTrS4tU3AeWOWVrQpPGYLqTwVV7a3NWQzDdycJWthubtmFJ4l0nO7qsjm7kOW5eYCEC05186WZ2jBrlRUl3s7cmjMJ0J+ONiaDUjMOSdBpTBUbGRYk3+i4URgnZ0/vXY6ftJYG5wC7qJM2PmT5qK9TJNEitvmJCao/IpnTb62h4WZ+0z3e9jHmav+aQMPRbP6M2bUEIiULHNWOCbFETDsvWiez1nMTnrSi8NPQw15ibp5T72ZAkkGhzaOgnxqsOjapn9WomsLHps17b7Sd8Xou9SqIYfPezwMgJ7LEag++rjnwX4OoJgbosORdrRhIPEft7//qkhI8TfVDk5xm+KkPBMrle0GoVPGqz1OlESCpTcYbp8Lt8VBLJG20mab8Sjx2Jg5oDqFPe2MJxICjEuHh5KNXMmOri5cHLWxV03y8+Dl2sNiZf2fcqLqjmFbej8OhAA4tORw4qFCYc4nRK6iecrUCZ5i4CwoAdYUCXSF8NCohD7oeDl59O6nlHAXGgjjgkX0iYitDFw4H74aBiLjx53klAHKQfDqVBlSwSEofSEQcG0qYxxwvth4NUaKVhTBxqRxwIuFWO6UZ19PG5gvmxMYeLbzrCYKEOU1Cv+l0/HEghZ984GBCGb/uvfuwWq5vryaZM5evgOJmEx7VqCziB0rGlQ+9jTxnv1qoDwtBvpWS7Vh0QhX7rJru1alyyTuzWqjG8NPSw1lgq0DR6+WxAIz1hUeUUGvJ0GmZTL/qWOFCf9BNNLNmGrZAg9DPWKAUUtYSE4aojDAkaexGOtmiVYIGsVJauFObC8LRlIyAM7zvCQBZfZRq+ANvQY+yu1rZOtbHebux2GpLHvOOiDZUyEE15+W3JhsocOS611WHZo7c5cqVpbQtXCnPlGiHpsGxpYIuwuLaYvtzQDwbx2lOsC1cKc+UUq7SFjxTmylUpKAu3DX7abp1qDbQlK0XOXvVDvhTP/uv31+8v4/v/ATmN4b8NCmVuZHN0cmVhbQ0KZW5kb2JqDQo2IDAgb2JqDQo8PA0KL0ZpbHRlciBbL0RDVERlY29kZV0NCi9EZWNvZGUgWy4wMCAxLjAwIC4wMCAxLjAwIC4wMCAxLjAwXQ0KL0NvbG9yU3BhY2UgL0RldmljZVJHQg0KL1R5cGUgL1hPYmplY3QNCi9TdWJ0eXBlIC9JbWFnZQ0KL1dpZHRoIDMwNA0KL0hlaWdodCA3OTENCi9CaXRzUGVyQ29tcG9uZW50IDgNCi9MZW5ndGggMjE4MjcNCj4+DQpzdHJlYW0NCv/Y/+AAEEpGSUYAAQEBAMsAywAA/9sAQwABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB/9sAQwEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB/8AAEQgDFwEwAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A/v4ooooAKKKKACiiigAooooAKKKKAP4A/wDgxj/5yi/92Tf+/cV/AHX9/n/BjH/zlF/7sm/9+4r+AOgD/f4r+AP/AIMY/wDnKL/3ZN/79xX9/lfwB/8ABjH/AM5Rf+7Jv/fuKAP4A6/v8/4PnP8AnF1/3ez/AO+j1/AHX9/n/B85/wA4uv8Au9n/AN9HoAP+DGP/AJyi/wDdk3/v3FH/AAYx/wDOUX/uyb/37ij/AIMY/wDnKL/3ZN/79xR/wYx/85Rf+7Jv/fuKAD/g+c/5xdf93s/++j0f8Hzn/OLr/u9n/wB9Ho/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30egD+/yv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egA/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/gxj/5yi/8Adk3/AL9xX8Adf3+f8GMf/OUX/uyb/wB+4r+AOgD/AH+K/gD/AODGP/nKL/3ZN/79xX9/lfwB/wDBjH/zlF/7sm/9+4oA/gDr+/z/AIPnP+cXX/d7P/vo9fwB1/f5/wAHzn/OLr/u9n/30egA/wCDGP8A5yi/92Tf+/cUf8GMf/OUX/uyb/37ij/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4oAP+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9Ho/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99HoA/v8r/AHr/f4r/AHoA/3+K/wB6/3+K/wB6AP7/P+D5z/AJxdf93s/wDvo9H/AAfOf84uv+72f/fR6P8Ag+c/5xdf93s/++j0f8Hzn/OLr/u9n/30egA/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6P+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9HoA/v8ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA/gD/4MY/8AnKL/AN2Tf+/cV/AHX9/n/BjH/wA5Rf8Auyb/AN+4r+AOgD/f4r+AP/gxj/5yi/8Adk3/AL9xX9/lfwB/8GMf/OUX/uyb/wB+4oA/gDr+/wA/4PnP+cXX/d7P/vo9fwB1/f5/wfOf84uv+72f/fR6AD/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4o/4MY/+cov/AHZN/wC/cUf8GMf/ADlF/wC7Jv8A37igA/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99Ho/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6AP7/ACv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egA/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/AIMY/wDnKL/3ZN/79xX8Adf3+f8ABjH/AM5Rf+7Jv/fuK/gDoA/3+K/gD/4MY/8AnKL/AN2Tf+/cV/f5X8Af/BjH/wA5Rf8Auyb/AN+4oA/gDr+/z/g+c/5xdf8Ad7P/AL6PX8Adf3+f8Hzn/OLr/u9n/wB9HoAP+DGP/nKL/wB2Tf8Av3FH/BjH/wA5Rf8Auyb/AN+4o/4MY/8AnKL/AN2Tf+/cUf8ABjH/AM5Rf+7Jv/fuKAD/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99Ho/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6AP7/K/wAAev8Af4r/AAB6AP8Af4r/AAB6/wB/iv8AAHoA/v8AP+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0ej/g+c/5xdf8Ad7P/AL6PR/wfOf8AOLr/ALvZ/wDfR6AD/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30ej/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99HoA/v8AKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP4A/wDgxj/5yi/92Tf+/cV/AHX9/n/BjH/zlF/7sm/9+4r+AOgD/f4r+AP/AIMY/wDnKL/3ZN/79xX9/lfwB/8ABjH/AM5Rf+7Jv/fuKAP4A6/v8/4PnP8AnF1/3ez/AO+j1/AHX9/n/B85/wA4uv8Au9n/AN9HoAP+DGP/AJyi/wDdk3/v3FH/AAYx/wDOUX/uyb/37ij/AIMY/wDnKL/3ZN/79xR/wYx/85Rf+7Jv/fuKAD/g+c/5xdf93s/++j0f8Hzn/OLr/u9n/wB9Ho/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30egD+/yv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egA/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/gxj/5yi/8Adk3/AL9xX8Adf3+f8GMf/OUX/uyb/wB+4r+AOgD/AH+K/gD/AODGP/nKL/3ZN/79xX9/lfwB/wDBjH/zlF/7sm/9+4oA/gDr+/z/AIPnP+cXX/d7P/vo9fwB1/f5/wAHzn/OLr/u9n/30egA/wCDGP8A5yi/92Tf+/cUf8GMf/OUX/uyb/37ij/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4oAP+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9Ho/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99HoA/v8r/AHr/f4r/AHoA/3+K/wB6/3+K/wB6AP7/P+D5z/AJxdf93s/wDvo9H/AAfOf84uv+72f/fR6P8Ag+c/5xdf93s/++j0f8Hzn/OLr/u9n/30egA/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6P+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9HoA/v8ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA/gD/4MY/8AnKL/AN2Tf+/cV/AHX9/n/BjH/wA5Rf8Auyb/AN+4r+AOgD/f4r+AP/gxj/5yi/8Adk3/AL9xX9/lfwB/8GMf/OUX/uyb/wB+4oA/gDr+/wA/4PnP+cXX/d7P/vo9fwB1/f5/wfOf84uv+72f/fR6AD/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4o/4MY/+cov/AHZN/wC/cUf8GMf/ADlF/wC7Jv8A37igA/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99Ho/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6AP7/ACv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egA/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/AIMY/wDnKL/3ZN/79xX8Adf3+f8ABjH/AM5Rf+7Jv/fuK/gDoA/3+K/gD/4MY/8AnKL/AN2Tf+/cV/f5X8Af/BjH/wA5Rf8Auyb/AN+4oA/gDr+/z/g+c/5xdf8Ad7P/AL6PX8Adf3+f8Hzn/OLr/u9n/wB9HoAP+DGP/nKL/wB2Tf8Av3FH/BjH/wA5Rf8Auyb/AN+4o/4MY/8AnKL/AN2Tf+/cUf8ABjH/AM5Rf+7Jv/fuKAD/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99Ho/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6AP7/K/wAAev8Af4r/AAB6AP8Af4r/AAB6/wB/iv8AAHoA/v8AP+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0ej/g+c/5xdf8Ad7P/AL6PR/wfOf8AOLr/ALvZ/wDfR6AD/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30ej/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99HoA/v8AKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP4A/wDgxj/5yi/92Tf+/cV/AHX9/n/BjH/zlF/7sm/9+4r+AOgD/f4r+AP/AIMY/wDnKL/3ZN/79xX9/lfwB/8ABjH/AM5Rf+7Jv/fuKAP4A6/v8/4PnP8AnF1/3ez/AO+j1/AHX9/n/B85/wA4uv8Au9n/AN9HoAP+DGP/AJyi/wDdk3/v3FH/AAYx/wDOUX/uyb/37ij/AIMY/wDnKL/3ZN/79xR/wYx/85Rf+7Jv/fuKAD/g+c/5xdf93s/++j0f8Hzn/OLr/u9n/wB9Ho/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30egD+/yv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egA/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/gxj/5yi/8Adk3/AL9xX8Adf3+f8GMf/OUX/uyb/wB+4r+AOgD/AH+K/gD/AODGP/nKL/3ZN/79xX9/lfwB/wDBjH/zlF/7sm/9+4oA/gDr+/z/AIPnP+cXX/d7P/vo9fwB1/f5/wAHzn/OLr/u9n/30egA/wCDGP8A5yi/92Tf+/cUf8GMf/OUX/uyb/37ij/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4oAP+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9Ho/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99HoA/v8r/AHr/f4r/AHoA/3+K/wB6/3+K/wB6AP7/P+D5z/AJxdf93s/wDvo9H/AAfOf84uv+72f/fR6P8Ag+c/5xdf93s/++j0f8Hzn/OLr/u9n/30egA/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6P+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9HoA/v8ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA/gD/4MY/8AnKL/AN2Tf+/cV/AHX9/n/BjH/wA5Rf8Auyb/AN+4r+AOgD/f4r+AP/gxj/5yi/8Adk3/AL9xX9/lfwB/8GMf/OUX/uyb/wB+4oA/gDr+/wA/4PnP+cXX/d7P/vo9fwB1/f5/wfOf84uv+72f/fR6AD/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4o/4MY/+cov/AHZN/wC/cUf8GMf/ADlF/wC7Jv8A37igA/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99Ho/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6AP7/ACv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egA/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/AIMY/wDnKL/3ZN/79xX8Adf3+f8ABjH/AM5Rf+7Jv/fuK/gDoA/3+K/gD/4MY/8AnKL/AN2Tf+/cV/f5X8Af/BjH/wA5Rf8Auyb/AN+4oA/gDr+/z/g+c/5xdf8Ad7P/AL6PX8Adf3+f8Hzn/OLr/u9n/wB9HoAP+DGP/nKL/wB2Tf8Av3FH/BjH/wA5Rf8Auyb/AN+4o/4MY/8AnKL/AN2Tf+/cUf8ABjH/AM5Rf+7Jv/fuKAD/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99Ho/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6AP7/K/wAAev8Af4r/AAB6AP8Af4r/AAB6/wB/iv8AAHoA/v8AP+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0ej/g+c/5xdf8Ad7P/AL6PR/wfOf8AOLr/ALvZ/wDfR6AD/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30ej/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99HoA/v8AKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP4A/wDgxj/5yi/92Tf+/cV/AHX9/n/BjH/zlF/7sm/9+4r+AOgD/f4r+AP/AIMY/wDnKL/3ZN/79xX9/lfwB/8ABjH/AM5Rf+7Jv/fuKAP4A6/v8/4PnP8AnF1/3ez/AO+j1/AHX9/n/B85/wA4uv8Au9n/AN9HoAP+DGP/AJyi/wDdk3/v3FH/AAYx/wDOUX/uyb/37ij/AIMY/wDnKL/3ZN/79xR/wYx/85Rf+7Jv/fuKAD/g+c/5xdf93s/++j0f8Hzn/OLr/u9n/wB9Ho/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30egD+/yv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egA/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/gxj/5yi/8Adk3/AL9xX8Adf3+f8GMf/OUX/uyb/wB+4r+AOgD/AH+K/gD/AODGP/nKL/3ZN/79xX9/lfwB/wDBjH/zlF/7sm/9+4oA/gDr+/z/AIPnP+cXX/d7P/vo9fwB1/f5/wAHzn/OLr/u9n/30egA/wCDGP8A5yi/92Tf+/cUf8GMf/OUX/uyb/37ij/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4oAP+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9Ho/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99HoA/v8r/AHr/f4r/AHoA/3+K/wB6/3+K/wB6AP7/P+D5z/AJxdf93s/wDvo9H/AAfOf84uv+72f/fR6P8Ag+c/5xdf93s/++j0f8Hzn/OLr/u9n/30egA/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6P+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9HoA/v8ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA/gD/4MY/8AnKL/AN2Tf+/cV/AHX9/n/BjH/wA5Rf8Auyb/AN+4r+AOgD/f4r+AP/gxj/5yi/8Adk3/AL9xX9/lfwB/8GMf/OUX/uyb/wB+4oA/gDr+/wA/4PnP+cXX/d7P/vo9fwB1/f5/wfOf84uv+72f/fR6AD/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4o/4MY/+cov/AHZN/wC/cUf8GMf/ADlF/wC7Jv8A37igA/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99Ho/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6AP7/ACv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egA/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/AIMY/wDnKL/3ZN/79xX8Adf3+f8ABjH/AM5Rf+7Jv/fuK/gDoA/3+K/gD/4MY/8AnKL/AN2Tf+/cV/f5X8Af/BjH/wA5Rf8Auyb/AN+4oA/gDr+/z/g+c/5xdf8Ad7P/AL6PX8Adf3+f8Hzn/OLr/u9n/wB9HoAP+DGP/nKL/wB2Tf8Av3FH/BjH/wA5Rf8Auyb/AN+4o/4MY/8AnKL/AN2Tf+/cUf8ABjH/AM5Rf+7Jv/fuKAD/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99Ho/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6AP7/K/wAAev8Af4r/AAB6AP8Af4r/AAB6/wB/iv8AAHoA/v8AP+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0ej/g+c/5xdf8Ad7P/AL6PR/wfOf8AOLr/ALvZ/wDfR6AD/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30ej/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99HoA/v8AKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP4A/wDgxj/5yi/92Tf+/cV/AHX9/n/BjH/zlF/7sm/9+4r+AOgD/f4r+AP/AIMY/wDnKL/3ZN/79xX9/lfwB/8ABjH/AM5Rf+7Jv/fuKAP4A6/v8/4PnP8AnF1/3ez/AO+j1/AHX9/n/B85/wA4uv8Au9n/AN9HoAP+DGP/AJyi/wDdk3/v3FH/AAYx/wDOUX/uyb/37ij/AIMY/wDnKL/3ZN/79xR/wYx/85Rf+7Jv/fuKAD/g+c/5xdf93s/++j0f8Hzn/OLr/u9n/wB9Ho/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30egD+/yv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egA/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/gxj/5yi/8Adk3/AL9xX8Adf3+f8GMf/OUX/uyb/wB+4r+AOgD/AH+K/gD/AODGP/nKL/3ZN/79xX9/lfwB/wDBjH/zlF/7sm/9+4oA/gDr+/z/AIPnP+cXX/d7P/vo9fwB1/f5/wAHzn/OLr/u9n/30egA/wCDGP8A5yi/92Tf+/cUf8GMf/OUX/uyb/37ij/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4oAP+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9Ho/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99HoA/v8r/AHr/f4r/AHoA/3+K/wB6/3+K/wB6AP7/P+D5z/AJxdf93s/wDvo9H/AAfOf84uv+72f/fR6P8Ag+c/5xdf93s/++j0f8Hzn/OLr/u9n/30egA/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6P+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9HoA/v8ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA/gD/4MY/8AnKL/AN2Tf+/cV/AHX9/n/BjH/wA5Rf8Auyb/AN+4r+AOgD/f4r+AP/gxj/5yi/8Adk3/AL9xX9/lfwB/8GMf/OUX/uyb/wB+4oA/gDr+/wA/4PnP+cXX/d7P/vo9fwB1/f5/wfOf84uv+72f/fR6AD/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4o/4MY/+cov/AHZN/wC/cUf8GMf/ADlF/wC7Jv8A37igA/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99Ho/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6AP7/ACv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egA/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/AIMY/wDnKL/3ZN/79xX8Adf3+f8ABjH/AM5Rf+7Jv/fuK/gDoA/3+K/gD/4MY/8AnKL/AN2Tf+/cV/f5X8Af/BjH/wA5Rf8Auyb/AN+4oA/gDr+/z/g+c/5xdf8Ad7P/AL6PX8Adf3+f8Hzn/OLr/u9n/wB9HoAP+DGP/nKL/wB2Tf8Av3FH/BjH/wA5Rf8Auyb/AN+4o/4MY/8AnKL/AN2Tf+/cUf8ABjH/AM5Rf+7Jv/fuKAD/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99Ho/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6AP7/K/wAAev8Af4r/AAB6AP8Af4r/AAB6/wB/iv8AAHoA/v8AP+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0ej/g+c/5xdf8Ad7P/AL6PR/wfOf8AOLr/ALvZ/wDfR6AD/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30ej/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99HoA/v8AKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP4A/wDgxj/5yi/92Tf+/cV/AHX9/n/BjH/zlF/7sm/9+4r+AOgD/f4r+AP/AIMY/wDnKL/3ZN/79xX9/lfwB/8ABjH/AM5Rf+7Jv/fuKAP4A6/v8/4PnP8AnF1/3ez/AO+j1/AHX9/n/B85/wA4uv8Au9n/AN9HoAP+DGP/AJyi/wDdk3/v3FH/AAYx/wDOUX/uyb/37ij/AIMY/wDnKL/3ZN/79xR/wYx/85Rf+7Jv/fuKAD/g+c/5xdf93s/++j0f8Hzn/OLr/u9n/wB9Ho/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30egD+/yv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egA/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/gxj/5yi/8Adk3/AL9xX8Adf3+f8GMf/OUX/uyb/wB+4r+AOgD/AH+K/gD/AODGP/nKL/3ZN/79xX9/lfwB/wDBjH/zlF/7sm/9+4oA/gDr+/z/AIPnP+cXX/d7P/vo9fwB1/f5/wAHzn/OLr/u9n/30egA/wCDGP8A5yi/92Tf+/cUf8GMf/OUX/uyb/37ij/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4oAP+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9Ho/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99HoA/v8r/AHr/f4r/AHoA/3+K/wB6/3+K/wB6AP7/P+D5z/AJxdf93s/wDvo9H/AAfOf84uv+72f/fR6P8Ag+c/5xdf93s/++j0f8Hzn/OLr/u9n/30egA/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6P+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9HoA/v8ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA/gD/4MY/8AnKL/AN2Tf+/cV/AHX9/n/BjH/wA5Rf8Auyb/AN+4r+AOgD/f4r+AP/gxj/5yi/8Adk3/AL9xX9/lfwB/8GMf/OUX/uyb/wB+4oA/gDr+/wA/4PnP+cXX/d7P/vo9fwB1/f5/wfOf84uv+72f/fR6AD/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4o/4MY/+cov/AHZN/wC/cUf8GMf/ADlF/wC7Jv8A37igA/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99Ho/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6AP7/ACv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egA/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/AIMY/wDnKL/3ZN/79xX8Adf3+f8ABjH/AM5Rf+7Jv/fuK/gDoA/3+K/gD/4MY/8AnKL/AN2Tf+/cV/f5X8Af/BjH/wA5Rf8Auyb/AN+4oA/gDr+/z/g+c/5xdf8Ad7P/AL6PX8Adf3+f8Hzn/OLr/u9n/wB9HoAP+DGP/nKL/wB2Tf8Av3FH/BjH/wA5Rf8Auyb/AN+4o/4MY/8AnKL/AN2Tf+/cUf8ABjH/AM5Rf+7Jv/fuKAD/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99Ho/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6AP7/K/wAAev8Af4r/AAB6AP8Af4r/AAB6/wB/iv8AAHoA/v8AP+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0ej/g+c/5xdf8Ad7P/AL6PR/wfOf8AOLr/ALvZ/wDfR6AD/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30ej/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99HoA/v8AKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP4A/wDgxj/5yi/92Tf+/cV/AHX9/n/BjH/zlF/7sm/9+4r+AOgD/f4r+AP/AIMY/wDnKL/3ZN/79xX9/lfwB/8ABjH/AM5Rf+7Jv/fuKAP4A6/v8/4PnP8AnF1/3ez/AO+j1/AHX9/n/B85/wA4uv8Au9n/AN9HoAP+DGP/AJyi/wDdk3/v3FH/AAYx/wDOUX/uyb/37ij/AIMY/wDnKL/3ZN/79xR/wYx/85Rf+7Jv/fuKAD/g+c/5xdf93s/++j0f8Hzn/OLr/u9n/wB9Ho/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30egD+/yv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egA/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/gxj/5yi/8Adk3/AL9xX8Adf3+f8GMf/OUX/uyb/wB+4r+AOgD/AH+K/gD/AODGP/nKL/3ZN/79xX9/lfwB/wDBjH/zlF/7sm/9+4oA/gDr+/z/AIPnP+cXX/d7P/vo9fwB1/f5/wAHzn/OLr/u9n/30egA/wCDGP8A5yi/92Tf+/cUf8GMf/OUX/uyb/37ij/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4oAP+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9Ho/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99HoA/v8r/AHr/f4r/AHoA/3+K/wB6/3+K/wB6AP7/P+D5z/AJxdf93s/wDvo9H/AAfOf84uv+72f/fR6P8Ag+c/5xdf93s/++j0f8Hzn/OLr/u9n/30egA/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6P+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9HoA/v8ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA/gD/4MY/8AnKL/AN2Tf+/cV/AHX9/n/BjH/wA5Rf8Auyb/AN+4r+AOgD/f4r+AP/gxj/5yi/8Adk3/AL9xX9/lfwB/8GMf/OUX/uyb/wB+4oA/gDr+/wA/4PnP+cXX/d7P/vo9fwB1/f5/wfOf84uv+72f/fR6AD/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4o/4MY/+cov/AHZN/wC/cUf8GMf/ADlF/wC7Jv8A37igA/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99Ho/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6AP7/ACv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egA/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/AIMY/wDnKL/3ZN/79xX8Adf3+f8ABjH/AM5Rf+7Jv/fuK/gDoA/3+K/gD/4MY/8AnKL/AN2Tf+/cV/f5X8Af/BjH/wA5Rf8Auyb/AN+4oA/gDr+/z/g+c/5xdf8Ad7P/AL6PX8Adf3+f8Hzn/OLr/u9n/wB9HoAP+DGP/nKL/wB2Tf8Av3FH/BjH/wA5Rf8Auyb/AN+4o/4MY/8AnKL/AN2Tf+/cUf8ABjH/AM5Rf+7Jv/fuKAD/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99Ho/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6AP7/K/wAAev8Af4r/AAB6AP8Af4r/AAB6/wB/iv8AAHoA/v8AP+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0ej/g+c/5xdf8Ad7P/AL6PR/wfOf8AOLr/ALvZ/wDfR6AD/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30ej/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99HoA/v8AKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP4A/wDgxj/5yi/92Tf+/cV/AHX9/n/BjH/zlF/7sm/9+4r+AOgD/f4r+AP/AIMY/wDnKL/3ZN/79xX9/lfwB/8ABjH/AM5Rf+7Jv/fuKAP4A6/v8/4PnP8AnF1/3ez/AO+j1/AHX9/n/B85/wA4uv8Au9n/AN9HoAP+DGP/AJyi/wDdk3/v3FH/AAYx/wDOUX/uyb/37ij/AIMY/wDnKL/3ZN/79xR/wYx/85Rf+7Jv/fuKAD/g+c/5xdf93s/++j0f8Hzn/OLr/u9n/wB9Ho/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30egD+/yv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egA/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/gxj/5yi/8Adk3/AL9xX8Adf3+f8GMf/OUX/uyb/wB+4r+AOgD/AH+K/gD/AODGP/nKL/3ZN/79xX9/lfwB/wDBjH/zlF/7sm/9+4oA/gDr+/z/AIPnP+cXX/d7P/vo9fwB1/f5/wAHzn/OLr/u9n/30egA/wCDGP8A5yi/92Tf+/cUf8GMf/OUX/uyb/37ij/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4oAP+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9Ho/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99HoA/v8r/AHr/f4r/AHoA/3+K/wB6/3+K/wB6AP7/P+D5z/AJxdf93s/wDvo9H/AAfOf84uv+72f/fR6P8Ag+c/5xdf93s/++j0f8Hzn/OLr/u9n/30egA/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6P+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9HoA/v8ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA/gD/4MY/8AnKL/AN2Tf+/cV/AHX9/n/BjH/wA5Rf8Auyb/AN+4r+AOgD/f4r+AP/gxj/5yi/8Adk3/AL9xX9/lfwB/8GMf/OUX/uyb/wB+4oA/gDr+/wA/4PnP+cXX/d7P/vo9fwB1/f5/wfOf84uv+72f/fR6AD/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4o/4MY/+cov/AHZN/wC/cUf8GMf/ADlF/wC7Jv8A37igA/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99Ho/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6AP7/ACv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egA/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/AIMY/wDnKL/3ZN/79xX8Adf3+f8ABjH/AM5Rf+7Jv/fuK/gDoA/3+K/gD/4MY/8AnKL/AN2Tf+/cV/f5X8Af/BjH/wA5Rf8Auyb/AN+4oA/gDr+/z/g+c/5xdf8Ad7P/AL6PX8Adf3+f8Hzn/OLr/u9n/wB9HoAP+DGP/nKL/wB2Tf8Av3FH/BjH/wA5Rf8Auyb/AN+4o/4MY/8AnKL/AN2Tf+/cUf8ABjH/AM5Rf+7Jv/fuKAD/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99Ho/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6AP7/K/wAAev8Af4r/AAB6AP8Af4r/AAB6/wB/iv8AAHoA/v8AP+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0ej/g+c/5xdf8Ad7P/AL6PR/wfOf8AOLr/ALvZ/wDfR6AD/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30ej/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99HoA/v8AKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP4A/wDgxj/5yi/92Tf+/cV/AHX9/n/BjH/zlF/7sm/9+4r+AOgD/f4r+AP/AIMY/wDnKL/3ZN/79xX9/lfwB/8ABjH/AM5Rf+7Jv/fuKAP4A6/v8/4PnP8AnF1/3ez/AO+j1/AHX9/n/B85/wA4uv8Au9n/AN9HoAP+DGP/AJyi/wDdk3/v3FH/AAYx/wDOUX/uyb/37ij/AIMY/wDnKL/3ZN/79xR/wYx/85Rf+7Jv/fuKAD/g+c/5xdf93s/++j0f8Hzn/OLr/u9n/wB9Ho/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30egD+/yv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egA/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/gxj/5yi/8Adk3/AL9xX8Adf3+f8GMf/OUX/uyb/wB+4r+AOgD/AH+K/gD/AODGP/nKL/3ZN/79xX9/lfwB/wDBjH/zlF/7sm/9+4oA/gDr+/z/AIPnP+cXX/d7P/vo9fwB1/f5/wAHzn/OLr/u9n/30egA/wCDGP8A5yi/92Tf+/cUf8GMf/OUX/uyb/37ij/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4oAP+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9Ho/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99HoA/v8r/AHr/f4r/AHoA/3+K/wB6/3+K/wB6AP7/P+D5z/AJxdf93s/wDvo9H/AAfOf84uv+72f/fR6P8Ag+c/5xdf93s/++j0f8Hzn/OLr/u9n/30egA/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6P+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9HoA/v8ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA/gD/4MY/8AnKL/AN2Tf+/cV/AHX9/n/BjH/wA5Rf8Auyb/AN+4r+AOgD/f4r+AP/gxj/5yi/8Adk3/AL9xX9/lfwB/8GMf/OUX/uyb/wB+4oA/gDr+/wA/4PnP+cXX/d7P/vo9fwB1/f5/wfOf84uv+72f/fR6AD/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4o/4MY/+cov/AHZN/wC/cUf8GMf/ADlF/wC7Jv8A37igA/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99Ho/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6AP7/ACv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egA/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/AIMY/wDnKL/3ZN/79xX8Adf3+f8ABjH/AM5Rf+7Jv/fuK/gDoA/3+K/gD/4MY/8AnKL/AN2Tf+/cV/f5X8Af/BjH/wA5Rf8Auyb/AN+4oA/gDr+/z/g+c/5xdf8Ad7P/AL6PX8Adf3+f8Hzn/OLr/u9n/wB9HoAP+DGP/nKL/wB2Tf8Av3FH/BjH/wA5Rf8Auyb/AN+4o/4MY/8AnKL/AN2Tf+/cUf8ABjH/AM5Rf+7Jv/fuKAD/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99Ho/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6AP7/K/wAAev8Af4r/AAB6AP8Af4r/AAB6/wB/iv8AAHoA/v8AP+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0ej/g+c/5xdf8Ad7P/AL6PR/wfOf8AOLr/ALvZ/wDfR6AD/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30ej/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99HoA/v8AKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP4A/wDgxj/5yi/92Tf+/cV/AHX9/n/BjH/zlF/7sm/9+4r+AOgD/f4r+AP/AIMY/wDnKL/3ZN/79xX9/lfwB/8ABjH/AM5Rf+7Jv/fuKAP4A6/v8/4PnP8AnF1/3ez/AO+j1/AHX9/n/B85/wA4uv8Au9n/AN9HoAP+DGP/AJyi/wDdk3/v3FH/AAYx/wDOUX/uyb/37ij/AIMY/wDnKL/3ZN/79xR/wYx/85Rf+7Jv/fuKAD/g+c/5xdf93s/++j0f8Hzn/OLr/u9n/wB9Ho/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30egD+/yv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egA/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/gxj/5yi/8Adk3/AL9xX8Adf3+f8GMf/OUX/uyb/wB+4r+AOgD/AH+K/gD/AODGP/nKL/3ZN/79xX9/lfwB/wDBjH/zlF/7sm/9+4oA/gDr+/z/AIPnP+cXX/d7P/vo9fwB1/f5/wAHzn/OLr/u9n/30egA/wCDGP8A5yi/92Tf+/cUf8GMf/OUX/uyb/37ij/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4oAP+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9Ho/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99HoA/v8r/AHr/f4r/AHoA/3+K/wB6/3+K/wB6AP7/P+D5z/AJxdf93s/wDvo9H/AAfOf84uv+72f/fR6P8Ag+c/5xdf93s/++j0f8Hzn/OLr/u9n/30egA/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6P+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9HoA/v8ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA/gD/4MY/8AnKL/AN2Tf+/cV/AHX9/n/BjH/wA5Rf8Auyb/AN+4r+AOgD/f4r+AP/gxj/5yi/8Adk3/AL9xX9/lfwB/8GMf/OUX/uyb/wB+4oA/gDr+/wA/4PnP+cXX/d7P/vo9fwB1/f5/wfOf84uv+72f/fR6AD/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4o/4MY/+cov/AHZN/wC/cUf8GMf/ADlF/wC7Jv8A37igA/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99Ho/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6AP7/ACv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egA/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/AIMY/wDnKL/3ZN/79xX8Adf3+f8ABjH/AM5Rf+7Jv/fuK/gDoA/3+K/gD/4MY/8AnKL/AN2Tf+/cV/f5X8Af/BjH/wA5Rf8Auyb/AN+4oA/gDr+/z/g+c/5xdf8Ad7P/AL6PX8Adf3+f8Hzn/OLr/u9n/wB9HoAP+DGP/nKL/wB2Tf8Av3FH/BjH/wA5Rf8Auyb/AN+4o/4MY/8AnKL/AN2Tf+/cUf8ABjH/AM5Rf+7Jv/fuKAD/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99Ho/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6AP7/K/wAAev8Af4r/AAB6AP8Af4r/AAB6/wB/iv8AAHoA/v8AP+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0ej/g+c/5xdf8Ad7P/AL6PR/wfOf8AOLr/ALvZ/wDfR6AD/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30ej/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99HoA/v8AKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP4A/wDgxj/5yi/92Tf+/cV/AHX9/n/BjH/zlF/7sm/9+4r+AOgD/f4r+AP/AIMY/wDnKL/3ZN/79xX9/lfwB/8ABjH/AM5Rf+7Jv/fuKAP4A6/v8/4PnP8AnF1/3ez/AO+j1/AHX9/n/B85/wA4uv8Au9n/AN9HoAP+DGP/AJyi/wDdk3/v3FH/AAYx/wDOUX/uyb/37ij/AIMY/wDnKL/3ZN/79xR/wYx/85Rf+7Jv/fuKAD/g+c/5xdf93s/++j0f8Hzn/OLr/u9n/wB9Ho/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30egD+/yv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egA/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/gxj/5yi/8Adk3/AL9xX8Adf3+f8GMf/OUX/uyb/wB+4r+AOgD/AH+K/gD/AODGP/nKL/3ZN/79xX9/lfwB/wDBjH/zlF/7sm/9+4oA/gDr+/z/AIPnP+cXX/d7P/vo9fwB1/f5/wAHzn/OLr/u9n/30egA/wCDGP8A5yi/92Tf+/cUf8GMf/OUX/uyb/37ij/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4oAP+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9Ho/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99HoA/v8r/AHr/f4r/AHoA/3+K/wB6/3+K/wB6AP7/P+D5z/AJxdf93s/wDvo9H/AAfOf84uv+72f/fR6P8Ag+c/5xdf93s/++j0f8Hzn/OLr/u9n/30egA/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6P+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9HoA/v8ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA/gD/4MY/8AnKL/AN2Tf+/cV/AHX9/n/BjH/wA5Rf8Auyb/AN+4r+AOgD/f4r+AP/gxj/5yi/8Adk3/AL9xX9/lfwB/8GMf/OUX/uyb/wB+4oA/gDr+/wA/4PnP+cXX/d7P/vo9fwB1/f5/wfOf84uv+72f/fR6AD/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4o/4MY/+cov/AHZN/wC/cUf8GMf/ADlF/wC7Jv8A37igA/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99Ho/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6AP7/ACv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egA/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/AIMY/wDnKL/3ZN/79xX8Adf3+f8ABjH/AM5Rf+7Jv/fuK/gDoA/3+K/gD/4MY/8AnKL/AN2Tf+/cV/f5X8Af/BjH/wA5Rf8Auyb/AN+4oA/gDr+/z/g+c/5xdf8Ad7P/AL6PX8Adf3+f8Hzn/OLr/u9n/wB9HoAP+DGP/nKL/wB2Tf8Av3FH/BjH/wA5Rf8Auyb/AN+4o/4MY/8AnKL/AN2Tf+/cUf8ABjH/AM5Rf+7Jv/fuKAD/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99Ho/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6AP7/K/wAAev8Af4r/AAB6AP8Af4r/AAB6/wB/iv8AAHoA/v8AP+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0ej/g+c/5xdf8Ad7P/AL6PR/wfOf8AOLr/ALvZ/wDfR6AD/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30ej/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99HoA/v8AKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP4A/wDgxj/5yi/92Tf+/cV/AHX9/n/BjH/zlF/7sm/9+4r+AOgD/f4r+AP/AIMY/wDnKL/3ZN/79xX9/lfwB/8ABjH/AM5Rf+7Jv/fuKAP4A6/v8/4PnP8AnF1/3ez/AO+j1/AHX9/n/B85/wA4uv8Au9n/AN9HoAP+DGP/AJyi/wDdk3/v3FH/AAYx/wDOUX/uyb/37ij/AIMY/wDnKL/3ZN/79xR/wYx/85Rf+7Jv/fuKAD/g+c/5xdf93s/++j0f8Hzn/OLr/u9n/wB9Ho/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30egD+/yv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egA/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/gxj/5yi/8Adk3/AL9xX8Adf3+f8GMf/OUX/uyb/wB+4r+AOgD/AH+K/gD/AODGP/nKL/3ZN/79xX9/lfwB/wDBjH/zlF/7sm/9+4oA/gDr+/z/AIPnP+cXX/d7P/vo9fwB1/f5/wAHzn/OLr/u9n/30egA/wCDGP8A5yi/92Tf+/cUf8GMf/OUX/uyb/37ij/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4oAP+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9Ho/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99HoA/v8r/AHr/f4r/AHoA/3+K/wB6/3+K/wB6AP7/P+D5z/AJxdf93s/wDvo9H/AAfOf84uv+72f/fR6P8Ag+c/5xdf93s/++j0f8Hzn/OLr/u9n/30egA/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6P+D5z/nF1/wB3s/8Avo9H/B85/wA4uv8Au9n/AN9HoA/v8ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA/gD/4MY/8AnKL/AN2Tf+/cV/AHX9/n/BjH/wA5Rf8Auyb/AN+4r+AOgD/f4r+AP/gxj/5yi/8Adk3/AL9xX9/lfwB/8GMf/OUX/uyb/wB+4oA/gDr+/wA/4PnP+cXX/d7P/vo9fwB1/f5/wfOf84uv+72f/fR6AD/gxj/5yi/92Tf+/cUf8GMf/OUX/uyb/wB+4o/4MY/+cov/AHZN/wC/cUf8GMf/ADlF/wC7Jv8A37igA/4PnP8AnF1/3ez/AO+j0f8AB85/zi6/7vZ/99Ho/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6AP7/ACv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egA/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/AIMY/wDnKL/3ZN/79xX8Adf3+f8ABjH/AM5Rf+7Jv/fuK/gDoA/3+K/gD/4MY/8AnKL/AN2Tf+/cV/f5X8Af/BjH/wA5Rf8Auyb/AN+4oA/gDr+/z/g+c/5xdf8Ad7P/AL6PX8Adf3+f8Hzn/OLr/u9n/wB9HoAP+DGP/nKL/wB2Tf8Av3FH/BjH/wA5Rf8Auyb/AN+4o/4MY/8AnKL/AN2Tf+/cUf8ABjH/AM5Rf+7Jv/fuKAD/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99Ho/4PnP+cXX/d7P/vo9H/B85/zi6/7vZ/8AfR6AP7/K/wAAev8Af4r/AAB6AP8Af4r/AAB6/wB/iv8AAHoA/v8AP+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0ej/g+c/5xdf8Ad7P/AL6PR/wfOf8AOLr/ALvZ/wDfR6AD/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30ej/AIPnP+cXX/d7P/vo9H/B85/zi6/7vZ/99HoA/v8AKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP4A/wDgxj/5yi/92Tf+/cV/AHX9/n/BjH/zlF/7sm/9+4r+AOgD/f4r+AP/AIMY/wDnKL/3ZN/79xX9/lfwB/8ABjH/AM5Rf+7Jv/fuKAP4A6/v8/4PnP8AnF1/3ez/AO+j1/AHX9/n/B85/wA4uv8Au9n/AN9HoAP+DGP/AJyi/wDdk3/v3FH/AAYx/wDOUX/uyb/37ij/AIMY/wDnKL/3ZN/79xR/wYx/85Rf+7Jv/fuKAD/g+c/5xdf93s/++j0f8Hzn/OLr/u9n/wB9Ho/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30egD+/yv8Aev9/iv8AegD/f4r/AHr/f4r/AHoA/v8/4PnP+cXX/AHez/wC+j0f8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PR/wAHzn/OLr/u9n/30egA/wCD5z/nF1/3ez/76PR/wfOf84uv+72f/fR6P+D5z/nF1/3ez/76PR/wfOf84uv+72f/AH0egD+/yiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD+AP/gxj/5yi/8Adk3/AL9xX8AdFFAH+/xX8Af/AAYx/wDOUX/uyb/37iiigD+AOv7/AD/g+c/5xdf93s/++j0UUAH/AAYx/wDOUX/uyb/37ij/AIMY/wDnKL/3ZN/79xRRQAf8Hzn/ADi6/wC72f8A30ej/g+c/wCcXX/d7P8A76PRRQB/f5X+APRRQB/v8V/gD0UUAf3+f8Hzn/OLr/u9n/30ej/g+c/5xdf93s/++j0UUAH/AAfOf84uv+72f/fR6P8Ag+c/5xdf93s/++j0UUAf3+UUUUAFFFFABRRRQAUUUUAFFFFAH//ZDQplbmRzdHJlYW0NCmVuZG9iag0KNyAwIG9iag0KPDwNCi9GaWx0ZXIgWy9EQ1REZWNvZGVdDQovRGVjb2RlIFsuMDAgMS4wMCAuMDAgMS4wMCAuMDAgMS4wMF0NCi9Db2xvclNwYWNlIC9EZXZpY2VSR0INCi9UeXBlIC9YT2JqZWN0DQovU3VidHlwZSAvSW1hZ2UNCi9XaWR0aCAyMjgNCi9IZWlnaHQgODANCi9CaXRzUGVyQ29tcG9uZW50IDgNCi9MZW5ndGggMzg0Mg0KPj4NCnN0cmVhbQ0K/9j/4AAQSkZJRgABAQEAywDLAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCABQAOQDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD+/iiiigAooooAKKKKACv5Av8Ag47/AOUpv/Bsf/2f/qP/AK0V+wDX9ftfyBf8HHf/AClN/wCDY/8A7P8A9R/9aK/YBoA/kC/4Ojv+U6/7c3/ds3/rHn7Ptf3+f8HR3/KCj9ub/u2b/wBbD/Z9r+AP/g6O/wCU6/7c3/ds3/rHn7Ptf3+f8HR3/KCj9ub/ALtm/wDWw/2faAP5Av8Ag47/AOUWX/Bsf/2YBqP/AKzr+wDR+2R/ypt/8Enf+z//AIjf+pz/AMFLKP8Ag47/AOUWX/Bsf/2YBqP/AKzr+wDR+2R/ypt/8Enf+z//AIjf+pz/AMFLKAD9jf8A5U2/+CsX/Z//AMOf/U5/4Jp1/X7/AM6sv/eAH/4HXX8gX7G//Km3/wAFYv8As/8A+HP/AKnP/BNOv6/f+dWX/vAD/wDA66AP5Av2yP8AlTb/AOCTv/Z//wARv/U5/wCCllf1+/8AB0d/ygo/bm/7tm/9bD/Z9r+QL9sj/lTb/wCCTv8A2f8A/Eb/ANTn/gpZX9fv/B0d/wAoKP25v+7Zv/Ww/wBn2gD8Af8Agxj/AOcov/dk3/v3FfgD/wAHR3/Kdf8Abm/7tm/9Y8/Z9r9/v+DGP/nKL/3ZN/79xX4A/wDB0d/ynX/bm/7tm/8AWPP2faAP6/f2yP8Alck/4JO/9mAfEb/1Bv8AgpZXwB+xN/zvOf8Ad3H/AMFFr7//AGyP+VyT/gk7/wBmAfEb/wBQb/gpZXwB+xN/zvOf93cf/BRaAPgD9sj/AJU2/wDgk7/2f/8AEb/1Of8AgpZX+n3X+YJ+2R/ypt/8Enf+z/8A4jf+pz/wUsr/AE+6ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAr+QL/AIOO/wDlKb/wbH/9n/6j/wCtFfsA1/X7X8gX/Bx3/wApTf8Ag2P/AOz/APUf/Wiv2AaAP5Av+Do7/lOv+3N/3bN/6x5+z7X9/n/B0d/ygo/bm/7tm/8AWw/2fa/gD/4Ojv8AlOv+3N/3bN/6x5+z7X9/n/B0d/ygo/bm/wC7Zv8A1sP9n2gD+QL/AIOO/wDlFl/wbH/9mAaj/wCs6/sA0ftkf8qbf/BJ3/s//wCI3/qc/wDBSyj/AIOO/wDlFl/wbH/9mAaj/wCs6/sA0ftkf8qbf/BJ3/s//wCI3/qc/wDBSygA/Y3/AOVNv/grF/2f/wDDn/1Of+Cadf1+/wDOrL/3gB/+B11/IF+xv/ypt/8ABWL/ALP/APhz/wCpz/wTTr+v3/nVl/7wA/8AwOugD+QL9sj/AJU2/wDgk7/2f/8AEb/1Of8AgpZX9fv/AAdHf8oKP25v+7Zv/Ww/2fa/kC/bI/5U2/8Agk7/ANn/APxG/wDU5/4KWV/X7/wdHf8AKCj9ub/u2b/1sP8AZ9oA/AH/AIMY/wDnKL/3ZN/79xX4A/8AB0d/ynX/AG5v+7Zv/WPP2fa/f7/gxj/5yi/92Tf+/cV+AP8AwdHf8p1/25v+7Zv/AFjz9n2gD+v39sj/AJXJP+CTv/ZgHxG/9Qb/AIKWV8AfsTf87zn/AHdx/wDBRa+//wBsj/lck/4JO/8AZgHxG/8AUG/4KWV8AfsTf87zn/d3H/wUWgD4A/bI/wCVNv8A4JO/9n//ABG/9Tn/AIKWV/p91/mCftkf8qbf/BJ3/s//AOI3/qc/8FLK/wBPugAooooAKKKKACiiigAooooAKKKKACiiigAooooAK/kC/wCDjv8A5Sm/8Gx//Z/+o/8ArRX7ANf1+1/IF/wcd/8AKU3/AINj/wDs/wD1H/1or9gGgD+QL/g6O/5Tr/tzf92zf+sefs+1/f5/wdHf8oKP25v+7Zv/AFsP9n2v4A/+Do7/AJTr/tzf92zf+sefs+1/f5/wdHf8oKP25v8Au2b/ANbD/Z9oA/kC/wCDjv8A5RZf8Gx//ZgGo/8ArOv7ANH7ZH/Km3/wSd/7P/8AiN/6nP8AwUso/wCDjv8A5RZf8Gx//ZgGo/8ArOv7ANH7ZH/Km3/wSd/7P/8AiN/6nP8AwUsoAP2N/wDlTb/4Kxf9n/8Aw5/9Tn/gmnX9fv8Azqy/94Af/gddfyBfsb/8qbf/AAVi/wCz/wD4c/8Aqc/8E06/r9/51Zf+8AP/AMDroA/kC/bI/wCVNv8A4JO/9n//ABG/9Tn/AIKWV/X7/wAHR3/KCj9ub/u2b/1sP9n2v5Av2yP+VNv/AIJO/wDZ/wD8Rv8A1Of+Cllf1+/8HR3/ACgo/bm/7tm/9bD/AGfaAPwB/wCDGP8A5yi/92Tf+/cV+AP/AAdHf8p1/wBub/u2b/1jz9n2v3+/4MY/+cov/dk3/v3FfgD/AMHR3/Kdf9ub/u2b/wBY8/Z9oA/r9/bI/wCVyT/gk7/2YB8Rv/UG/wCCllfAH7E3/O85/wB3cf8AwUWvv/8AbI/5XJP+CTv/AGYB8Rv/AFBv+CllfAH7E3/O85/3dx/8FFoA+AP2yP8AlTb/AOCTv/Z//wARv/U5/wCCllf6fdf5gn7ZH/Km3/wSd/7P/wDiN/6nP/BSyv8AT7oAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACv5Av8Ag47/AOUpv/Bsf/2f/qP/AK0V+wDX9ftfyBf8HHf/AClN/wCDY/8A7P8A9R/9aK/YBoA/kC/4Ojv+U6/7c3/ds3/rHn7Ptf3+f8HR3/KCj9ub/u2b/wBbD/Z9r+AP/g6O/wCU6/7c3/ds3/rHn7Ptf3+f8HR3/KCj9ub/ALtm/wDWw/2faAP5Av8Ag47/AOUWX/Bsf/2YBqP/AKzr+wDR+2R/ypt/8Enf+z//AIjf+pz/AMFLKP8Ag47/AOUWX/Bsf/2YBqP/AKzr+wDR+2R/ypt/8Enf+z//AIjf+pz/AMFLKAD9jf8A5U2/+CsX/Z//AMOf/U5/4Jp1/X7/AM6sv/eAH/4HXX8gX7G//Km3/wAFYv8As/8A+HP/AKnP/BNOv6/f+dWX/vAD/wDA66AP5Av2yP8AlTb/AOCTv/Z//wARv/U5/wCCllf1+/8AB0d/ygo/bm/7tm/9bD/Z9r+QL9sj/lTb/wCCTv8A2f8A/Eb/ANTn/gpZX9fv/B0d/wAoKP25v+7Zv/Ww/wBn2gD8Af8Agxj/AOcov/dk3/v3FfgD/wAHR3/Kdf8Abm/7tm/9Y8/Z9r9/v+DGP/nKL/3ZN/79xX4A/wDB0d/ynX/bm/7tm/8AWPP2faAP6/f2yP8Alck/4JO/9mAfEb/1Bv8AgpZXwB+xN/zvOf8Ad3H/AMFFr7//AGyP+VyT/gk7/wBmAfEb/wBQb/gpZXwB+xN/zvOf93cf/BRaAPgD9sj/AJU2/wDgk7/2f/8AEb/1Of8AgpZX+n3X+YJ+2R/ypt/8Enf+z/8A4jf+pz/wUsr/AE+6ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAr+QL/AIOO/wDlKb/wbH/9n/6j/wCtFfsA1/X7X8gX/Bx3/wApTf8Ag2P/AOz/APUf/Wiv2AaAP5Av+Do7/lOv+3N/3bN/6x5+z7X9/n/B0d/ygo/bm/7tm/8AWw/2fa/gD/4Ojv8AlOv+3N/3bN/6x5+z7X9/n/B0d/ygo/bm/wC7Zv8A1sP9n2gD+QL/AIOO/wDlFl/wbH/9mAaj/wCs6/sA0ftkf8qbf/BJ3/s//wCI3/qc/wDBSyj/AIOO/wDlFl/wbH/9mAaj/wCs6/sA0ftkf8qbf/BJ3/s//wCI3/qc/wDBSygA/Y3/AOVNv/grF/2f/wDDn/1Of+Cadf1+/wDOrL/3gB/+B11/IF+xv/ypt/8ABWL/ALP/APhz/wCpz/wTTr+v3/nVl/7wA/8AwOugD+QL9sj/AJU2/wDgk7/2f/8AEb/1Of8AgpZX9fv/AAdHf8oKP25v+7Zv/Ww/2fa/kC/bI/5U2/8Agk7/ANn/APxG/wDU5/4KWV/X7/wdHf8AKCj9ub/u2b/1sP8AZ9oA/AH/AIMY/wDnKL/3ZN/79xX4A/8AB0d/ynX/AG5v+7Zv/WPP2fa/f7/gxj/5yi/92Tf+/cV+AP8AwdHf8p1/25v+7Zv/AFjz9n2gD+v39sj/AJXJP+CTv/ZgHxG/9Qb/AIKWV8AfsTf87zn/AHdx/wDBRa+//wBsj/lck/4JO/8AZgHxG/8AUG/4KWV8AfsTf87zn/d3H/wUWgD4A/bI/wCVNv8A4JO/9n//ABG/9Tn/AIKWV/p91/mCftkf8qbf/BJ3/s//AOI3/qc/8FLK/wBPugAooooAKKKKACiiigAooooA/9kNCmVuZHN0cmVhbQ0KZW5kb2JqDQo4IDAgb2JqDQo8PA0KL0ZpbHRlciBbL0RDVERlY29kZV0NCi9EZWNvZGUgWy4wMCAxLjAwIC4wMCAxLjAwIC4wMCAxLjAwXQ0KL0NvbG9yU3BhY2UgL0RldmljZVJHQg0KL1R5cGUgL1hPYmplY3QNCi9TdWJ0eXBlIC9JbWFnZQ0KL1dpZHRoIDMyNg0KL0hlaWdodCA4OA0KL0JpdHNQZXJDb21wb25lbnQgOA0KL0xlbmd0aCAxNTc1Nw0KPj4NCnN0cmVhbQ0K/9j/4AAQSkZJRgABAQEAyADIAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCABYAUYDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD+/iiiigAooooAKKK/mq/4LK/8HJf7Mn/BMyLWfg38IItA/aY/bEj+02Nz8ONI1tG8AfB/UIf7JmWX42+ItKuRe2eoT2OpTXWm+BPDbXPia7msHt/EFz4NtLuy1WcA/ar9sX9tn9mX9gn4N6v8dP2pvinoHwx8C6eL220pdSuEl8ReNfEFppN/rMHg7wF4cib+0/Fni7U7TTbr+ztF0yJ5HKGa6ltLOOa6i/zRv+CuP/Bz1+1n+2h8Z/Do/Yq8efGr9i34CfC3/hL9K8Mj4e/FPxL4S+I3xiOu6np3leNPiu3hDUNL0m08jStB0weFfA9u+up4Lm1TxQW8V69JrjGy/DT9tr9v79rP/gof8Wbn4yftZfF3XviX4lU3EPhvRH8rR/AfgDSrhLSJtD+H3gXSltvDnhPTpINP09NQk06xXVfEFxZxar4n1LW9ae41Kb42oA/Sv/h8t/wVl/6SQftp/wDiRfxR/wDmko/4fLf8FZf+kkH7af8A4kX8Uf8A5pK/NSigD9K/+Hy3/BWX/pJB+2n/AOJF/FH/AOaSj/h8t/wVl/6SQftp/wDiRfxR/wDmkr85NK0rVNe1TTdD0PTdQ1nWtZ1Cz0rR9H0qzudR1TVdU1G4js9P03TdPs45ru+1C+u5obWzs7WGW4uriWOCCN5XVT/cV/wRu/4NIfHHxGvPCn7Q/wDwVKsb74f/AA4U/wBp6L+yDZXeq6V8SfGkLx6lbwN8YfFWg6rpN/8AC7S4Z49N1SLwn4au7/xlrFpcm01rVfAV3aXGn3wB8H/8EstE/wCDjT/gq34wgPwd/b3/AG0/AvwI0nW10vx/+0j47+Pnxft/hz4c+x3uhpr2j+G/K8QxTeP/AB9p+la3Dqtr4K0a5tvOjRV1rW/DtpMuoL/pX/sd/s8+Kf2W/wBn/wAE/Brxt+0T8av2qPFnh2G4ufEXxq+PviFPEvj7xTrmqut7rLLemN72x8Mx6rJeP4W8P6pq3iS/8NaNPbaCfEeq2un2sq+yfDP4X/Dj4L+BPDfwv+EfgTwn8M/hz4OsBpfhbwP4G0DTfDHhfQbHzZbh7fS9F0i2tLC1E91PcXl1JHCJby9uLm9unmuriaZ+7oAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAK8c+PX7QfwT/Ze+F/ib40ftB/E7wj8JPhh4Rsbq+1vxd4y1WHTLBPstjeaj/Z2mwHfqGv6/eWthdnSPDWg2ep+IdbnhNno+mX14yQN+P/APwV9/4L8/si/wDBKzwx4i8Ezazp/wAaf2vrjQriXwX+zx4U1KG5m0HVbuysLrQ9X+NWsWc0n/CufC09tqtnrNvaXSP4r8UaYjjw3pMtu82r2H+Xx/wUO/4Kjfti/wDBTn4p3XxH/af+Jt/q+i2d/dS+AfhH4dMmhfCX4ZaW93qk+n6Z4U8H2sn2Se/0+01e60yTxl4hbWvHWs2Plwa54k1COKCOIA/oW/4LI/8AB158cf2mtQ8a/s9/8E87zX/2f/2d0vNQ0DUfjvaXV/o/xz+MmlQXNmU1Tw7KLbTdS+CvhLUntruKLTbKa58d63olzC+u6t4Z+36p4Otv43Xd5HeSRmkkkZnd3Ys7uxLM7sxLMzMSWYkkkkk5ptFABRRRQAV9x/sGf8E5v2uP+Ck3xaHwg/ZQ+F2oeNdTsPsc/jPxlqDTaN8NPhnpd/HqEtlrPxG8bSW0+neGrO/XSdSj0i1dbnWvEF3Zz6f4d0rVtQX7Kf25/wCCMv8AwbFftGf8FCU8J/H/APaZuNd/Zp/ZEuNRF5aHUdGuLb4zfGfTNM1DSmubTwF4b1iOzj8J+EfENjNqtpYfFPXotQgW5sftGgeEfFFlML2H/TN/ZZ/ZE/Zt/Yo+E2i/BD9lv4Q+E/g78NtDUmLRfDdtcS3+rXjvLLLrHirxNq1xqPijxjr87zSefr3irWdY1eSMpA159nihijAPxy/4I8f8G6/7J3/BLzRNH+IvjK00H9pL9sJxJc6j8dfE3h2SDRPAvnXOnX1toXwc8E6rqOs6d4V/sW40238v4gzxv8QtYuX1G4j1Lw7oepJ4S07+hyiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigDhPiZ8UPhx8F/AniT4ofFzx34T+Gfw58HWB1TxT448c6/pvhjwvoNj5sVulxqmtavc2lhaie6nt7O1SSYS3l7cW1lapNdXEML/5+3/BZL/g7h1r4g2Xin9nX/gllda54M8KXTNpfiL9sHVdO1Lw9461u2SbS7pl+B3hjV4bLVPA9jdCLU9JvvF/jPSU8Wz2ly03h3Q/CN9b2muT/wBJn/BTP/ghNF/wVU8YWepftD/t6/tQaF8M/Ddzfv4F+A/w1034beG/hV4Zju9QlvYb/UdHn8O6hceM/FlvC1vp48XeKbm/1OOzs4YNNTTIZLqKf8p/+IJL9h7/AKO9/as/8APhF/8AMRQB/m0avq+reINV1LXde1TUdb1vWb661TV9Z1e9udS1XVdTvp3ub7UdS1C8lmu76+vLmWS4uru6mluLieR5ZpHkdmOdX+lR/wAQSX7D3/R3v7Vn/gB8Iv8A5iKP+IJL9h7/AKO9/as/8APhF/8AMRQB/mr0V/pUf8QSX7D3/R3v7Vn/AIAfCL/5iKP+IJL9h7/o739qz/wA+EX/AMxFAH+bRpGmza1q2l6Pb3GnWk+rajZaZBd6vqVjo2k2s1/cxWsdxqmsanPa6bpWnQvKsl7qWoXNvY2Nsst1dzwwRSSL/c//AMEcf2Ef+CCP7Gk2kfHX9vb/AIKKfsJ/tR/tIW5s9S8M/Dy3+JOn618A/g1qEQ1KGZo7TV7i2svjT4kmtbqzmOr+MPClp4Y8Nanbk+G/D19qOnad4zn++P8AiCS/Ye/6O9/as/8AAD4Rf/MRR/xBJfsPf9He/tWf+AHwi/8AmIoA/f1f+C4X/BINQFX/AIKKfspKqgKqr8VNBAUAYAAEuAAOABwBS/8AD8P/AIJC/wDSRX9lP/w6ug//AB2vwB/4gkv2Hv8Ao739qz/wA+EX/wAxFH/EEl+w9/0d7+1Z/wCAHwi/+YigD9/v+H4f/BIX/pIr+yn/AOHV0H/47R/w/D/4JC/9JFf2U/8Aw6ug/wDx2vwB/wCIJL9h7/o739qz/wAAPhF/8xFH/EEl+w9/0d7+1Z/4AfCL/wCYigD9/v8Ah+H/AMEhf+kiv7Kf/h1dB/8AjtH/AA/D/wCCQv8A0kV/ZT/8OroP/wAdr8Af+IJL9h7/AKO9/as/8APhF/8AMRR/xBJfsPf9He/tWf8AgB8Iv/mIoA/f7/h+H/wSF/6SK/sp/wDh1dB/+O0f8Pw/+CQv/SRX9lP/AMOroP8A8dr8Af8AiCS/Ye/6O9/as/8AAD4Rf/MRR/xBJfsPf9He/tWf+AHwi/8AmIoA/f7/AIfh/wDBIX/pIr+yn/4dXQf/AI7R/wAPw/8AgkL/ANJFf2U//Dq6D/8AHa/AH/iCS/Ye/wCjvf2rP/AD4Rf/ADEUf8QSX7D3/R3v7Vn/AIAfCL/5iKAP3+/4fh/8Ehf+kiv7Kf8A4dXQf/jtH/D8P/gkL/0kV/ZT/wDDq6D/APHa/AH/AIgkv2Hv+jvf2rP/AAA+EX/zEUf8QSX7D3/R3v7Vn/gB8Iv/AJiKAP3+/wCH4f8AwSF/6SK/sp/+HV0H/wCO0f8AD8P/AIJC/wDSRX9lP/w6ug//AB2vwB/4gkv2Hv8Ao739qz/wA+EX/wAxFH/EEl+w9/0d7+1Z/wCAHwi/+YigD9/v+H4f/BIX/pIr+yn/AOHV0H/47R/w/D/4JC/9JFf2U/8Aw6ug/wDx2vwB/wCIJL9h7/o739qz/wAAPhF/8xFH/EEl+w9/0d7+1Z/4AfCL/wCYigD9/v8Ah+H/AMEhf+kiv7Kf/h1dB/8AjtH/AA/D/wCCQv8A0kV/ZT/8OroP/wAdr8Af+IJL9h7/AKO9/as/8APhF/8AMRR/xBJfsPf9He/tWf8AgB8Iv/mIoA/f7/h+H/wSF/6SK/sp/wDh1dB/+O0f8Pw/+CQv/SRX9lP/AMOroP8A8dr8Af8AiCS/Ye/6O9/as/8AAD4Rf/MRR/xBJfsPf9He/tWf+AHwi/8AmIoA/f7/AIfh/wDBIX/pIr+yn/4dXQf/AI7R/wAPw/8AgkL/ANJFf2U//Dq6D/8AHa/AH/iCS/Ye/wCjvf2rP/AD4Rf/ADEUf8QSX7D3/R3v7Vn/AIAfCL/5iKAP3+/4fh/8Ehf+kiv7Kf8A4dXQf/jtH/D8P/gkL/0kV/ZT/wDDq6D/APHa/AH/AIgkv2Hv+jvf2rP/AAA+EX/zEUf8QSX7D3/R3v7Vn/gB8Iv/AJiKAP3+/wCH4f8AwSF/6SK/sp/+HV0H/wCO0f8AD8P/AIJC/wDSRX9lP/w6ug//AB2vwB/4gkv2Hv8Ao739qz/wA+EX/wAxFH/EEl+w9/0d7+1Z/wCAHwi/+YigD9/v+H4f/BIX/pIr+yn/AOHV0H/47R/w/D/4JC/9JFf2U/8Aw6ug/wDx2vwB/wCIJL9h7/o739qz/wAAPhF/8xFH/EEl+w9/0d7+1Z/4AfCL/wCYigD9/v8Ah+H/AMEhf+kiv7Kf/h1dB/8AjtH/AA/D/wCCQv8A0kV/ZT/8OroP/wAdr8Af+IJL9h7/AKO9/as/8APhF/8AMRR/xBJfsPf9He/tWf8AgB8Iv/mIoA/f7/h+H/wSF/6SK/sp/wDh1dB/+O0f8Pw/+CQv/SRX9lP/AMOroP8A8dr8Af8AiCS/Ye/6O9/as/8AAD4Rf/MRR/xBJfsPf9He/tWf+AHwi/8AmIoA/qg/Zo/br/Y6/bJufF9l+yt+0j8Jfj5d+AINEuvGtt8MvFun+JpvDFv4jk1OLQZ9YjsnZrOPVpdF1aOxeQATtp90EJMTYK/Oj/gkT/wQy+Dv/BHrxT8ate+C/wAdviz8UNN+OmgeENI8VaB8TdI8ColjfeBdR1q88O6vo2qeF9C0a/t3jt/EviCyv7CdrizvheWlwVim06IylAH7j0UUhIUFmIAAJJJwABySSeAAOST0oAWiv4vfjH/wc6fte/Gv9sDxt+y5/wAEk/8Agne/7WGkfDjxzd/D/VPid4juvF/iDT/FOrxWYibVTF4Ek0PwZ8LfCNt4g8OfEK30fxV4v+JGuaR438M6PY+K7WbwxGl/pgZqH/Byv+3l+w3+1R8PPgn/AMFkv+Ccuj/syfDL4jtFb23xN+E+t674pGjWr/2ab3xjoF5Z6/8AEbwT8YNC8MPq+l2/jfw74F8VWvijw1HfPMI9R1y1tfCergH9otFMilSaOOaJg8UqJJG69HjdQyMPZlII9jX4I/C3/grb8YPHn/Ber9oX/gkvqHwu+G1j8Ifg78HdO+JOi/E6zufFDfEjVtQvfhb8FfHb2OqQT6o/hhLNNS+J2q2CC00qOf7Dp2nyG4Nw1z5gB++NFfBn/BTX9oL9oH9lD9iL48ftK/s0eCPAXxK+JXwP8LH4jT+AfiLbeKp9F8T+CPD1zDcePLawl8Hajp+s2viHTPC51HxBozBb2zvrnRzo11b2qamNX0zh/wDgkZ/wUF0//gpz+wd8Hf2s/wCx9B8K+MfFJ8TeFvin4F8PX8l/YeCPiT4K8QX2h63o8JnvNQvbWy1awh0fxn4ftNTvJ9UTwr4q0GXUGW7mljQA/SuivlX9uD9rPwB+wr+yZ8d/2tPiaBN4S+CXgS98TyaX9rXT5PEniK6urTQPBHg21v3huUs7/wAbeNtY8PeEdOuWtrnyr/W7ZltrhtsEnyD/AMEX/wBtz9pf/goZ+xD4d/a6/aZ+EngX4J3HxL8W+K2+FvhbwRB4kitdV+Fvhy5i0Oy8bahJ4p8Ra3qbf8JD4jsfEq6SZLTRorrQtO0/W7KC+0rWdO1O6AP1oor+Ln9j7/g6k8W/tTf8FcvCX7Ea/Cj4MaL+yt8Rvjt8S/hH8P8A4z6Vc+PNc+IHiCw03SvGdr8INZt/7L1fVPCs5+I3jLSfCVjJfJpEWjWGieJJNRuLmxt7SS+h/X7/AIL1/wDBWfxZ/wAEif2V/h58Zfhx8P8Awl8SviP8S/jJpfw38P8Ahzx4mur4TXSk8MeI/EviPUb268Oavo+p29/awaTYx6aqyy287XNws0YKxtQB+5FFfgD/AMG/3/BZbxX/AMFhPgn8dfFnxP8AAHw1+F/xU+B3xJ0Lw3q/hL4ca5r2oWlz4K8Z+HG1Xwd4qvtK8SSX2paONV1bRfGui2sp1e+ttUl8Mag8EVm1pNG+F/wXh/4Kt/tm/wDBJPS/gV8bvhL+zd8Jvjn+y94y1uTwZ8XfEXinWPGGj+L/AAF46S/iv9F0hbrRNQXTrDSvH/hf+1ofC+tv4e8Rx6Z4h8M6vHrtqqXvh6w1cA/oborhfhh8SfBXxk+HHgP4tfDfX7LxV8P/AIl+EfD3jnwX4j00ymy1vwz4o0q11nRtStxPFBcxLdWF5BI1vdW9vd2zlre7t4LiOWFPxI+Fv/BW34wePP8AgvV+0L/wSX1D4XfDax+EPwd+DunfEnRfidZ3PihviRq2oXvwt+Cvjt7HVIJ9UfwwlmmpfE7VbBBaaVHP9h07T5DcG4a58wA/fGiv5Rf+DgD/AIOAf2hf+CQ37QvwR+D3wf8Agf8ABn4o6J8UfgzP8StT1b4lXXjeHVdO1WHxv4i8LnTrCPwvr2j2n9n/AGTR7a533Ec9y1zPOu9IkjB/af4s/tS/HPSf+CXWp/tp/CTwP4B13432n7Inhn9pmx+HfiWTU/8AhAb/AFE/DnRvib4v8L/bB4j8N6pbWA0ZtdsdF1GfXUns7hLC8u4NU8qXTrwA/ROiv5nP+DfH/gvD48/4LBap+0v4H+NPww+E3wh+IvwTsPh34r8JaV8Odf8AEtxJ428FeLrjxNpHibUJNA8UXGqXkCeBtc0jw1bahrFrrMltct480aybTLKW1N1qP9MdABRX8OXib/g6M/bB8X/8FTta/wCCe37N/wCzr+zf408Naj+13d/sz/D/AMf+Jh8UbTxBeaZafEF/BN74p1vS73xp4PsWvtIittS1K9sB/Y1veNp729nMqTwTv/cYM4G7BbA3EAgE45IBJIGegJJA7nrQAtFfmp/wV9/bc8df8E6P+CeXx/8A2xvhr4O8J+PvGfwiPwu/sjwp44l1iDwxqi+O/jH8P/hvqJ1KTQbuw1UGx0vxfe6jZLa3cAfULS1SdjbtKreNf8EMf+Ck3xN/4Kp/sRz/ALUXxY+H/gT4a+KY/jN49+GqeHfh3N4gn0BtK8JaZ4TvbTUXbxJqGqaiuoXU2v3Udygu2tvLt7dokRmkWgD9kKK/NT/gr7+2546/4J0f8E8vj/8AtjfDXwd4T8feM/hEfhd/ZHhTxxLrEHhjVF8d/GP4f/DfUTqUmg3dhqoNjpfi+91GyW1u4A+oWlqk7G3aVW+Uv+CSn/BXzW/23f8AgmV8Uv8AgoZ+054O8C/CHTfhH4s+M8HivS/hxcavNo8fgr4R+EtA8VXOoWw8Y61PcSa/fxalfWlvZSatDa3dzHYQQvBJcPgA/diiv4j/AIVf8HJH/BV/9vf4w+IPD/8AwTH/AOCTmjfFL4RWWoeItP8AD3jn4ma94wg0vUbXw3c3ch1nxR8Ub3U/hR8IvBGp6tpD6POPAV7r+pappeq3q6Rb694iuLuyZvpz9g//AIONPj34k/b60v8A4Ju/8FR/2LH/AGPvj34+1zT/AA38M9Y8JSeKL7RJfFOuwQt4S8P+JPD+syeIZrvw/wCNZY71PDXxV8IeK9a8JzXsmnWV3p1vpR1HxRYAH9alFfzVf8HDP/Bbf45f8EdG/ZLHwZ+D/wAKPiqv7Qg+OP8AwkbfE658XwNoTfC0/CT+yBoi+FdZ0cONTHxC1M6k1+85Q6fYC2VN9wW/e/8AZy+JWq/Gf9nr4D/GHXdO0/R9b+K/wZ+F/wAStY0nSXuZNK0vVfHXgjQ/FGoadpkl48l2+n2V3qk1tZvdSPctbRRtO7SliQD2aivwP/4OAP8Agrb8Xv8AgkN+z78EPjB8Hvhh8N/ijrHxS+Mlz8NdW0v4lT+J4NN03TYfBOv+KEv9PPhfVtIumvmutIit2+0zyQC3kkxEZCrp+lX/AAT3/aS8TfthfsRfsu/tR+M/D+heFfFXx3+Dfg74la94c8MPqEnh/RdS8Sael7cWGkPqtxd6i1jAzbYftl1cT7eHlkI3EA+xqK/G/wD4Lnf8FJvib/wSs/Yjg/ai+E/w/wDAnxK8UyfGbwF8NX8O/ESbxBBoC6V4t0zxZe3eoo3hvUNL1FtQtZtAtY7ZDdrbeXcXDSo7LGtey/8ABIL9tzx1/wAFF/8Agnl8AP2xviV4O8J+AfGfxdPxR/tfwp4Hl1ifwxpa+BPjH8QPhvpx02TXru/1Um+0vwhZajerdXc4TULu6SBhbrEqgH6V0V/NX8Bf+C0X7TWnf8FpfGn/AASf/bg+BvwY+DGm+INO8b6t+zT8U/COt+NEm+K+nWsl14j+F1wr67d61oF2fH3gHSPEcbxed4alsvH2g3fg+FZ/EEtroM/73ftG/ErVfgx+z18ePjDoWnafrGt/Cj4M/FD4laPpOrPcx6Vqmq+BfBGueKNP07U5LN47tNPvbvS4ba8e1kS5W2lkaB1lCkAHs1FfjB/wRG/4KdeOf+Cm37B+s/te/HPwh8Nfg7f6F8V/iT4K1S08IalrFt4M03wv4B0TwvrEniPVNR8YapfTaeY4tav5dUubjUV061s7KO5YwKJiP5pv2i/+Dsv9q39pX9sHwX+yl/wST+B/w2k0fxp8V9F+Fvw6+JPxw0XxH4m8SfF/U9b1EaFZ+IIvBlhfeGoPhp4Lmvbr+1Y49VPiTxPH4f05Na1ddAubq/8AC+mgH9/FFfkhpv7Pn/BZ6XTLCbWP+Clf7HlhrMthayapp+mf8E2vFGraXZam9vG19Z6fq91+2zo13qVhb3ZlgtNSudE0q4vLdI7qbSrGWRrSL+NzwR/wdD/8FbLP9vXwh+yN8Q7v9lLWNNtf2vNA/Zx8ca14X+DfiWwXVLGD4y2nwy8Tap4dn1Dx99pshfQLe3ekS3tl59uJbd7m18xHioA/0kqKKKACoLmIz21xApAM0EsQLAkAyRsgJxzgE5OO1T0UAf5ZP7LP7QP/AAUT/wCDUX9qH49eCPjf+yZYfFT4JfG7VfDWleIfECzeIvC3gX4rp8O7Lx9cfDHxV8EfjpB4f8Q6DpRjj8d61qHiHwlrvhPWNe+wi40HWtG8Ma5pzX1j/Vr+zD/wWY/4It/8F3W8LfsvfH34XeH7b4n3vji81D4Y/s8ftbeA9H1tfE2taNoDSweJPh9400+PXfh+uvXun6jrOkWfh2TxRovjbVBaanb2egahYywSz3/GP/BxP+yd4Y/4KIftPf8ABMn/AIKG/BHRf2bfh78PdRvfB3h/4u/FzXJviP8ADn4wxaquh6j4W/4TbwG/wsi0z4feE/H/AID8QW3jDStd8Sa/4g8Lf2ZLb2+rajZpew3B/k6/4LgWH/BKX4j/ALaX7Mdn/wAEO459S/bH8XfF3w9N4tP7MYuNM+AEnjXUr/w5efC+++H0l+tp4Z0f4nW3iZ7G+u7v4WCz+HWkRw6nd+L2tPF1rqDxgH+o14W8MeHvBPhnw54M8I6PYeHvCnhHQtI8MeGNA0qBbXTND8PaBp9vpWi6Pp1rHhLaw0zTbS2srOBBsht4I414UV/Bj8D/ANsb9nDw/wD8Hk37TXi/UvidoUXgz4reE0/Zf8IeM3uILXwrN8bNC+CHwP8ACd14RvtZ1Kewt4GvPHXwv8T/AA+0W9tvttvrvjSXQdK0o3UGtW18v9Mn/BbP9uLxp+xH+wp4il+DGn6/4l/a5/aL1bRf2a/2T/BvhK3uNQ8Ya38afiYraXHrWgWWnJcahdah4I0E614r05bG2na88R6foGjtJZJq/wDaNp+Gnxm/4NRP2dPBP/BLVk+C/hDxEv8AwVN+GXwn8PfEyy+Ofhv4k/E25uPG3x08JTW3jbxT4L0Lwgvi5fBdnYa9JFq3gXwBquheG9K1/StQh8J+JpLy91G21ODVwD+zTxV4X8PeOPC/iTwV4u0ex8Q+E/F+g6x4X8T6BqkIudM1zw94g0+40nWtH1G3b5Z7HU9Nu7myu4W+WW3nkQ8NX+Sj8K/+Cd/7XHh3/gsH8e/+CPv7PP7Yfjn9kPWLX4k/FK8+GrX/AMQvjb4Y8NeO9H8PeF2+JngRtSj+GNpFLc6/4j+BSWninT/FOv6Rpmm6rpumJFDex3OqaHp95/fz/wAG+v8AwVDt/wDgpv8AsJeGdd8Z3zD9pD4AvpHwc/aE0+/1FbzWNc8RaRoln/YXxVkR44btbH4m6bFPqU7XEbeR4s07xbpS3N4umC7n/nn/AODtX4AeO/2S/wBq/wDYn/4LB/s7/wBm+GPGujeK/DvgHxxrOn6ZqEd5/wALT+GFzJ40+EvifxFPbXJsNXsPEPhGy1/wNq0c6aPdppXhPRtJa81q31WKLQAD8IP+Ckn/AAT5/wCCin7N/wC1B8Af+CbPxC/b68V/tffF79qqXwisfwj0j4ufHbXfBmjx+JvHFjovw5k8eWvxEm/su4/tPxDpWpa9biLR9SbQbTwxHrsi+bLYgf6K37bnjbwb/wAEmv8AgjL8X7z4fXuoaFo/7Lv7J2m/CH4U6tpxl0TV4vHGp6No3wY+FespdeHtH1KHRNa1D4ieJfDmoyalBpsWkafqt213dT6Zpsc17a/yzf8ABvTB41/4LFf8FkP2tf8Agr3+0h4VQ2vwP03Qrf4ReFrq/vNX8KfDzxr490nVfBfw48K+F2u9HsNM19PhZ8KPDXiWe5vo4bG7sPGPiHQ/iBqGjx+J/EdjrNv/AEHf8FsH/wCF+/Gv/glJ/wAE37fKp+1n+2R/wuXx79r40K9+DH7EnhtPjX8RNB1iG6N5oOunW57vw5Hp3hDxNoOr6P4jvLQHNhfadaXagH8an/BZz9iy1/4I7/Ff/giN8fPB3gm08N614Z+BHwquPijpXh7xBo/hfxD4s+Pf7OHjbwp8Qvi7ca7qvg7TLyztNX8Ty/FGx0e+8fwaj4nbXpBetFFNY6LAt9/T9+0nH8If+CpX/BeX9lD9njXtB8P/ABl/Zl/ZR/4J7/G74/8AiKTT7TQfFfgjxbrH7anhHSfh3odp4q1aK6ur/StLuPh7feEfG3gK5itPD/izRvEkOg+LfD98ltqlrdx4n/B4v8ANR+J//BLDQvi7pSXjp+zP+0B8P/GWuQ29zo1rY2/hn4gR6h8I5NQvk1CSG/uxB4n8Y+EtOs7HQjPemfVRd3Nm+nWV3c2fwJ/wZF/s+zaf8PP24P2qNa0nTZZfF3in4X/AzwNrd1pOpp4gstP8F6Zr3jT4h22n63cQJpl14d8Q3viv4dx3Vlp1zcXUWreCEOpR2yR6b54B+bH/AAbJ+OfF3/BPb/guN8Yv2Efi1rZ8Py/Euy+L37Omu2M0N7p+ha38Vfg3qt/4t8D6gIta1TSl0wazpfhjxba+EbvUdO1PXb658VaZ4Z060huvE9w1f37f8FNLP9h7Wf2J/jd4W/4KLeKPCHhH9k7xfodjoHxB1fxdrVzoQh1A6rZar4Qm8KXGnJPrlz4+0vxTpela94KsvD1jqmtya/pFpLaaXfrBNbv/AJ4H/Bw/4V1r/gnR/wAHA/h/9rrwLo15pmleMvEPwC/bA8O2eg2j+CbbUfEXhe80rQvidpOm+JIzrovNS8W+J/AOt+IPEniZNJV7fVfHl7DJol2bPz9R/oX/AODutviD8ev+CVX7NHxa+Ben+LvG/wADL74z+Bvi/wDEK88NafqYs0+GniL4S+LNZ8EeMPFOiz2trrmn6JbyapbzSahqWm2kfh64v4l1v+zpp40oA/C//glp/wAHJ2if8ErPEHxj/ZI8R3nxY/ba/wCCf3gzxX4pt/2R/HT6bpfgn41eEfDUPiG4bR7Y6F4s1G2tV8B+KNLmk1U+FdT1DT7/AMIawTNo1np9hql74c0/7I/4Iz/tueBf+Ci//B0P+0R+2N8NfB3izwD4M+Lv7KWs/wBkeFPHEujz+J9LbwJ8Ov2dvhvqJ1KTQLu/0oi+1Twhe6jZLa3c5TT7u1SdhcLKq/pF8Cv+Cw//AAanaJ8GPhbo8Pg/9n74Vx6V4E8MaePhx47/AGGPHXjbxp4Jaz0m2t38OeK/GemfA/x3Y+LvEOmSRtb6t4ng8aeKRr96k2qya7qUt291J8D/APBJ340/sq/tC/8AB1b+0z8Xf2JpPCcv7Mviv9lG9X4bP4H+HOofCbwww0L4V/s2+HPFv9m+AdV8MeDb/QyPG+keJBetc+HNPbVb8XWsr9rTUFvbkA+UP+D23/k+H9kL/s1O/wD/AFbvjev77P2JtK0zXv2Cv2R9D1rT7HV9G1n9kP4CaVq2lanZ22oabqemaj8GfClnf6fqFhexT2d9Y3trNLbXdndwTW1zbySQzxSRO6H+BP8A4Pbf+T4f2Qv+zU7/AP8AVu+N6/v8/YQ/5Me/Y1/7NT/Z4/8AVReEKAP8qfQPiz+2v/wQK/4K0/tVfBn9j3STq/xH0/xZ4o+CHhzwJ4s8GeIfiOPiJ8L/ABbrei+PPhNcx+FdLl0DW/Euu6n4Vk8Ia54e1KzgjkvhqMt3aWLwaisdfqL4n/4OSv8Ag438HeHtZ8U+MP2TdE8J+FtA0661TX/E3iT9iP41aHoGhaVaRNLeanq+satr9vpmm2FpErS3N5fzxWsMal5nCA11n/B3x+zv4q/ZY/4KIfsu/wDBRP4Rwf8ACNap8XtG0G8m8XadpltdLp3x9/Zl1Pw5J4e1vW0m0mfS5dQvPBV34Ah0VNeur5vEFj4I1Wxh01tN8N3nmfd3/Bxj/wAFfNF+LP8AwRO/Y40/4W+MLHQvG3/BSnQfB3jbx34a8J6wIr7Tfhh4CsLLVfi14duY7ePXbrT/AA6/xiXRPAlzZ3viTRtY1aHRvEXhu7/tuysvHWj2wB+XP/Bnd+y1qf7QH/BR34tftieMmvtetf2Wfh1quqx+INV1K5vtQufjJ+0MviXwfpmoX91N4it9W1XUL7wRa/Fy5u7zU9M8SadJcym51OXT9ck0K9k/066/md/4NOv2VYv2eP8Agkj8O/iLqem3+m+NP2rvHPjb4569Bqdje6fcw+HI9Vl+H3w3WKO41W/tb3TdY8F+DtO8baTq1jZaMt7pvjG2insZpLMahef0xUAfgD/wdE/8oNP22f8Au3j/ANam+CdfgB/wbYf8FsP+CZv7An/BOOf4D/tZftI/8Kr+Kr/tA/E/xuvhf/hUvxv8aZ8MeItJ8E2uj6l/bPw/+G/irQB9rm0jUE+yHVBfQfZ91zbQpLC0n7//APB0T/yg0/bZ/wC7eP8A1qb4J1+Of/Brb/wTT/YD/av/AOCYc/xT/aS/ZG+Bnxq+IyftH/FnwwPGnxC8DaZ4h8Qf8I/pGj+A59L0j+0LxHlFjYS6jevbQAhI2uZSBl2JAL3/AAXn/wCC8/8AwSm/bP8A+CU37Uf7Nv7Nv7UZ+Ivxo+Iv/Cmf+EN8Hf8ACmfj54U/tf8A4RT4+/C3xrr+df8AGvwu8OeGrD7B4Z8O6zqX/Ex1i0+0/Y/sloJ724t7eWP/AIIE/s6+NP2uP+DZP9r39mb4c3+gaZ49+Nvjn9qb4feD77xTcXdn4dh8Ra74J+HsGlf2xeWNnqF3Z2Mt2IoZryCxu3tVk+0fZphGUb6G/wCDh3/gld/wTj/Zv/4I/wD7W/xl+A/7Fv7Pvwo+K3hAfA4eFvH/AIJ8AaXofifw+de/aM+EfhvWW0vU7RUmtzqfh/V9V0a8A3LNp+o3cLL+8yvpn/Bng7R/8EgL10jMrp+1R8bXWMHBkZfD/wAOCqA4OC5AUHBwT0NAH823/BND/gsz+15/wbur4n/YM/bi/Yc8Wz/DV/HV746tdNuJpvh38UfDGr+I2sLfxR4n8K69qVjr3gH4yeEtW03TNN/sS20rU9B0z7XZtNa+OHguLi3X+ur9jz9uH/gi/wD8F4vE3w08b2/w6+Gvjv8Aau+A+hW3j7Rfhh8e/h1aWvxz+DMMHiuK5F94U8QXVpNoXi/S9K8R6Fo/iB774d+KPFFj4cbVNBvNfg8OatrhsH+Pf2Y/+DjL/gmv/wAFAbL43/s4f8FQfg18KP2OvFvw/wDF2q+Gp/g3+1Jqtn8XfAHimLRor7w74oe48WeIfhV4W8LeE/HfhbWm1jw9f+FdYtrXWfJnW88OapqpOs22jfz5/Azw5+wz4i/4Oav2Nz/wQ+tfiJL8INB8d2/iP4x3nh+bWbP4WaXZado3ihPj1dfCu/8AGjxeKrT4O33w3ub/AEHWIfEk0ek634h1jUvDXwwS78Oa34KsLoA+3/8Ag+W/5xmf93f/APvs1f2ofsIf8mPfsa/9mp/s8f8AqovCFfxX/wDB8t/zjM/7u/8A/fZq/ql+FP7cn7IH7Gf7EH7AKftV/tGfCj4BP8Rf2Ufgy3gVfid4ssPDDeK18J/CP4aDxK2iC+dTfDQz4k0AakYsi2Or2Akx9oTIB+A3/B7b/wAmPfshf9nWX/8A6qLxvX7/AH/BDz/lEL/wTq/7NT+FX/phir+T3/g7b/4KD/sQ/thfshfsxeD/ANl39qT4M/HfxX4U/aRu/EviPw98NfGNh4l1XR/D0nwx8YaWus39tZFzb6f/AGld2lj58jKDc3UEahtzFf2p/wCCRH/BXf8A4Jh/Br/gmJ+wx8K/in+3T+zd4E+I3gL9m74beGvGng3xJ8R9I0zX/DPiHTtFii1HRtY0+5dJrTULGbdBcwSLlJFYAsuGIBwH/B4r/wAohbb/ALOs+Cv/AKYfiRX1Z/wa7f8AKDT9ib/u4f8A9am+Nlfn7/wdT/HP4P8A7SX/AAQ48J/Gf4CfEfwn8WvhT4v/AGrvhavhjx/4H1a31zwxrreHv+Ft+GNcXTdUtWaC5OleIdG1XRr0Icwahp91bvh4mA/QL/g12/5QafsTf93D/wDrU3xsoA/OL/g64v8A/gnnoXw+8AfFvxb+0de/Av8A4Km/s+WOjeN/2P5PhLPqmp/F/WrT/hKZNT03QfF+laHcWy+FvAD65pur674e+IviHU9AuPCGv6dqN14Wn8RS6hq3grxT+bHhj/g8N8KfEb9gTx3+z7+1h+zt8R/Ef7TXjf4GfFD4O6/8U/hZP4H074ceK9T8WeCta8JaD4+1Dw7quq6TqHhjUbsapb3fi7Q9Dsb/AEdNQtr2/wDDaWNhqFp4b0fb/wCCh/j79n39lf8A4OlNW/aC/wCCrvwq8QeMP2M/Evwp8Eaf+zt4o8Z+Cb34mfDDw1e6P8HPh/pv/CUR+DNPttVTxd4e8DfE64+Iw8QeEYtE8S634d8SeNNL8cnwtL5/h++k/Qv9p7/grz/wbG+L/wBmv9obwn8L779mFviZ4o+BvxZ8O/DtdM/YG+IWg6k3jrW/AWv6b4RGn65dfs4aba6LfHX7nTxaatc6jYQadceXeTXtrHC08YBH/wAGx3w88Q/F3/g3r/aU+E/hJNPk8V/E/wAbftj/AA88Mx6tdGx0qTxD41+EXhfw3oqanerBdNZ6e2panbLe3Qtrg29sZJRBMU8tv48f+CJnjfTf+Cdv/BbP9lfWf2z9F8W/ASH4c+OvHvw++IOn+PvC2p6DrXgbXvir8H/Hnww8JXnirSNYisLzR9ATxJ458PX2ra/cR/Y9N8MzT+JVa40+3V5P7hP+DOr/AJRC3P8A2dZ8av8A0w/Devw//wCDxf8AbL+G/wAZP2iPhF/wT2+Hfwf0zxJ8b/g7qvgfxv4t+L2iPpms+N5tS+JPhnW4vDP7PtpoGl+Grrxa7Taf4q8MeO0sv+EjaK/uvEGiJZeFp7i4g1IgH+i5pmp6brem6frOjahY6vo+r2Npqelarpl3Bf6bqem39vHdWOoaffWsktre2N7ayxXNpd20slvc28sc0MjxurH/ABbmVk/4LhsrKVZf+CrJVlYFWVh+13gqwOCCCMEEZB4Nf3X/AAZ/4NcPGek/CH4Y6X8Qf+Ct3/BRzwz470/wB4TsvFnhr4YfGi5034beGPEVtoVjDqfhvwJYXcl3cw+EPD94smkeHxLcK0ul2VtKIbNZBawfwE/B7wNqXww/4KzfCz4aax4qv/Her/Dz/gol4I8Dar441WO4i1TxlqPhL9pTTNAvvFepRXd/qt1Hf+IbnT5dXvI7nVNSuEuLyRZr+8kDXEgB/tzUUUUAFFFFAHwB+2f/AMEs/wBgD/goR/Y1x+13+zJ4E+LOueH9qaP4y8/xH4H+IVnaLsxpX/CxPh1rfhLxvc6H+7Rv7BvNeuNGEg80WIl+evOf2Ov+CLn/AATH/YI+Itz8Xf2WP2UvCXw++J81i+m2fjzWfE/xE+J3ijw/Z3Fpf2F/F4Q1b4q+MPG114MbV9P1O907XJ/Cj6Pca3p0osNWlvLSKKFCigD6q+J37Hf7O3xm/aA+AP7T/wAUPAL+MvjL+y4PGb/AXXdW8U+MDoHw7vviDpQ0XxfrWneAbfX4PAGpeItW06KwjTxDrvhnVdZ0yfRtCvtIvrC/0TS7q1+m6KKAPz//AGeP+CXP7DP7J/7RPxX/AGqv2d/gn/wq342fHAeIh8VPEGg/EP4o3Hh/xd/wlXiGDxZrf2n4e6v411L4eaf5/iSBdXtho3hfThpt08/9mi0huJon90/at/ZL/Z7/AG3fgl4m/Z1/ah+HGn/FP4P+Lr3w/qWteFr3VfEHh+c6l4W1yx8RaHqOl+JPCWraD4q0DULPUtOgDXug63pt1d6dNqGjXktxo+qalY3ZRQByn7Gf7Cv7Kv8AwT6+FmofBb9kT4T2Pwi+HOreLNS8carolv4i8YeLrzUvFGrWWm6de6pf+I/HniDxP4mvGNjpGnWltbXOsSWdjBbiOxt7dXkD9vq/7LnwM139pfwj+2Bqvgj7X+0P4E+FWv8AwT8KfEA+IfFMX9lfDPxPrsfiXXPDi+F4dbj8HXTX2tRR3Z1m98P3OvQKv2W21OGzZoCUUAb37QHwB+EP7Uvwa+IP7P3x78Faf8RPhD8UtBl8N+OPB2pXOpWNvrGlSTwXkYi1PRr3Tda0jULK+tLTUdK1nRdS0/WNI1K0tNS0u+tL62guI/N/2Ov2J/2Zv2BPhA3wG/ZO+G//AAqv4VN4r1vxsfC3/CW+OPGn/FTeIoNNttY1Iaz8QPEnirXx9rh0nT1+xjVPsMHkbra2haWZpCigDwz9t7/gkn/wT4/4KN+J/A/jT9sf9nnT/i54s+HOg6j4X8I+IY/HfxQ8AarYeHdT1BdWuNGu7v4Z+NvB0mtafFqXn32n2+tnUU0q5vtTk0wWjapqP2n668Bfs/8Awh+HHwM8L/s1+H/BdnqHwR8H+BNP+GWj+AfG97q3xK02XwHpenppNl4a168+Il/4o1bxVp8emRx2Eg8T6hq8tzaosNzLMigAooA/K34r/wDBuX/wRb+M/jPUPHvjH9hbwHp3iDVN7X8fw58b/F74QeHJ5pbu7vprseDfhT8QvBvhCC9nuL2Yz3ltocNxNGIIJJGhtbeOL2H9jr/gif8A8Ezf2BPi+3x4/ZN/Zu/4VX8VX8Ka34IbxR/wtr43+NM+GPEU+m3Osab/AGN8QPiR4q0Afa5tI09/tg0sX0H2fbbXMKSzLIUUAdV+23/wSD/4J5f8FF/HPhD4kfti/AD/AIW74z8B+FH8EeFdY/4Wf8YfAv8AZfhiTV77Xn0z7D8N/H/hDTb3Oq6le3X2vULO6vh53krciBI4k+//AAH4I8MfDPwN4M+G/gnTP7F8GfD7wp4d8EeEdH+131//AGT4Y8KaRZ6DoGmfbtTubzUr37BpVhaWv2vULy6vrnyvOu7med5JWKKAPwV/4Obv2GPiH+3N/wAEv/GOg/BX4f6x8Tvjh8F/iR4A+L/w68EeFPDJ8TeNfFUFtqUng3xvoXhS2jvrS7g1BfBni7VvE8qafbaxqOrweFj4f0/R7m+1a3mtv86Twv8A8Efv+Cxnxd1z4I/Bvxp+xn+2joPgTR/Edn4D8Iat8Qfgj8Xj8P8A4N+HviF44/tTxRq/77w/LH4a8HWuv69q/jjxRFpscNv9quNb1poWv767mnKKAP8AYn+Dfwp8HfAn4SfDP4K/D3T/AOy/A3wn8CeFfh54TsWFt50OgeENEstC0w3b2dpYW099Na2Mc+oXUNlareXslxdGCNpio9JoooA+f/2o/wBlz4Gftn/Azxt+zb+0n4I/4WN8F/iL/wAI6fGPg7/hIfFPhT+1/wDhE/FWieNtA/4n3grW/DviWx+weJvDmjakP7N1i0+0fY/st159lPcW8vKfsdfsTfsy/sCfCF/gP+yb8Nv+FV/Cp/FeteN28L/8Jb438aZ8T+IrfTbXWNT/ALZ+IHiTxVr/APpcGkacn2T+1PsMH2fdbW0LSSmQooA6v9qP9lz4Gftn/Azxt+zb+0n4I/4WN8F/iL/wjp8Y+Dv+Eh8U+FP7X/4RPxVonjbQP+J94K1vw74lsfsHibw5o2pD+zdYtPtH2P7LdefZT3FvLyn7HX7E37Mv7Anwhf4D/sm/Db/hVfwqfxXrXjdvC/8AwlvjfxpnxP4it9NtdY1P+2fiB4k8Va//AKXBpGnJ9k/tT7DB9n3W1tC0kpkKKAPlb9rP/giP/wAEs/23/HsXxS/aQ/ZA8BeLPiL/AKe2o+M/Cut+PPhH4g8SzalNFcXN5401D4QeLPAk3jfURNFvttS8WnWtQsvOu1s7mBLy6Wb2P9ij/gmP+wn/AME7LLxTa/sdfs6+Evg7d+NxbxeLfEdvqXirxl411+ys5nubPSNQ8dfEHX/FfjCTQbS5ka6tdATW49Fguj9qjsFuf3tFFAFH9ub/AIJdfsM/8FJP+FZH9s/4Jf8AC4j8Hf8AhMB8Oj/wsP4p+Av+Ee/4T3/hGP8AhK/+SaeNvB39qf2r/wAId4c/5DP9ofY/7OH2D7L9pvPtHJ/tVf8ABIP/AIJ5ftseB/2f/hx+0z+z+PiR4N/Zc8J33gf4GaP/AMLO+MPhL/hCPDGpaR4M0G90wX/gfx/4b1LxCJtK+H3hG1+1+KbzW71P7JE0dylxeahLdFFAHxZ/xC7f8ENP+jJv/Nh/2pv/AJ9tH/ELt/wQ0/6Mm/8ANh/2pv8A59tFFAH2n4j/AOCQf/BPLxZ+xd4G/wCCemv/ALP4v/2Qvhv4suPHHgz4Tf8ACzvjDbf2P4nuvEXi7xZPqf8Awm9p4/g+Il+ZNf8AHXim/NpqXiy8sh/ahtlthaWljBbfVf7Ln7LnwM/Yw+Bngn9m39mzwR/wrn4L/Dr/AISI+DvB3/CQ+KfFf9kf8JZ4q1vxtr//ABPvGut+IvEt99v8TeI9Z1I/2lrF39n+2fZbXyLKC3t4iigBf2hP2U/2af2sfC8Hgv8AaX+BHwq+Ofhqz87+zdN+JvgnQfFn9jm6vtI1G8bQ7zVbKe/0J7688P6JNqD6RdWT3/8AZVjHeGeK3jRfyBP/AAa7/wDBDRiSf2JQCSTgftDftSqOTnhR8bAAPQAAAcAYoooA/VL9jr9ib9mX9gT4Qv8AAf8AZN+G3/Cq/hU/ivWvG7eF/wDhLfG/jTPifxFb6ba6xqf9s/EDxJ4q1/8A0uDSNOT7J/an2GD7PutraFpJTJ/FF/wc0/8ABC/9sn4i/tg3f/BSj9iDwd47+NsnxFg+HH/C0fAPwx/tK/8AjD8NfiN8N9C8MeAvCXjfwNoWjSx+Itb8NaloPhzwtJI/g6K51/wd4g0fVfEN/FHo97/aWllFAH6+/AD/AILrf8FA4vhT4Vs/2nf+CDv/AAUob4z6fp9rp/i/Vfgp8Fddvvh74kvrSztYrjxLo1h4ysdA13wuNYvBd3LeFJ5PEUWix+TDF4o1cOzQ/wAWvgL/AIJr/wDBTLxv/wAFRPBf7R17/wAE5v20vA/w98X/ALfHhz423dx4u+AXxAsl8H+DNf8A2hrPx3PN4n1MaENKtB4f0S7Z9avxcDT4BaXNwJhbLvoooA/11aKKKAP/2Q0KZW5kc3RyZWFtDQplbmRvYmoNCjkgMCBvYmoNCjw8DQovRmlsdGVyIFsvRENURGVjb2RlXQ0KL0RlY29kZSBbLjAwIDEuMDAgLjAwIDEuMDAgLjAwIDEuMDBdDQovQ29sb3JTcGFjZSAvRGV2aWNlUkdCDQovVHlwZSAvWE9iamVjdA0KL1N1YnR5cGUgL0ltYWdlDQovV2lkdGggMzI2DQovSGVpZ2h0IDg4DQovQml0c1BlckNvbXBvbmVudCA4DQovTGVuZ3RoIDE1NzU3DQo+Pg0Kc3RyZWFtDQr/2P/gABBKRklGAAEBAQDIAMgAAP/bAEMAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/bAEMBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIAFgBRgMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP7+KKKKACiiigAoor+ar/gsr/wcl/syf8EzItZ+Dfwgi0D9pj9sSP7TY3Pw40jW0bwB8H9Qh/smZZfjb4i0q5F7Z6hPY6lNdab4E8Ntc+Jruawe38QXPg20u7LVZwD9qv2xf22f2Zf2Cfg3q/x0/am+KegfDHwLp4vbbSl1K4SXxF418QWmk3+sweDvAXhyJv7T8WeLtTtNNuv7O0XTInkcoZrqW0s45rqL/NG/4K4/8HPX7Wf7aHxn8Oj9irx58av2LfgJ8Lf+Ev0rwyPh78U/EvhL4jfGI67qeneV40+K7eENQ0vSbTyNK0HTB4V8D2766ngubVPFBbxXr0muMbL8NP22v2/v2s/+Ch/xZufjJ+1l8Xde+JfiVTcQ+G9EfytH8B+ANKuEtIm0P4feBdKW28OeE9Okg0/T01CTTrFdV8QXFnFqvifUtb1p7jUpvjagD9K/+Hy3/BWX/pJB+2n/AOJF/FH/AOaSj/h8t/wVl/6SQftp/wDiRfxR/wDmkr81KKAP0r/4fLf8FZf+kkH7af8A4kX8Uf8A5pKP+Hy3/BWX/pJB+2n/AOJF/FH/AOaSvzk0rStU17VNN0PQ9N1DWda1nULPStH0fSrO51HVNV1TUbiOz0/TdN0+zjmu77UL67mhtbOztYZbi6uJY4II3ldVP9xX/BG7/g0h8cfEa88KftD/APBUqxvvh/8ADhT/AGnov7INld6rpXxJ8aQvHqVvA3xh8VaDquk3/wALtLhnj03VIvCfhq7v/GWsWlybTWtV8BXdpcaffAHwf/wSy0T/AIONP+CrfjCA/B39vf8AbT8C/AjSdbXS/H/7SPjv4+fF+3+HPhz7He6GmvaP4b8rxDFN4/8AH2n6VrcOq2vgrRrm286NFXWtb8O2ky6gv+lf+x3+zz4p/Zb/AGf/AAT8GvG37RPxq/ao8WeHYbi58RfGr4++IU8S+PvFOuaq63usst6Y3vbHwzHqsl4/hbw/qmreJL/w1o09toJ8R6ra6fayr7J8M/hf8OPgv4E8N/C/4R+BPCfwz+HPg6wGl+FvA/gbQNN8MeF9BsfNluHt9L0XSLa0sLUT3U9xeXUkcIlvL24ub26ea6uJpn7ugAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigArxz49ftB/BP9l74X+JvjR+0H8TvCPwk+GHhGxur7W/F3jLVYdMsE+y2N5qP9nabAd+oa/r95a2F2dI8NaDZ6n4h1ueE2ej6ZfXjJA34/8A/BX3/gvz+yL/AMErPDHiLwTNrOn/ABp/a+uNCuJfBf7PHhTUobmbQdVu7KwutD1f41axZzSf8K58LT22q2es29pdI/ivxRpiOPDeky27zavYf5fH/BQ7/gqN+2L/AMFOfindfEf9p/4m3+r6LZ391L4B+Efh0yaF8Jfhlpb3eqT6fpnhTwfayfZJ7/T7TV7rTJPGXiFta8dazY+XBrniTUI4oI4gD+hb/gsj/wAHXnxx/aa1Dxr+z3/wTzvNf/Z//Z3S81DQNR+O9pdX+j/HP4yaVBc2ZTVPDsottN1L4K+EtSe2u4otNsprnx3reiXML67q3hn7fqng62/jdd3kd5JGaSSRmd3dizu7EszuzEszMxJZiSSSSTmm0UAFFFFABX3H+wZ/wTm/a4/4KTfFofCD9lD4Xah411Ow+xz+M/GWoNNo3w0+Gel38eoS2Ws/EbxtJbT6d4as79dJ1KPSLV1uda8QXdnPp/h3StW1Bfsp/bn/AIIy/wDBsV+0Z/wUJTwn8f8A9pm4139mn9kS41EXlodR0a4tvjN8Z9M0zUNKa5tPAXhvWI7OPwn4R8Q2M2q2lh8U9ei1CBbmx+0aB4R8UWUwvYf9M39ln9kT9m39ij4TaL8EP2W/hD4T+Dvw20NSYtF8N21xLf6teO8ssuseKvE2rXGo+KPGOvzvNJ5+veKtZ1jV5IykDXn2eKGKMA/HL/gjx/wbr/snf8EvNE0f4i+MrTQf2kv2wnElzqPx18TeHZINE8C+dc6dfW2hfBzwTquo6zp3hX+xbjTbfy/iDPG/xC1i5fUbiPUvDuh6knhLTv6HKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAOE+JnxQ+HHwX8CeJPih8XPHfhP4Z/DnwdYHVPFPjjxzr+m+GPC+g2PmxW6XGqa1q9zaWFqJ7qe3s7VJJhLeXtxbWVqk11cQwv/n7f8Fkv+DuHWviDZeKf2df+CWV1rngzwpdM2l+Iv2wdV07UvD3jrW7ZJtLumX4HeGNXhstU8D2N0ItT0m+8X+M9JTxbPaXLTeHdD8I31vaa5P/AEmf8FM/+CE0X/BVTxhZ6l+0P+3r+1BoXwz8N3N+/gX4D/DXTfht4b+FXhmO71CW9hv9R0efw7qFx4z8WW8LW+njxd4pub/U47Ozhg01NMhkuop/yn/4gkv2Hv8Ao739qz/wA+EX/wAxFAH+bRq+r6t4g1XUtd17VNR1vW9ZvrrVNX1nV7251LVdV1O+ne5vtR1LULyWa7vr68uZZLi6u7qaW4uJ5HlmkeR2Y51f6VH/ABBJfsPf9He/tWf+AHwi/wDmIo/4gkv2Hv8Ao739qz/wA+EX/wAxFAH+avRX+lR/xBJfsPf9He/tWf8AgB8Iv/mIo/4gkv2Hv+jvf2rP/AD4Rf8AzEUAf5tGkabNrWraXo9vcadaT6tqNlpkF3q+pWOjaTazX9zFax3Gqaxqc9rpuladC8qyXupahc29jY2yy3V3PDBFJIv9z/8AwRx/YR/4II/saTaR8df29v8Agop+wn+1H+0hbmz1Lwz8PLf4k6frXwD+DWoRDUoZmjtNXuLay+NPiSa1urOY6v4w8KWnhjw1qduT4b8PX2o6dp3jOf74/wCIJL9h7/o739qz/wAAPhF/8xFH/EEl+w9/0d7+1Z/4AfCL/wCYigD9/V/4Lhf8Eg1AVf8Agop+ykqqAqqvxU0EBQBgAAS4AA4AHAFL/wAPw/8AgkL/ANJFf2U//Dq6D/8AHa/AH/iCS/Ye/wCjvf2rP/AD4Rf/ADEUf8QSX7D3/R3v7Vn/AIAfCL/5iKAP3+/4fh/8Ehf+kiv7Kf8A4dXQf/jtH/D8P/gkL/0kV/ZT/wDDq6D/APHa/AH/AIgkv2Hv+jvf2rP/AAA+EX/zEUf8QSX7D3/R3v7Vn/gB8Iv/AJiKAP3+/wCH4f8AwSF/6SK/sp/+HV0H/wCO0f8AD8P/AIJC/wDSRX9lP/w6ug//AB2vwB/4gkv2Hv8Ao739qz/wA+EX/wAxFH/EEl+w9/0d7+1Z/wCAHwi/+YigD9/v+H4f/BIX/pIr+yn/AOHV0H/47R/w/D/4JC/9JFf2U/8Aw6ug/wDx2vwB/wCIJL9h7/o739qz/wAAPhF/8xFH/EEl+w9/0d7+1Z/4AfCL/wCYigD9/v8Ah+H/AMEhf+kiv7Kf/h1dB/8AjtH/AA/D/wCCQv8A0kV/ZT/8OroP/wAdr8Af+IJL9h7/AKO9/as/8APhF/8AMRR/xBJfsPf9He/tWf8AgB8Iv/mIoA/f7/h+H/wSF/6SK/sp/wDh1dB/+O0f8Pw/+CQv/SRX9lP/AMOroP8A8dr8Af8AiCS/Ye/6O9/as/8AAD4Rf/MRR/xBJfsPf9He/tWf+AHwi/8AmIoA/f7/AIfh/wDBIX/pIr+yn/4dXQf/AI7R/wAPw/8AgkL/ANJFf2U//Dq6D/8AHa/AH/iCS/Ye/wCjvf2rP/AD4Rf/ADEUf8QSX7D3/R3v7Vn/AIAfCL/5iKAP3+/4fh/8Ehf+kiv7Kf8A4dXQf/jtH/D8P/gkL/0kV/ZT/wDDq6D/APHa/AH/AIgkv2Hv+jvf2rP/AAA+EX/zEUf8QSX7D3/R3v7Vn/gB8Iv/AJiKAP3+/wCH4f8AwSF/6SK/sp/+HV0H/wCO0f8AD8P/AIJC/wDSRX9lP/w6ug//AB2vwB/4gkv2Hv8Ao739qz/wA+EX/wAxFH/EEl+w9/0d7+1Z/wCAHwi/+YigD9/v+H4f/BIX/pIr+yn/AOHV0H/47R/w/D/4JC/9JFf2U/8Aw6ug/wDx2vwB/wCIJL9h7/o739qz/wAAPhF/8xFH/EEl+w9/0d7+1Z/4AfCL/wCYigD9/v8Ah+H/AMEhf+kiv7Kf/h1dB/8AjtH/AA/D/wCCQv8A0kV/ZT/8OroP/wAdr8Af+IJL9h7/AKO9/as/8APhF/8AMRR/xBJfsPf9He/tWf8AgB8Iv/mIoA/f7/h+H/wSF/6SK/sp/wDh1dB/+O0f8Pw/+CQv/SRX9lP/AMOroP8A8dr8Af8AiCS/Ye/6O9/as/8AAD4Rf/MRR/xBJfsPf9He/tWf+AHwi/8AmIoA/f7/AIfh/wDBIX/pIr+yn/4dXQf/AI7R/wAPw/8AgkL/ANJFf2U//Dq6D/8AHa/AH/iCS/Ye/wCjvf2rP/AD4Rf/ADEUf8QSX7D3/R3v7Vn/AIAfCL/5iKAP3+/4fh/8Ehf+kiv7Kf8A4dXQf/jtH/D8P/gkL/0kV/ZT/wDDq6D/APHa/AH/AIgkv2Hv+jvf2rP/AAA+EX/zEUf8QSX7D3/R3v7Vn/gB8Iv/AJiKAP3+/wCH4f8AwSF/6SK/sp/+HV0H/wCO0f8AD8P/AIJC/wDSRX9lP/w6ug//AB2vwB/4gkv2Hv8Ao739qz/wA+EX/wAxFH/EEl+w9/0d7+1Z/wCAHwi/+YigD9/v+H4f/BIX/pIr+yn/AOHV0H/47R/w/D/4JC/9JFf2U/8Aw6ug/wDx2vwB/wCIJL9h7/o739qz/wAAPhF/8xFH/EEl+w9/0d7+1Z/4AfCL/wCYigD+qD9mj9uv9jr9sm58X2X7K37SPwl+Pl34Ag0S68a23wy8W6f4mm8MW/iOTU4tBn1iOydms49Wl0XVo7F5ABO2n3QQkxNgr86P+CRP/BDL4O/8EevFPxq174L/AB2+LPxQ0346aB4Q0jxVoHxN0jwKiWN94F1HWrzw7q+jap4X0LRr+3eO38S+ILK/sJ2uLO+F5aXBWKbTojKUAfuPRRSEhQWYgAAkknAAHJJJ4AA5JPSgBaK/i9+Mf/Bzp+178a/2wPG37Ln/AAST/wCCd7/tYaR8OPHN38P9U+J3iO68X+INP8U6vFZiJtVMXgSTQ/Bnwt8I23iDw58QrfR/FXi/4ka5pHjfwzo9j4rtZvDEaX+mBmof8HK/7eX7Df7VHw8+Cf8AwWS/4Jy6P+zJ8MviO0VvbfE34T63rvikaNav/ZpvfGOgXlnr/wARvBPxg0Lww+r6Xb+N/DvgXxVa+KPDUd88wj1HXLW18J6uAf2i0UyKVJo45omDxSokkbr0eN1DIw9mUgj2Nfgj8Lf+Ctvxg8ef8F6v2hf+CS+ofC74bWPwh+Dvwd074k6L8TrO58UN8SNW1C9+FvwV8dvY6pBPqj+GEs01L4narYILTSo5/sOnafIbg3DXPmAH740V8Gf8FNf2gv2gf2UP2Ivjx+0r+zR4I8BfEr4lfA/wsfiNP4B+Itt4qn0XxP4I8PXMNx48trCXwdqOn6za+IdM8LnUfEGjMFvbO+udHOjXVvapqY1fTOH/AOCRn/BQXT/+CnP7B3wd/az/ALH0Hwr4x8UnxN4W+KfgXw9fyX9h4I+JPgrxBfaHrejwme81C9tbLVrCHR/Gfh+01O8n1RPCvirQZdQZbuaWNAD9K6K+Vf24P2s/AH7Cv7Jnx3/a0+JoE3hL4JeBL3xPJpf2tdPk8SeIrq6tNA8EeDbW/eG5Szv/ABt421jw94R065a2ufKv9btmW2uG2wSfIP8AwRf/AG3P2l/+Chn7EPh39rr9pn4SeBfgncfEvxb4rb4W+FvBEHiSK11X4W+HLmLQ7LxtqEninxFrept/wkPiOx8SrpJktNGiutC07T9bsoL7StZ07U7oA/Wiiv4uf2Pv+DqTxb+1N/wVy8JfsRr8KPgxov7K3xG+O3xL+Efw/wDjPpVz481z4geILDTdK8Z2vwg1m3/svV9U8Kzn4jeMtJ8JWMl8mkRaNYaJ4kk1G4ubG3tJL6H9fv8AgvX/AMFZ/Fn/AASJ/ZX+Hnxl+HHw/wDCXxK+I/xL+Mml/Dfw/wCHPHia6vhNdKTwx4j8S+I9Rvbrw5q+j6nb39rBpNjHpqrLLbztc3CzRgrG1AH7kUV+AP8Awb/f8FlvFf8AwWE+Cfx18WfE/wAAfDX4X/FT4HfEnQvDer+EvhxrmvahaXPgrxn4cbVfB3iq+0rxJJfalo41XVtF8a6LaynV7621SXwxqDwRWbWk0b4X/BeH/gq3+2b/AMEk9L+BXxu+Ev7N3wm+Of7L3jLW5PBnxd8ReKdY8YaP4v8AAXjpL+K/0XSFutE1BdOsNK8f+F/7Wh8L62/h7xHHpniHwzq8eu2qpe+HrDVwD+huiuF+GHxJ8FfGT4ceA/i18N9fsvFXw/8AiX4R8PeOfBfiPTTKbLW/DPijSrXWdG1K3E8UFzEt1YXkEjW91b293bOWt7u3guI5YU/Ej4W/8FbfjB48/wCC9X7Qv/BJfUPhd8NrH4Q/B34O6d8SdF+J1nc+KG+JGrahe/C34K+O3sdUgn1R/DCWaal8TtVsEFppUc/2HTtPkNwbhrnzAD98aK/lF/4OAP8Ag4B/aF/4JDftC/BH4PfB/wCB/wAGfijonxR+DM/xK1PVviVdeN4dV07VYfG/iLwudOsI/C+vaPaf2f8AZNHtrnfcRz3LXM8670iSMH9p/iz+1L8c9J/4Jdan+2n8JPA/gHXfjfafsieGf2mbH4d+JZNT/wCEBv8AUT8OdG+Jvi/wv9sHiPw3qltYDRm12x0XUZ9dSezuEsLy7g1TypdOvAD9E6K/mc/4N8f+C8Pjz/gsFqn7S/gf40/DD4TfCH4i/BOw+HfivwlpXw51/wAS3EnjbwV4uuPE2keJtQk0DxRcapeQJ4G1zSPDVtqGsWusyW1y3jzRrJtMspbU3Wo/0x0AFFfw5eJv+Doz9sHxf/wVO1r/AIJ7fs3/ALOv7N/jTw1qP7Xd3+zP8P8Ax/4mHxRtPEF5plp8QX8E3vinW9LvfGng+xa+0iK21LUr2wH9jW942nvb2cypPBO/9xgzgbsFsDcQCATjkgEkgZ6AkkDuetAC0V+an/BX39tzx1/wTo/4J5fH/wDbG+Gvg7wn4+8Z/CI/C7+yPCnjiXWIPDGqL47+Mfw/+G+onUpNBu7DVQbHS/F97qNktrdwB9QtLVJ2Nu0qt41/wQx/4KTfE3/gqn+xHP8AtRfFj4f+BPhr4pj+M3j34ap4d+Hc3iCfQG0rwlpnhO9tNRdvEmoapqK6hdTa/dR3KC7a28u3t2iRGaRaAP2Qor81P+Cvv7bnjr/gnR/wTy+P/wC2N8NfB3hPx94z+ER+F39keFPHEusQeGNUXx38Y/h/8N9ROpSaDd2Gqg2Ol+L73UbJbW7gD6haWqTsbdpVb5S/4JKf8FfNb/bd/wCCZXxS/wCChn7Tng7wL8IdN+Efiz4zweK9L+HFxq82jx+CvhH4S0DxVc6hbDxjrU9xJr9/FqV9aW9lJq0Nrd3MdhBC8Elw+AD92KK/iP8AhV/wckf8FX/29/jD4g8P/wDBMf8A4JOaN8UvhFZah4i0/wAPeOfiZr3jCDS9RtfDdzdyHWfFHxRvdT+FHwi8Eanq2kPo848BXuv6lqml6rerpFvr3iK4u7Jm+nP2D/8Ag40+PfiT9vrS/wDgm7/wVH/Ysf8AY++Pfj7XNP8ADfwz1jwlJ4ovtEl8U67BC3hLw/4k8P6zJ4hmu/D/AI1ljvU8NfFXwh4r1rwnNeyadZXenW+lHUfFFgAf1qUV/NV/wcM/8Ft/jl/wR0b9ksfBn4P/AAo+Kq/tCD44/wDCRt8TrnxfA2hN8LT8JP7IGiL4V1nRw41MfELUzqTX7zlDp9gLZU33Bb97/wBnL4lar8Z/2evgP8Ydd07T9H1v4r/Bn4X/ABK1jSdJe5k0rS9V8deCND8Uahp2mSXjyXb6fZXeqTW1m91I9y1tFG07tKWJAPZqK/A//g4A/wCCtvxe/wCCQ37PvwQ+MHwe+GHw3+KOsfFL4yXPw11bS/iVP4ng03TdNh8E6/4oS/08+F9W0i6a+a60iK3b7TPJALeSTERkKun6Vf8ABPf9pLxN+2F+xF+y7+1H4z8P6F4V8VfHf4N+DviVr3hzww+oSeH9F1LxJp6XtxYaQ+q3F3qLWMDNth+2XVxPt4eWQjcQD7Gor8b/APgud/wUm+Jv/BKz9iOD9qL4T/D/AMCfErxTJ8ZvAXw1fw78RJvEEGgLpXi3TPFl7d6ijeG9Q0vUW1C1m0C1jtkN2tt5dxcNKjssa17L/wAEgv23PHX/AAUX/wCCeXwA/bG+JXg7wn4B8Z/F0/FH+1/CngeXWJ/DGlr4E+MfxA+G+nHTZNeu7/VSb7S/CFlqN6t1dzhNQu7pIGFusSqAfpXRX81fwF/4LRftNad/wWl8af8ABJ/9uD4G/Bj4Mab4g07xvq37NPxT8I6340Sb4r6dayXXiP4XXCvrt3rWgXZ8feAdI8RxvF53hqWy8faDd+D4Vn8QS2ugz/vd+0b8StV+DH7PXx4+MOhadp+sa38KPgz8UPiVo+k6s9zHpWqar4F8Ea54o0/TtTks3ju00+9u9Lhtrx7WRLlbaWRoHWUKQAezUV+MH/BEb/gp145/4KbfsH6z+178c/CHw1+Dt/oXxX+JPgrVLTwhqWsW3gzTfC/gHRPC+sSeI9U1Hxhql9Np5ji1q/l1S5uNRXTrWzso7ljAomI/mm/aL/4Oy/2rf2lf2wfBf7KX/BJP4H/DaTR/GnxX0X4W/Dr4k/HDRfEfibxJ8X9T1vURoVn4gi8GWF94ag+Gngua9uv7Vjj1U+JPE8fh/Tk1rV10C5ur/wAL6aAf38UV+SGm/s+f8FnpdMsJtY/4KV/seWGsy2FrJqmn6Z/wTa8Uatpdlqb28bX1np+r3X7bOjXepWFvdmWC01K50TSri8t0juptKsZZGtIv43PBH/B0P/wVss/29fCH7I3xDu/2UtY021/a80D9nHxxrXhf4N+JbBdUsYPjLafDLxNqnh2fUPH32myF9At7d6RLe2Xn24lt3ubXzEeKgD/SSooooAKguYjPbXECkAzQSxAsCQDJGyAnHOATk47VPRQB/lk/ss/tA/8ABRP/AINRf2ofj14I+N/7Jlh8VPgl8btV8NaV4h8QLN4i8LeBfiunw7svH1x8MfFXwR+OkHh/xDoOlGOPx3rWoeIfCWu+E9Y177CLjQda0bwxrmnNfWP9Wv7MP/BZj/gi3/wXdbwt+y98ffhd4ftvife+OLzUPhj+zx+1t4D0fW18Ta1o2gNLB4k+H3jTT49d+H669e6fqOs6RZ+HZPFGi+NtUFpqdvZ6BqFjLBLPf8Y/8HE/7J3hj/goh+09/wAEyf8Agob8EdF/Zt+Hvw91G98HeH/i78XNcm+I/wAOfjDFqq6HqPhb/hNvAb/CyLTPh94T8f8AgPxBbeMNK13xJr/iDwt/Zktvb6tqNml7DcH+Tr/guBYf8EpfiP8Atpfsx2f/AAQ7jn1L9sfxd8XfD03i0/sxi40z4ASeNdSv/Dl58L774fSX62nhnR/idbeJnsb67u/hYLP4daRHDqd34va08XWuoPGAf6jXhbwx4e8E+GfDngzwjo9h4e8KeEdC0jwx4Y0DSoFtdM0Pw9oGn2+laLo+nWseEtrDTNNtLays4EGyG3gjjXhRX8GPwP8A2xv2cPD/APweTftNeL9S+J2hReDPit4TT9l/wh4ze4gtfCs3xs0L4IfA/wAJ3XhG+1nUp7C3ga88dfC/xP8AD7Rb22+22+u+NJdB0rSjdQa1bXy/0yf8Fs/24vGn7Ef7CniKX4Mafr/iX9rn9ovVtF/Zr/ZP8G+Ere41Dxhrfxp+JitpcetaBZaclxqF1qHgjQTrXivTlsbadrzxHp+gaO0lkmr/ANo2n4afGb/g1E/Z08E/8EtWT4L+EPES/wDBU34ZfCfw98TLL45+G/iT8Tbm48bfHTwlNbeNvFPgvQvCC+Ll8F2dhr0kWreBfAGq6F4b0rX9K1CHwn4mkvL3UbbU4NXAP7NPFXhfw9448L+JPBXi7R7HxD4T8X6DrHhfxPoGqQi50zXPD3iDT7jSda0fUbdvlnsdT027ubK7hb5ZbeeRDw1f5KPwr/4J3/tceHf+Cwfx7/4I+/s8/th+Of2Q9YtfiT8Urz4atf8AxC+Nvhjw1470fw94Xb4meBG1KP4Y2kUtzr/iP4FJaeKdP8U6/pGmabqum6YkUN7Hc6poen3n9/P/AAb6/wDBUO3/AOCm/wCwl4Z13xnfMP2kPgC+kfBz9oTT7/UVvNY1zxFpGiWf9hfFWRHjhu1sfibpsU+pTtcRt5HizTvFulLc3i6YLuf+ef8A4O1fgB47/ZL/AGr/ANif/gsH+zv/AGb4Y8a6N4r8O+AfHGs6fpmoR3n/AAtP4YXMnjT4S+J/EU9tcmw1ew8Q+EbLX/A2rRzpo92mleE9G0lrzWrfVYotAAPwg/4KSf8ABPn/AIKKfs3/ALUHwB/4Js/EL9vrxX+198Xv2qpfCKx/CPSPi58dtd8GaPH4m8cWOi/DmTx5a/ESb+y7j+0/EOlalr1uItH1JtBtPDEeuyL5stiB/orftueNvBv/AASa/wCCMvxfvPh9e6hoWj/su/snab8IfhTq2nGXRNXi8cano2jfBj4V6yl14e0fUodE1rUPiJ4l8OajJqUGmxaRp+q3bXd1PpmmxzXtr/LN/wAG9MHjX/gsV/wWQ/a1/wCCvf7SHhVDa/A/TdCt/hF4Wur+81fwp8PPGvj3SdV8F/Djwr4Xa70ew0zX0+Fnwo8NeJZ7m+jhsbuw8Y+IdD+IGoaPH4n8R2Os2/8AQd/wWwf/AIX78a/+CUn/AATft8qn7Wf7ZH/C5fHv2vjQr34MfsSeG0+NfxE0HWIbo3mg66dbnu/DkeneEPE2g6vo/iO8tAc2F9p1pdqAfxqf8FnP2LLX/gjv8V/+CI3x88HeCbTw3rXhn4EfCq4+KOleHvEGj+F/EPiz49/s4eNvCnxC+Ltxruq+DtMvLO01fxPL8UbHR77x/BqPidtekF60UU1josC339P37Scfwh/4Klf8F5f2UP2eNe0Hw/8AGX9mX9lH/gnv8bvj/wCIpNPtNB8V+CPFusftqeEdJ+Heh2nirVorq6v9K0u4+Ht94R8beArmK08P+LNG8SQ6D4t8P3yW2qWt3Hif8Hi/wA1H4n/8EsNC+LulJeOn7M/7QHw/8Za5Db3OjWtjb+GfiBHqHwjk1C+TUJIb+7EHifxj4S06zsdCM96Z9VF3c2b6dZXdzZ/An/BkX+z7Np/w8/bg/ao1rSdNll8XeKfhf8DPA2t3Wk6mniCy0/wXpmveNPiHbafrdxAmmXXh3xDe+K/h3HdWWnXNxdRat4IQ6lHbJHpvngH5sf8ABsn458Xf8E9v+C43xi/YR+LWtnw/L8S7L4vfs6a7YzQ3un6FrfxV+Deq3/i3wPqAi1rVNKXTBrOl+GPFtr4Ru9R07U9dvrnxVpnhnTrSG68T3DV/ft/wU0s/2HtZ/Yn+N3hb/got4o8IeEf2TvF+h2OgfEHV/F2tXOhCHUDqtlqvhCbwpcack+uXPj7S/FOl6Vr3gqy8PWOqa3Jr+kWktppd+sE1u/8Angf8HD/hXWv+CdH/AAcD+H/2uvAujXmmaV4y8Q/AL9sDw7Z6DaP4JttR8ReF7zStC+J2k6b4kjOui81Lxb4n8A634g8SeJk0lXt9V8eXsMmiXZs/P1H+hf8A4O62+IPx6/4JVfs0fFr4F6f4u8b/AAMvvjP4G+L/AMQrzw1p+pizT4aeIvhL4s1nwR4w8U6LPa2uuafolvJqlvNJqGpabaR+Hri/iXW/7OmnjSgD8L/+CWn/AAcnaJ/wSs8QfGP9kjxHefFj9tr/AIJ/eDPFfim3/ZH8dPpul+CfjV4R8NQ+IbhtHtjoXizUba1XwH4o0uaTVT4V1PUNPv8AwhrBM2jWen2GqXvhzT/sj/gjP+254F/4KL/8HQ/7RH7Y3w18HeLPAPgz4u/spaz/AGR4U8cS6PP4n0tvAnw6/Z2+G+onUpNAu7/SiL7VPCF7qNktrdzlNPu7VJ2Fwsqr+kXwK/4LD/8ABqdonwY+Fujw+D/2fvhXHpXgTwxp4+HHjv8AYY8deNvGnglrPSba3fw54r8Z6Z8D/Hdj4u8Q6ZJG1vq3ieDxp4pGv3qTarJrupS3b3UnwP8A8EnfjT+yr+0L/wAHVv7TPxd/Ymk8Jy/sy+K/2Ub1fhs/gf4c6h8JvDDDQvhX+zb4c8W/2b4B1Xwx4Nv9DI8b6R4kF61z4c09tVvxdayv2tNQW9uQD5Q/4Pbf+T4f2Qv+zU7/AP8AVu+N6/vs/Ym0rTNe/YK/ZH0PWtPsdX0bWf2Q/gJpWraVqdnbahpup6ZqPwZ8KWd/p+oWF7FPZ31je2s0ttd2d3BNbXNvJJDPFJE7of4E/wDg9t/5Ph/ZC/7NTv8A/wBW743r+/z9hD/kx79jX/s1P9nj/wBVF4QoA/yp9A+LP7a//BAr/grT+1V8Gf2PdJOr/EfT/Fnij4IeHPAnizwZ4h+I4+Inwv8AFut6L48+E1zH4V0uXQNb8S67qfhWTwhrnh7UrOCOS+Goy3dpYvBqKx1+ovif/g5K/wCDjfwd4e1nxT4w/ZN0Twn4W0DTrrVNf8TeJP2I/jVoegaFpVpE0t5qer6xq2v2+mabYWkStLc3l/PFawxqXmcIDXWf8HfH7O/ir9lj/goh+y7/AMFE/hHB/wAI1qnxe0bQbybxdp2mW10unfH39mXU/Dknh7W9bSbSZ9Ll1C88FXfgCHRU166vm8QWPgjVbGHTW03w3eeZ93f8HGP/AAV80X4s/wDBE79jjT/hb4wsdC8bf8FKdB8HeNvHfhrwnrAivtN+GHgKwstV+LXh25jt49dutP8ADr/GJdE8CXNne+JNG1jVodG8ReG7v+27Ky8daPbAH5c/8Gd37LWp/tAf8FHfi1+2J4ya+161/ZZ+HWq6rH4g1XUrm+1C5+Mn7Qy+JfB+mahf3U3iK31bVdQvvBFr8XLm7vNT0zxJp0lzKbnU5dP1yTQr2T/Trr+Z3/g06/ZVi/Z4/wCCSPw7+Iup6bf6b40/au8c+Nvjnr0Gp2N7p9zD4cj1WX4ffDdYo7jVb+1vdN1jwX4O07xtpOrWNloy3um+MbaKexmksxqF5/TFQB+AP/B0T/yg0/bZ/wC7eP8A1qb4J1+AH/Bth/wWw/4Jm/sCf8E45/gP+1l+0j/wqv4qv+0D8T/G6+F/+FS/G/xpnwx4i0nwTa6PqX9s/D/4b+KtAH2ubSNQT7IdUF9B9n3XNtCksLSfv/8A8HRP/KDT9tn/ALt4/wDWpvgnX45/8Gtv/BNP9gP9q/8A4Jhz/FP9pL9kb4GfGr4jJ+0f8WfDA8afELwNpniHxB/wj+kaP4Dn0vSP7QvEeUWNhLqN69tACEja5lIGXYkAvf8ABef/AILz/wDBKb9s/wD4JTftR/s2/s2/tRn4i/Gj4i/8KZ/4Q3wd/wAKZ+PnhT+1/wDhFPj78LfGuv51/wAa/C7w54asPsHhnw7rOpf8THWLT7T9j+yWgnvbi3t5Y/8AggT+zr40/a4/4Nk/2vf2Zvhzf6Bpnj342+Of2pvh94PvvFNxd2fh2HxFrvgn4ewaV/bF5Y2eoXdnYy3YihmvILG7e1WT7R9mmEZRvob/AIOHf+CV3/BOP9m//gj/APtb/GX4D/sW/s+/Cj4reEB8Dh4W8f8AgnwBpeh+J/D5179oz4R+G9ZbS9TtFSa3Op+H9X1XRrwDcs2n6jdwsv7zK+mf8GeDtH/wSAvXSMyun7VHxtdYwcGRl8P/AA4KoDg4LkBQcHBPQ0Afzbf8E0P+CzP7Xn/Bu6vif9gz9uL9hzxbP8NX8dXvjq1024mm+HfxR8Mav4jawt/FHifwrr2pWOveAfjJ4S1bTdM03+xLbStT0HTPtdm01r44eC4uLdf66v2PP24f+CL/APwXi8TfDTxvb/Dr4a+O/wBq74D6FbePtF+GHx7+HVpa/HP4MwweK4rkX3hTxBdWk2heL9L0rxHoWj+IHvvh34o8UWPhxtU0G81+Dw5q2uGwf49/Zj/4OMv+Ca//AAUBsvjf+zh/wVB+DXwo/Y68W/D/AMXar4an+Df7Umq2fxd8AeKYtGivvDvih7jxZ4h+FXhbwt4T8d+FtabWPD1/4V1i2tdZ8mdbzw5qmqk6zbaN/Pn8DPDn7DPiL/g5q/Y3P/BD61+Ikvwg0Hx3b+I/jHeeH5tZs/hZpdlp2jeKE+PV18K7/wAaPF4qtPg7ffDe5v8AQdYh8STR6TrfiHWNS8NfDBLvw5rfgqwugD7f/wCD5b/nGZ/3d/8A++zV/ah+wh/yY9+xr/2an+zx/wCqi8IV/Ff/AMHy3/OMz/u7/wD99mr+qX4U/tyfsgfsZ/sQfsAp+1X+0Z8KPgE/xF/ZR+DLeBV+J3iyw8MN4rXwn8I/hoPEraIL51N8NDPiTQBqRiyLY6vYCTH2hMgH4Df8Htv/ACY9+yF/2dZf/wDqovG9fv8Af8EPP+UQv/BOr/s1P4Vf+mGKv5Pf+Dtv/goP+xD+2F+yF+zF4P8A2Xf2pPgz8d/FfhT9pG78S+I/D3w18Y2HiXVdH8PSfDHxhpa6zf21kXNvp/8AaV3aWPnyMoNzdQRqG3MV/an/AIJEf8Fd/wDgmH8Gv+CYn7DHwr+Kf7dP7N3gT4jeAv2bvht4a8aeDfEnxH0jTNf8M+IdO0WKLUdG1jT7l0mtNQsZt0FzBIuUkVgCy4YgHAf8Hiv/ACiFtv8As6z4K/8Aph+JFfVn/Brt/wAoNP2Jv+7h/wD1qb42V+fv/B1P8c/g/wDtJf8ABDjwn8Z/gJ8R/Cfxa+FPi/8Aau+Fq+GPH/gfVrfXPDGut4e/4W34Y1xdN1S1ZoLk6V4h0bVdGvQhzBqGn3Vu+HiYD9Av+DXb/lBp+xN/3cP/AOtTfGygD84v+Dri/wD+CeehfD7wB8W/Fv7R178C/wDgqb+z5Y6N43/Y/k+Es+qan8X9atP+Epk1PTdB8X6VodxbL4W8APrmm6vrvh74i+IdT0C48Ia/p2o3XhafxFLqGreCvFP5seGP+Dw3wp8Rv2BPHf7Pv7WH7O3xH8R/tNeN/gZ8UPg7r/xT+Fk/gfTvhx4r1PxZ4K1rwloPj7UPDuq6rpOoeGNRuxqlvd+LtD0Oxv8AR01C2vb/AMNpY2GoWnhvR9v/AIKH+Pv2ff2V/wDg6U1b9oL/AIKu/CrxB4w/Yz8S/CnwRp/7O3ijxn4JvfiZ8MPDV7o/wc+H+m/8JRH4M0+21VPF3h7wN8Trj4jDxB4Ri0TxLrfh3xJ400vxyfC0vn+H76T9C/2nv+CvP/Bsb4v/AGa/2hvCfwvvv2YW+Jnij4G/Fnw78O10z9gb4haDqTeOtb8Ba/pvhEafrl1+zhptrot8dfudPFpq1zqNhBp1x5d5Ne2scLTxgEf/AAbHfDzxD8Xf+Dev9pT4T+Ek0+TxX8T/ABt+2P8ADzwzHq10bHSpPEPjX4ReF/Deipqd6sF01np7alqdst7dC2uDb2xklEExTy2/jx/4ImeN9N/4J2/8Fs/2V9Z/bP0Xxb8BIfhz468e/D74g6f4+8LanoOteBte+Kvwf8efDDwleeKtI1iKwvNH0BPEnjnw9fatr9xH9j03wzNP4lVrjT7dXk/uE/4M6v8AlELc/wDZ1nxq/wDTD8N6/D//AIPF/wBsv4b/ABk/aI+EX/BPb4d/B/TPEnxv+Duq+B/G/i34vaI+maz43m1L4k+Gdbi8M/s+2mgaX4auvFrtNp/irwx47Sy/4SNor+68QaIll4WnuLiDUiAf6Lmmanput6bp+s6NqFjq+j6vY2mp6VqumXcF/pup6bf28d1Y6hp99ayS2t7Y3trLFc2l3bSyW9zbyxzQyPG6sf8AFuZWT/guGyspVl/4KslWVgVZWH7XeCrA4IIIwQRkHg1/df8ABn/g1w8Z6T8IfhjpfxB/4K3f8FHPDPjvT/AHhOy8WeGvhh8aLnTfht4Y8RW2hWMOp+G/AlhdyXdzD4Q8P3iyaR4fEtwrS6XZW0ohs1kFrB/AT8HvA2pfDD/grN8LPhprHiq/8d6v8PP+CiXgjwNqvjjVY7iLVPGWo+Ev2lNM0C+8V6lFd3+q3Ud/4hudPl1e8judU1K4S4vJFmv7yQNcSAH+3NRRRQAUUUUAfAH7Z/8AwSz/AGAP+ChH9jXH7Xf7MngT4s654f2po/jLz/Efgf4hWdouzGlf8LE+HWt+EvG9zof7tG/sG81640YSDzRYiX5685/Y6/4Iuf8ABMf9gj4i3Pxd/ZY/ZS8JfD74nzWL6bZ+PNZ8T/ET4neKPD9ncWl/YX8XhDVvir4w8bXXgxtX0/U73Ttcn8KPo9xrenSiw1aW8tIooUKKAPqr4nfsd/s7fGb9oD4A/tP/ABQ8Av4y+Mv7Lg8Zv8Bdd1bxT4wOgfDu++IOlDRfF+tad4Bt9fg8Aal4i1bTorCNPEOu+GdV1nTJ9G0K+0i+sL/RNLurX6boooA/P/8AZ4/4Jc/sM/sn/tE/Ff8Aaq/Z3+Cf/CrfjZ8cB4iHxU8QaD8Q/ijceH/F3/CVeIYPFmt/afh7q/jXUvh5p/n+JIF1e2GjeF9OGm3Tz/2aLSG4mif3T9q39kv9nv8Abd+CXib9nX9qH4caf8U/g/4uvfD+pa14WvdV8QeH5zqXhbXLHxFoeo6X4k8JatoPirQNQs9S06ANe6Drem3V3p02oaNeS3Gj6pqVjdlFAHKfsZ/sK/sq/wDBPr4Wah8Fv2RPhPY/CL4c6t4s1LxxquiW/iLxh4uvNS8UatZabp17ql/4j8eeIPE/ia8Y2OkadaW1tc6xJZ2MFuI7G3t1eQP2+r/sufAzXf2l/CP7YGq+CPtf7Q/gT4Va/wDBPwp8QD4h8Uxf2V8M/E+ux+Jdc8OL4Xh1uPwddNfa1FHdnWb3w/c69Aq/ZbbU4bNmgJRQBvftAfAH4Q/tS/Br4g/s/fHvwVp/xE+EPxS0GXw3448Halc6lY2+saVJPBeRiLU9GvdN1rSNQsr60tNR0rWdF1LT9Y0jUrS01LS760vraC4j83/Y6/Yn/Zm/YE+EDfAb9k74b/8ACq/hU3ivW/Gx8Lf8Jb448af8VN4ig0221jUhrPxA8SeKtfH2uHSdPX7GNU+wweRutraFpZmkKKAPDP23v+CSf/BPj/go34n8D+NP2x/2edP+Lniz4c6DqPhfwj4hj8d/FDwBqth4d1PUF1a40a7u/hn428HSa1p8Wpeffafb62dRTSrm+1OTTBaNqmo/afrrwF+z/wDCH4cfAzwv+zX4f8F2eofBHwf4E0/4ZaP4B8b3urfErTZfAel6emk2XhrXrz4iX/ijVvFWnx6ZHHYSDxPqGry3Nqiw3MsyKACigD8rfiv/AMG5f/BFv4z+M9Q8e+Mf2FvAeneINU3tfx/Dnxv8XvhB4cnmlu7u+mux4N+FPxC8G+EIL2e4vZjPeW2hw3E0YggkkaG1t44vYf2Ov+CJ/wDwTN/YE+L7fHj9k39m7/hVfxVfwprfghvFH/C2vjf40z4Y8RT6bc6xpv8AY3xA+JHirQB9rm0jT3+2DSxfQfZ9ttcwpLMshRQB1X7bf/BIP/gnl/wUX8c+EPiR+2L8AP8AhbvjPwH4UfwR4V1j/hZ/xh8C/wBl+GJNXvtefTPsPw38f+ENNvc6rqV7dfa9Qs7q+HneStyIEjiT7/8AAfgjwx8M/A3gz4b+CdM/sXwZ8PvCnh3wR4R0f7XfX/8AZPhjwppFnoOgaZ9u1O5vNSvfsGlWFpa/a9QvLq+ufK867uZ53klYooA/BX/g5u/YY+If7c3/AAS/8Y6D8Ffh/rHxO+OHwX+JHgD4v/DrwR4U8MnxN418VQW2pSeDfG+heFLaO+tLuDUF8GeLtW8Typp9trGo6vB4WPh/T9Hub7Vrea2/zpPC/wDwR+/4LGfF3XPgj8G/Gn7Gf7aOg+BNH8R2fgPwhq3xB+CPxePw/wDg34e+IXjj+1PFGr/vvD8sfhrwda6/r2r+OPFEWmxw2/2q41vWmha/vruacooA/wBif4N/Cnwd8CfhJ8M/gr8PdP8A7L8DfCfwJ4V+HnhOxYW3nQ6B4Q0Sy0LTDdvZ2lhbT301rYxz6hdQ2Vqt5eyXF0YI2mKj0miigD5//aj/AGXPgZ+2f8DPG37Nv7Sfgj/hY3wX+Iv/AAjp8Y+Dv+Eh8U+FP7X/AOET8VaJ420D/ifeCtb8O+JbH7B4m8OaNqQ/s3WLT7R9j+y3Xn2U9xby8p+x1+xN+zL+wJ8IX+A/7Jvw2/4VX8Kn8V6143bwv/wlvjfxpnxP4it9NtdY1P8Atn4geJPFWv8A+lwaRpyfZP7U+wwfZ91tbQtJKZCigDq/2o/2XPgZ+2f8DPG37Nv7Sfgj/hY3wX+Iv/COnxj4O/4SHxT4U/tf/hE/FWieNtA/4n3grW/DviWx+weJvDmjakP7N1i0+0fY/st159lPcW8vKfsdfsTfsy/sCfCF/gP+yb8Nv+FV/Cp/FeteN28L/wDCW+N/GmfE/iK30211jU/7Z+IHiTxVr/8ApcGkacn2T+1PsMH2fdbW0LSSmQooA+Vv2s/+CI//AASz/bf8exfFL9pD9kDwF4s+Iv8Ap7aj4z8K6348+EfiDxLNqU0Vxc3njTUPhB4s8CTeN9RE0W+21Lxada1Cy867WzuYEvLpZvY/2KP+CY/7Cf8AwTssvFNr+x1+zr4S+Dt343FvF4t8R2+peKvGXjXX7Kzme5s9I1Dx18Qdf8V+MJNBtLmRrq10BNbj0WC6P2qOwW5/e0UUAUf25v8Agl1+wz/wUk/4Vkf2z/gl/wALiPwd/wCEwHw6P/Cw/in4C/4R7/hPf+EY/wCEr/5Jp428Hf2p/av/AAh3hz/kM/2h9j/s4fYPsv2m8+0cn+1V/wAEg/8Agnl+2x4H/Z/+HH7TP7P4+JHg39lzwnfeB/gZo/8Aws74w+Ev+EI8MalpHgzQb3TBf+B/H/hvUvEIm0r4feEbX7X4pvNbvU/skTR3KXF5qEt0UUAfFn/ELt/wQ0/6Mm/82H/am/8An20f8Qu3/BDT/oyb/wA2H/am/wDn20UUAfafiP8A4JB/8E8vFn7F3gb/AIJ6a/8As/i//ZC+G/iy48ceDPhN/wALO+MNt/Y/ie68ReLvFk+p/wDCb2nj+D4iX5k1/wAdeKb82mpeLLyyH9qG2W2FpaWMFt9V/sufsufAz9jD4GeCf2bf2bPBH/Cufgv8Ov8AhIj4O8Hf8JD4p8V/2R/wlnirW/G2v/8AE+8a634i8S332/xN4j1nUj/aWsXf2f7Z9ltfIsoLe3iKKAF/aE/ZT/Zp/ax8LweC/wBpf4EfCr45+GrPzv7N034m+CdB8Wf2Obq+0jUbxtDvNVsp7/Qnvrzw/ok2oPpF1ZPf/wBlWMd4Z4reNF/IE/8ABrv/AMENGJJ/YlAJJOB+0N+1Ko5OeFHxsAA9AAABwBiiigD9Uv2Ov2Jv2Zf2BPhC/wAB/wBk34bf8Kr+FT+K9a8bt4X/AOEt8b+NM+J/EVvptrrGp/2z8QPEnirX/wDS4NI05Psn9qfYYPs+62toWklMn8UX/BzT/wAEL/2yfiL+2Dd/8FKP2IPB3jv42yfEWD4cf8LR8A/DH+0r/wCMPw1+I3w30Lwx4C8JeN/A2haNLH4i1vw1qWg+HPC0kj+DornX/B3iDR9V8Q38Uej3v9paWUUAfr78AP8Agut/wUDi+FPhWz/ad/4IO/8ABShvjPp+n2un+L9V+CnwV12++HviS+tLO1iuPEujWHjKx0DXfC41i8F3ct4Unk8RRaLH5MMXijVw7ND/ABa+Av8Agmv/AMFMvG//AAVE8F/tHXv/AATm/bS8D/D3xf8At8eHPjbd3Hi74BfECyXwf4M1/wDaGs/Hc83ifUxoQ0q0Hh/RLtn1q/FwNPgFpc3AmFsu+iigD/XVooooA//ZDQplbmRzdHJlYW0NCmVuZG9iag0KMTAgMCBvYmoNCjw8DQovRmlsdGVyIFsvRENURGVjb2RlXQ0KL0RlY29kZSBbLjAwIDEuMDAgLjAwIDEuMDAgLjAwIDEuMDBdDQovQ29sb3JTcGFjZSAvRGV2aWNlUkdCDQovVHlwZSAvWE9iamVjdA0KL1N1YnR5cGUgL0ltYWdlDQovV2lkdGggMzI2DQovSGVpZ2h0IDg4DQovQml0c1BlckNvbXBvbmVudCA4DQovTGVuZ3RoIDE1ODU0DQo+Pg0Kc3RyZWFtDQr/2P/gABBKRklGAAEBAQDIAMgAAP/bAEMAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/bAEMBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIAFgBRgMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP61/wDgsF8Q/Hnwm/4Jeft2fEv4YeMPEnw++IXgj9m34keI/B/jbwhq97oHifwxr2m6PJPY6voetadLBfaZqNpKA9veWk0VxC3zRyK2CP8APj/4IY/HD/gox/wVT/bcn/Zd+LH/AAVg/wCCh3w18LR/Bnx78Sk8R/Dv9oXxjPr7ar4S1PwnZWmnOviXU9U05dPuofEF1JcuLRrnzLe3WJ0VpGr++v8A4Lh/8ohf+Civ/ZqfxV/9MMtf5if/AAb9/ts6n+wL/wAFBLL462H7OXxk/agtrj4NfEnwNrPw7+A2i3HiL4j2GmeIZPD16nivTdChsLuPU7HTNV0XS9O1SK7udJt7az1l78ai9zaW2makAf1N/wDBZv8A4J3f8FUP+CfP7J3iz9sT9mH/AILQf8FC/i/4P+EP/CP3Hxd+G/xN+L/j1/GEPh3X/E1j4Zbxt4O13wNq1vYXth4dvdb0a58T6BrXhq0h0zwzB4g8Xz+LFtNIk0l/09/4Nlv+CwPxb/4Kkfs3fFjwd+0fFb6v+0L+yxrHgbRvF3xJ0zSrTRtP+KPg34k2viufwN4l1TTNNt7PR9O8a2tz4H8U6P4nttFs7XS7uCx0XXIbW0udYu7SD8Mv+C7P/BwN8Rv2pv8Agn346/Zu8CfsC/trfsraT8Y/Fvgvw38Rfi1+0P4B1PwD4d/4QrSdWXxjP4J8Oarp2Fn1/wAaat4a0vTtRsr2+gs9Q8FxeK9IuLLUrfVJo4P22/4NTf2HvhF+y9/wTusfjn4H+J/h34sfEH9saPwL8TvidqOiafoVtc/DZ9H8LBNB+B+q3mka/wCILu+vfAN14h8Sandf2zJo1/DqPi3UN3hzSPNZZwD9U/22/wDgr5/wTy/4J0eOfCHw3/bF+P8A/wAKi8Z+PPCj+N/Cuj/8Kw+MPjr+1PDEer32gvqf274b+APF+m2WNV029tfsmoXlrfHyfOW2MDxyv8r+B/8Ag5X/AOCJHxA8T6Z4S0X9unwnpmp6t9rFtfeOPhh8dfhx4Yh+xWNzqEv9p+M/H/wv8NeEtH8yG1kitDq2tWQvr57bTbMz6hd2ttN+Bf8Awcf/AA6+B3xc/wCC7v8AwSJ+GX7S66E/wA8ceBfC3h34up4n8XXfgLw+3gXUPjh40i10av4zsNa8OXvhuxNru87VrXXdKntV+aK9hfDV9++K/wDgjZ/wal2vhbxJdXd7+zD4YtbbQNYnufEml/8ABQnx7Pqfh63h065km1zToNQ/aJ1rT5r7SY1a/tIr7RtWs5Li3jS50y/hL2soB/U/4I8d+CPiZ4X0vxv8OPGXhX4geC9cF22i+L/BHiHSPFfhfWFsL+60q/bS9f0G8v8ASdQFlqdje6ddm0u5hbX9ndWc2y4t5Y0+Kv25v+Cov7DP/BNv/hWQ/bP+Nv8Awp0/GL/hMD8Oh/wrz4p+Pf8AhIf+EC/4Rj/hK/8Akmngnxj/AGX/AGV/wmPhz/kM/wBn/bP7RH2D7V9mvPs/80H/AAZweIvGP/CPf8FKPhn4V8e+PPiL+xv8Lfjr4F0j9lvxB4stp4NIni1PUvjHeeLr3Q7a8tLK60LU/EvhWL4U+MvFnhaG002w07VvEa6qNIstV13VpLn5h/4Plv8AnGZ/3d//AO+zUAf1o/tVf8FfP+CeX7E/gf8AZ/8AiP8AtM/tAD4b+Df2o/Cd944+Bmsf8Kx+MPi3/hN/DGm6R4M1691MWHgfwB4k1Lw8IdK+IPhG6+yeKbPRL1/7WEMds9xZ6hFa/Fn/ABFE/wDBDT/o9n/zXj9qb/5yVfU/wp/Yb/ZA/bM/Yg/YBf8Aar/Zz+FHx9f4dfso/BlfArfE7wnYeJ28KL4s+Efw0PiVdEN8jGxGuHw3oB1IRYFydIsDJn7OmP5Qv+Dtv/gnx+xD+x7+yF+zF4w/Zd/Zb+DPwI8V+K/2kbvw14j8Q/DXwdYeGtV1jw9H8MfGGqLo1/c2QQ3Gn/2laWl95EisBc2sEildrBgD+hb/AIiif+CGn/R7P/mvH7U3/wA5KvtPxH/wV8/4J5eE/wBi7wN/wUL1/wDaAFh+yF8SPFlx4H8GfFn/AIVj8Ybn+2PE9r4i8XeE59M/4Qi08AT/ABEsDHr/AIF8U2Bu9S8J2dkf7LNytybS7sZ7n8v/APgkR/wSI/4Jh/GX/gmJ+wx8VPin+wt+zd47+I3j39m74beJfGnjLxJ8ONI1PX/E3iHUdFil1HWdY1C5R5rvUL6bdNczyNl5GYgKuFHzV/wdT/Az4P8A7Nv/AAQ48J/Bj4CfDjwn8JfhT4Q/au+FreGPAHgfSbfQ/DGhN4h/4W34n1xtN0u1VYLY6p4h1nVdZvSgzPqGoXVw+XlYkA/qA/Zc/aj+Bn7Z/wADPBP7SX7Nnjf/AIWN8F/iL/wkQ8HeMf8AhHfFPhT+1/8AhE/FWt+Cdf8A+JD410Tw74lsfsHibw5rOmn+0tHtPtH2P7Va+fZT29xL8H/tS/8ABdv/AIJQfsZfFjWfgd+0J+2B4S8LfFbw0Vi8UeDvDPgv4qfFK98KXrRxTf2R4pu/hV4F8aaX4d15IZ4pZtA1i+s9ato5Ee5sIldSfgn/AIIDah8RtJ/4NrPhBqvweg1G5+LmmfBf9tnUPhZb6PptprGr3HxGsvjd+0Tc+CIdL0i/gurHVNRl8TR6ZHY6de21xaX1y0VtcQTQyvG38sn/AAbg2H/BC34kXnxRsP8AgpzN4a8Sftp+M/Hl0PCdx+1xqtw3wN17wprIglmuPC+s6neReD5/inqfiee/l8Y3vxdvhrt0JtDk+Hs7+b4zLAH+hZ+xx/wUW/Ym/wCCgHhm78U/sh/tE+AvjJbaZ9qbWdB0ybUvD3j7w/DZ3o097zxH8NfGOneHfiD4e06e5eEWOpaz4ZstP1KK5tLjT7m5t7q3lk8s/bY/4K8/8E9P+Cdnjzwb8M/2w/j8PhH42+IHhY+NPCWjf8Kx+MHjn+1fDS6ze6AdSOofDnwD4u0uxA1bTry1+y6je2l5+588W/2d45W/OL9lv/g3t/Z+/ZR/4KeeCf8AgpL+xD8c2+EPwFvPA3iSw1z9lPwl4fv/ABl4H8e2fxB8D67pd3ceH/i3f/E28m03wDeeIr7wZ8StJ8KR+F/EGlWWseFLWHQdRsNDutKs9C/ml/4PYHVP27f2N3dgqJ+y5OzseAqr8YvGhZj7AAk+1AH+k5HIssaSxnckiLIjYIyjgMpwQCMgg4IBHcA1+bX7bH/BXn/gnp/wTs8eeDfhn+2H8fh8I/G3xA8LHxp4S0b/AIVj8YPHP9q+Gl1m90A6kdQ+HPgHxdpdiBq2nXlr9l1G9tLz9z54t/s7xytxtp/wXA/4JDRWttG3/BRX9lMtHbwocfFTQxykaqeHkVxyOjKrDoyg5FfwYf8AB2Z+13+zF+2F+2T+yp4y/Zc+Onw3+O/hbwt+z3J4Y8R678NfEdp4l03RfED/ABT8VaqmkahdWRaO3vn069tbxYGbf9nmjkICupIB/qTRyLLGksZ3JIiyI2CMo4DKcEAjIIOCAR3ANPqpYf8AHjZf9elt/wCiUq3QB+Gf/BxX+3Jrf7B//BLT44ePfh94u1XwX8bfihd+Gvgp8Fdd8O61e6B4k0TxZ411SOTXvFWh6vpGq6Xrmlal4Q+H+l+Ltf0jVNMN19n8R2mg2t/bjT7+4mi/zPI/+Ctn/BZ/wVo/w/8AivqH7d37cdv4O8Ta/rI8C+JfEHxl+JWpeCfGOs/Dy90SXxXotp/beqX3hrxO3h6XW/D6eKfD93b6jBDZ69pUetae9hrNql3/AEf/APB6d+0nqPxG/aI/Y1/YZ8CjxBrGp+BPC2v/ABc8UeFtEOrX/wDwk3jj4w6vp3gr4YaVD4Zh0ULqfiLRdI8I+Jl0G50jU9Wu7tfiLfaSdNsbiNG1D7L/AOCtX/BF2P4Y/wDBtx+z98PPBfg3Q7H40fsA+H9B/aG+JKQx6XZazrOp+N9Nef8AaqeTVvt/iAapqVlqGvr4kureDxZJpWoaL8NtP0rw6l3HpPgvQIAD+xL9mL44aH+0v+zl8C/2hPDYsU0b41fCbwD8TLO203VV12y08+MfDOm65daTBrKWenLqg0e8vLjS3vxp9h9qls3m+xWpfyI+H/bF/ba/Zl/YE+EKfHj9rL4k/wDCq/hU/ivRfBC+KP8AhEvG/jTPifxFb6ldaPpn9jfD/wAN+Ktf/wBLg0jUX+1/2X9hg+z7bm5haSISfzyf8Gfn7Yo+Pf8AwTW1j9nLXtbTUvG37G/xI1PwjBaXN2bnWIfhT8UbvV/H3w9ubzz9Qvb6TT4Ndk+IfhfQZpYrCytdI8K2vh7S7RbTQA7bn/B4r/yiFtv+zrPgr/6YfiRQB+//AOx1+21+zL+338IX+PH7JvxJ/wCFqfCpPFeteCG8Uf8ACJeN/BePE/h230261jTP7G+IHhvwrr/+iQavpz/a/wCy/sM/2jbbXMzRyiP81/Hf/Byn/wAEWvhn448ZfDjxt+2P/Y3jL4f+K/EXgjxbo/8AwoP9pjUP7K8T+FNXvNB17Tft+l/By90y++w6rYXdr9s068u7G58rzrS5nt3jlf4o/wCDOr/lELc/9nWfGr/0w/DevwG/4JFfsYf8Eqv2uf23v+CzD/8ABTSD4UTjwB+1dMvwY/4Wd+0T4j+ApU+KPi5+0wfiB/YieH/id8OX8VA/8I94M/tFrkawuh4sRF9gOrv9sAP7Uv2Xv+C2/wDwSr/bG8QDwl8BP20vhRrni+bUItL0/wAI+Nv+El+DnirXL+eTT4La38NeHvjHoHgPVvE73d1qllZWg8P2mpfar6SSyg8y5gnij+if21f2/f2Uf+CePw88O/Ff9r34lX/wq+HvirxXF4I0fxPb/Dr4nfEGxfxPcaZf6vbaZqCfDPwb4xutF+1WGl30lpd6zb2FjdS27WlvcyXbJA38Qn/Bdn/gnj/wQH/ZU/YT1n42fsI/Eb4cfDf9rnwp8RvhvJ8F9M+Cn7V3jD40a/461SfxbpcXiTStV0PVfiZ8TJ/DWl+GfBh8Q+PbLxnpy+FLjS/EnhfQtN/4SJ5NZg0XVf2K/bN/Z7+J37cf/Bq94Mj+NK+Ldf8A2hPBX7GvwW/aXstT1awvr7xdfeN/hH4Z0zxbc3mqadYanoyare+LfhqniTQ73UtYOppbJ4lk8Y3Wm6rrWnWxYA/oc/ZF/bK/Zq/bv+DVh8f/ANlH4n6f8WvhRqOva54Xj8S2Wi+J/Ddxa+IvDc8cGs6Nqvh3xlovh3xPo2oWouLS7S31bRrJ7rTb/TtVsxPp2oWd1P4j+1j/AMFVf2Cf2HPjB8K/gP8AtUfHyw+EnxO+NNnpeo/DrRdV8EfErWdL1fT9Y8US+DbLUNR8X+GPB2t+DvC1gPEEM1peXvivXtEttNt4pNT1GW10tGvR/LH/AMGSf7TS678G/wBsb9kXWtYX+0Ph5448E/HPwJpF5r5muLrw58QtMvfB/jv+wvDLWSfYdO8NeIPB3hK513V49SuFvb/x9pNq9jZvai4v/wCej/g6m/aEn+OX/BY/476Dp+raZrXhP9nbwv8ADD4HeH7nRtU1HVdOj1HSfCFh4x8dW1/bXs02n6X4h0f4i+MPFfhHXbHS4LW0M3haEXUU2orfXEoB/reghgGUgggEEHIIPIII4II5BHWvAP2df2o/gZ+1h4X8Z+M/gF42/wCE88N/D74q+PPgn4t1IeHvFPhxdL+Jnwz1CHS/G3hxLfxZomhXeoLo1/PFANZ0yC80HUdxk0vU72JWcfnbF/wUWvfDf/BDTSv+Ckesss3i+D9hbRPi6v8Awk0fhiz/ALb+K998P7PTtMF7plre6L4bmPiP4kXNqYPDelz6edR/tCHQdItIr65tbEel/wDBHv8AZ5079iT/AIJV/sh/CfxLqR0Y+EPghD8TfiHqnim5uNJi0LxH8UrvWPjZ8RJNYk8RWei3WgafoOveNdbhmtNbt7WXw7YWIsb6ZhYPcMAaX7af/BYv/gnH/wAE9PiN4b+En7Xn7R9h8KfiJ4s8KweNtF8MRfD34r+PbyTwxdapqGjWuqahN8NvAvi+y0VLrUdL1CG1g1q50+6uY7WW6hge1AmP6T6Xqmm63pmna1ouo2Or6Pq9jaappOraXd2+oaZqmmahbx3dhqOnX9pJLa3tje2ssVzaXdtLLb3NvLHNDI8bqx/yaf2pPgP+0R/wXu/ad/4K8f8ABSP4Em+1P4Mfsu6NY+JvA0eqaNq9uPHXgDwOLDwp4P8AAHg2xu79pNI8Rf8ACl/BPi34x6/bWljOra7DHb63o2gax8SbK7i/tf8A+DWj9uqL9sP/AIJdfD7wD4k1jTLv4s/shX4/Z/8AGFlBqN7daxc+CdJtEvvg/wCKtVtNQi+0Wg1TwZIvhcTw32qWWp6t4H1u/gubGSafQdHAP1F+CX/BUb9hn9of9qL4nfsWfCv42f2r+1D8HT4pHxC+EOvfDz4o+Bte0f8A4QvV7XRPE/2PUPHHgrw94d8Q/wBlX17ZtKPDes6sZrC5i1W1E+mFrtfvLVNU03RNM1HWta1Gx0jR9IsbvVNW1bVLu30/TNL0zT7eS7v9R1G/u5IrWysbK1ilubu7uZYre2t4pJppEjRmH8en/BxR+y18Sf2Pv2k/2af+C+/7JGgalf8AxJ/Zp8V+EvDH7Ufg7w7p1zFD4x+FDRavoB8aeIdU0uK6uLGwvvCWtal8GvHWq3mm3ttH4W8Q+F7yeSC28Ozw3uH/AMFEv+C0vwU/4K4fDj4F/wDBMP8A4JOfGfxhr3xv/b0+I+gfD740eK4Ph78T/AWrfAn9nrT9Lbxp8YJNUufEeieFrfWL268M6ZqVh4rsvCOu61ot34E0X4gaXfazFZ6vpl5eAH9U/wCyp+1l8CP21/gn4f8A2i/2bfF+oePPgz4tv/EOn+FvG9/4M8b+BbTxC/hXWbzw5r93pGm/EHw74W1y80vT/EGmaporavHpn9mz6jpeo29rdTtZzlfzK+Jn/ByJ/wAEVvhL478SfDnxb+3F4Tu/EnhO/Ol6zN4G+Gvxu+J3hcXywxSzQ6X45+G/wz8VeCtfFsZfs91NoXiDUoLW9iubCeWO8tbiCLg/+C13w41z9kX/AIN/f2mvg7+x1YeIfCmmfBv9n/4Z/DDw3beGIoLvXdL+C+n+Pfh74U+K15d3KW0YXf8ACGbxtqHi/wAQWsFreW1rNrOuWslpdxpLF/PT/wAG9fwU/wCDcD9oP9lPRPhV8bPDnwr8WftweJ9P/s/42aD+1rrcnh7xNf61qOpatpGkwfs6alf6tovhCHw7LBqUUOjH4Y6hF8U47qTS7nxuU1SDw5NEAf3K/s2/tVfs5/tg/DXTfi9+zH8ZPAnxq+Hmp7UTxB4H1uDUv7Ouzu3aV4i0l/I1vwtrsOxvtOg+JdN0nWbXH+kWMWRn6Ar8DP8Agk9/wQw8Nf8ABJr9pr9q74t/Bb9ozxB4k/Z//aQ06ysPDP7Nd94OvbS0+HEWgeKrvXvA13qPxHv/AIheJb34iah4M0PWvE/hXR9V1Dw1omoS6d4k1C6vLq4uJZjP++dABRRRQB+Vv/BcP/lEL/wUV/7NT+Kv/phlr+AT/gzpZh/wV5uwCQH/AGUvjUrAEgMo1/4attYD7w3KrYORuVT1Ar/Rr/4KW/s+/ED9q79gP9rj9m34VHQh8R/jX8DfHHw88GHxPqUujeHxr/iHTHs7E6vqkFlqM1jYiRszXEdjdMi9Im7fxr/8EhP+CDn/AAWQ/wCCTn7YFp+1Zpfwm/ZB+OkX/CtvG3w01bwBf/tP+KPh7Ndab4wOkXS6lpfi23+CnjCPS9Q03U9B02VlvfC+tWt7prahYLDZ3dza6rYAH9wv7VP7M/wU/a++AvxI/Z+/aB8Gaf43+GPxA8Naxo+s6fdLBDqOlPeaXe2MPiPwvrEsE8/hnxZoqXk9zoPiSwVL7Sbs+dEzRtLFJ/kr/wDBvJ+0j8Z/2fP+Ct/7Inhr4W+P9e8N+D/jP8b/AA18Jvir4MTVtUXwb8QPB/i4ar4Ve28VeHLW/s9M1zUdBj1y41nwbf6jDcyeHfE8NjqtnG+ye1uv9Bv9uOf/AILtftVfss/GD9nX4S/sd/sf/s4+I/jF4U1HwDqnxbn/AG8fFfxQ1Hw94O8R28umeL7bw74ctP2VPhoLfxBrOh3F1pNjrtz4guYNGS7uLpdHvrkWz2/wN/wQv/4Neof+CfXxi8L/ALY37YfxE8JfFX9oXwbYXUnwp+GPgC11C6+Gnwm1/WtHvdH1TxdrHiXXILC7+IPjKy0zVLzT/DccfhvRtA8H3/neJLKfxBrw8Pan4YAPij/g5k/Zu8M/thf8Fvv+CUH7LnjPxBrvhTwr8d/hv4d+GuveI/DCafJ4g0XTfEnxu8aWdxf6Qmq293pzX0Ctuh+22txBu5eJwNp+JP8AgrL/AMGq1z+wP8LvDf7VX7MHib4rftcfA/4X6hBr37U/wm12Tw94Z+Lum/DfS9StL7WvF3gHXPCvhy5srjw/HoSalY+LJ/8AhFdT1rwDCIPHAsPEnhy28QR+G/6x/wBvr/gkf8Yv2t/+CsP/AATv/wCCgfg/4o/DTwt8O/2N/wDhEv8AhMvA3iW28UyeNPFf9gfErXvGt5/wjE2l6ZdaGn2nTtWgsrb+1b+y8q9ilkl3wFK/ejVNL03W9M1HRda06x1fR9XsbvS9W0nVLS31DTNU0zULeS0v9O1Gwu45bW9sb21lltru0uYpbe5t5ZIZo3jdlIB+U3/BFbX/APgnL41/YV8A+Pf+CY/w00D4UfAfxTfXB8TeDLWwvIPHWgfFLR9N0rSvFWhfFvVNYv8AWfEHiT4gaLbwaXaXfiLWNf19db0f+yNX0bWdR0HUNNu5v5av+D5b/nGZ/wB3f/8Avs1ftv8A8EcP+COH7Vn/AASc/ab/AGoU0T9oP4TeMP2Bfjlr/i3xV4N+Bmn6f47j8d+A/Ea+ILdvhtrNtceIINR0+3vtJ8CGfwR4wm/4SzVn8S29hoOoXTXV5pdnLaZ//Bwz/wAESPjl/wAFi2/ZLPwZ+MHwo+FK/s9j44/8JGvxOtvF87a63xSPwk/sg6I3hXRtYCDTB8PdTGpLfpAXOoWBtmfZcBQD9qf2EP8Akx79jX/s1P8AZ4/9VF4Qr+Vv/g9t/wCTHv2Qv+zrL/8A9VF43r+ur9nL4a6r8GP2evgP8Htd1HT9Y1v4UfBn4X/DXWNW0lLmPStU1XwL4I0PwvqGo6ZHeJHdpp97d6XNc2aXUaXK20sazosoYD8df+DgD/gkl8Xv+CvP7PvwQ+D/AMHvif8ADf4W6x8LfjJc/ErVtU+JUHiebTdS02bwTr/hdLDTx4X0nV7pb5brV4rhvtMEcBt45MSiQKjgH09/wQ8/5RC/8E6v+zU/hV/6YYq/Kf8A4PFf+UQtt/2dZ8Ff/TD8SK/eT/gnv+zd4m/Y9/Yi/Zd/Zc8Z+INC8V+KvgR8G/B3w117xH4YTUI/D+tal4b09LO4v9ITVbe01FbGdl3Q/bLW3n28vEhO0fGv/Bc7/gm18Tf+Cqf7EcH7Lvwn+IHgT4a+KY/jN4C+JT+I/iJB4gn0BtK8JaZ4ssrvTkXw3p+qaiuoXU2v2sls5tGtvLt7hZXRmjagD5Z/4NwfiZ4A+DX/AAb6/sqfFT4qeLtD8A/DnwFof7Svibxn4z8TX0WmeH/DPh/Tf2ovjbNf6vq+oTERWljaRAyTzyEKi8mvn/8Aae/4N9P+CPH/AAWS0vUP2uv2Svihp/w28SfETxZ431TxR8bP2WvEmk+N/hp8RPHDQRaTqR8W/DvVb6+8JaVqmj+IrFNc1u38Cj4d6x4ivNW1vUvEV1qGoeILTXbT9Xf+CYP/AATq1L9jf/glv8Nf+Cd/7QOveEviv/YnhT45+CPiLqngeXxJpXhjxR4X+M3xL+Jniq90zTbq9j0XxLZMPC3j5NFvL2IafeQ6hDdXOnTRAW04/AX4Uf8ABuV/wVO/4J5fGD4qan/wSd/4KmeGPg18DPiPqDamngf4w+DLrxFfQr5t7Fpdj4n8LX3gv4lfDTxV4g8N6TLb6ZZ/Eu00Hwz4g1OHzU/sfRoEWGYA/Gbwb4P/AOCi3/BtN/wVW/Zj/ZY8OftN2Hxb/Z5/aW+I/wAN0t/B7y+Ibb4afEz4aePPHtt8L9V1zxT8J9R1XUYfhl8SdA1DUNUn0zUvC3iTU7lL7RtMuLrXdc8PXN3od16Z/wAHsSLL+3X+xxE4ykn7LdxG4yRlX+MPjRWGQQRkEjIII7Gv2U/Yc/4Nm/2gdA/bP8Cftv8A/BTz9u7Uv2wfHvwi8V2/jT4deD7SXxx4ssp/FWla3p3ijQdX8QeMvilebrHw5p2tpqr3Hw30HwNFpl5Jc2epW3iPTJLd7B/V/wDgvz/wQD/aG/4K5/tGfAv4z/B744fBn4XaL8Kfg8/w51PSfiVZ+N7jVNR1VvHXiDxX/aNi3hfQ9VtG0/7Jq9tbeXcT21yLiCc8xPG6gH1faf8ABrz/AMENpbW2lf8AYny8lvDIxP7Q/wC1KMs8asxwvxrVeSSeFA9ABxX8Sn/B0F/wTx/ZA/4J0ftb/syfDf8AY5+Ev/CovBvjz4Et438V6P8A8Jx8RfHX9qeJ4viZ4m0FNS+3/Efxb4v1Oy26Vptna/ZLC8tbE+T5xtvtEksr/wCrXbRGC3t4WIZoYYomYZwTHGqEjPOCRkZ5xX8qH/Bfn/ggH+0N/wAFc/2jPgX8Z/g98cPgz8LtF+FPwef4c6npPxKs/G9xqmo6q3jrxB4r/tGxbwvoeq2jaf8AZNXtrby7ie2uRcQTnmJ43UA/qqsP+PGy/wCvS2/9EpViWVIY5JpWCRRI8kjt0SNFLOx9lUEn2FMtojBb28LEM0MMUTMM4JjjVCRnnBIyM84r5a/bk+E/xh+PP7IX7RPwQ+Afinwn4H+LHxf+Fnin4Z+FvGPjj7YfDPhkeOLF/Deua1qEVhoHie7upLDw5qOrzWFlFo8wvNRW0tnutMjlbU7QA/yffixZftFf8F1f+C2vxm1X9lbULP8A4WV8bPiz448S/BzxXq+rav4N0Twn8NPgN4Uni+G/iHU/FWj+EdO1bwj9i+H/AMOfDFnoer6l4e0/W5PFl9oOnalcP4n1Q3dx+vmq/wDBuH/wcj69pepaHrn7YNhrOi6xY3el6vpGq/ty/HLUdL1XTL+CS1vtO1LT7zQprS+sb21lltru0uYpbe5gkkhmjeN2U/uf/wAEFP8Ag3L+MP8AwSh/ar+IX7SPxz+L/wAGvizPrXwY1f4Z+BYPhq3xFs9U8O6n4g8U+GNW1rUNRg8QaVoWj3unXukaA9j5c8eoXEN09vPapbFZJT/XFQB/mR/8GuPxc+IX/BPb/gsf8aP+Ce/x0kfwhqXxosfG/wADvFvh+a+ZNBT47fAa51/xT4NujcXd3pdnN9v0ez+Ieg+ENSmtLq91W48YabpGk2AuvEilP6Mv+DxX/lELbf8AZ1nwV/8ATD8SK8a/a7/4Nw/2mfHf/BY7/h6T+yl8dv2efhzpCfGX4P8Ax5g+GfjLw94w0vVovG/gjTvCdv8AEGCe90rwx450K+f4j614d1jxTeeJZbCK7t9X8Y6ix0Rp9MhvtQ/X7/gub/wTa+J3/BVT9iK3/Zd+FPxA8B/DTxVH8ZfAPxLl8RfEKHxBceH20zwlpfiyyvNNjHhvTtS1EX9zN4gtZLaRrI2/lW86ytG7xkAH5zf8GdX/ACiFuf8As6z41f8Aph+G9fzZf8E1f+CJHwN/4LF/tv8A/BX8/Gb4wfFf4Ut+z3+1bq3/AAji/DG28ITrrq/FL4uftF/2udbbxVo2sFDph+HumDTVsEgDjUL83LPstwv9s/8AwQx/4JtfE3/glZ+xHP8Asu/Fj4geBPiV4pk+M3j34lJ4j+HcHiCDQF0rxbpnhOytNOdfEmn6XqLahazaBdSXLi0W28u4t1id2WRq8e/4I4/8EkvjB/wTb/aE/wCClXxh+JnxR+G3xA0X9tv4x+HPiT4H0nwJbeKIdU8H6fo3jf47+KLmx8Vv4g0vTrSS9ntPivo9tANIkvYEuNL1IyXBje1ZwD+PfQv+CVX7KX/BFT/gqx4N8J/8Fb/hXf8A7Qv7AHxibUtK/Zl/aVvZdVs/hToPi3+09NvNKvfj/wCCfD7r/aV/4WsjJofjfwnfX0/h6Ky1JPiBb6F4n8MxXNtpH+mwvhjwrN4TXwZFoGgN4Il8PDwxH4Wi0nTW8LN4VfTf7KTQI9C+zNo7aAdII05NJ+yNpp07Fn9mNr+6r8zf+Cyv/BNjTv8Agqj+wx4+/ZhtdY8K+DviWde8I+PPg38RvF2marqml+BfHfhXXLWa6uriDRLm31IWfijwbceKfA2oTRx6nHp9t4lOtDRdVvNJsrY+pf8ABMH4BftU/ss/sYfCf9nf9sD4s+A/jj8UfhBa3vgrRPiZ4Cg1+CDXPhjpUqL8PdP8RDxDpejXM/iPwpobp4Pe9g0+NNQ0LQdCvtQuNQ1641jUbsA/z3P+CP3juH/gj1/wcW/Ev9mb4meJrD4V/BjW/Hfxz/Zo8Xat8QNf1Kz0ax8HXUWq+NvgDr2ra9c2OhafdyaxrOh/Diwg8Wa9Y2HhZNM8X6h4lkmstOMGpQfpZ4R/ZGg/bE/4N1P+CsP7bPiGzvNY8fftpftJfGv9uv4Zalrugad46+JPh3wz+zv8Xb/S/C3h+8udFjstStdfutB8I/FTwPNImq65pHhzw/4y1XxHpFtNaavf2Nx9of8ABb7/AINjvjH/AMFM/wBt/Uv2ufgJ8b/gV8J7fxn8NvAnhv4g6B4+0DxtBrms+NfA1pdeHLbxVPqvhfTdfttaN14LtPCfh+OW6h0u5sbLwzY2AiuoIYp6/pc/Zq/Y88IfBH9gv4TfsM+I9vi3wd4N/Zt0b9n/AMZlNR1SC38TWM/gb/hFPGzWmqWg0fWrSw1qW91g2Fzb/wBnatZWVzA0clvewiVQD+Cv/gm1+0doX7bf/BIL9hv/AII0S67aap4t+K3/AAU50z4V/EX4daHp2mz+J7r9jrwL4k/4bT+JfifWdOg1638YyeG9P8QNdtffEHw5c+EbrQ4dLttHt9Qkg0rUhf8A9Sf/AAc1ftp3/wCxn/wSh+Mdv4O1ptG+KP7SN9o/7NvgC4jlhkvILDx0Lif4nX8cM93bXc7J8KNJ8Y6Ta3Vi1zd6drmuaLqc1tcWVteKfzy/4If/APBsh8Tv+CZ37ao/a3/aD+MvwX+Lkng/4deMfDvwo0X4d6V4u+3eHPGnjWO28P6l4qubnxfoGmiyNv4FuvFPh23k0y6mvJIvEl7bzlrWSUv+hX/BVT/gjn8Wv+Cpf7a/7FXjP4m/F7wJpX7Bn7MV6fE/jz4B3MXiHUvF3xU8XX2tf2v4laewg0Oz8M/2f4j0rQPCHgM3GteJNSl0Lw5P4tv9G06G61rUtO1kA/jp/wCCXX/BTv8A4Khf8EqP2bb39nX4G/8ABIzxV4ys/E/jPXvH/jvx/wCO/gV+1HJ4p8beINahttMtHvk0G10rSrTT9F8M6bpGg6bZ2dqF8uznvpZXudQnI7//AINfv2nvFX7GH/BW3xx+zl8bfhx4s/Zj8Kft3eGL/R/Dfwn8baF488M2ehePdE1zXfGHwO0u3tviFcReJbuwXTrvx78OPBXiPxC+v6pqWpeIIdLe9uNQ1u/1GP8A05xwAOeOOSSfxJySfcnJ71/Pn/wWn/4I7fFr/goX8W/2KP2oP2V/it8Nfgf+0z+x746u9asvFnxF07xLJo3i3wtHrfh7xn4Z0bVLzwZYXfiS4Xwn4y8O3U2m6Ul1p2nTaV458bpcXH2m7tDGAfvD468C+Dfif4L8VfDn4ieF9D8beA/HOgar4V8YeEPE2m2useHvEvhzXLKbTtX0XWdLvY5bW+07UbK4mtrq2njeOSKRgR0I/hq/4NHf2c/hF8Pf23f+Cx8mh+GReax+zt8S/CPwJ+E/ibXbp9T8ReHfhvqXxQ/aGsNb0v7WEtrOe/1+H4V+Bm1fVk0+C7mbR2itmtLS/vrW4/uu0d9XfSNKfxBb6da68+m2L63a6Pd3OoaTbau1rEdSg0u/vbHTLy+06G8M0dleXem6fc3NssU09jaSu9vH+EH/AARx/wCCSXxg/wCCbf7Qn/BSr4w/Ez4o/Db4gaL+238Y/DnxJ8D6T4EtvFEOqeD9P0bxv8d/FFzY+K38QaXp1pJez2nxX0e2gGkSXsCXGl6kZLgxvas4B+t/7TP7QX7Nf7N3wsv/ABv+1h8Svhr8Lfg/rt9D4D1XW/izqGm6f4L1e98T2OopH4V1H+10k06//tzTrPVI30y6ilhvbSG6ilieLep/lm/ab/4NJ/8Agmf+2hpmm/Hr9hb4163+zNoHxI8K6N4l8GyfDaW3+PH7PviKPWtWbX28baJpHiHxXbeIV0/XfD9++maZpPhL4laL4S0f7PpN3peiwwWl9pepf03ftv8A7Hfwm/b7/ZY+L/7JHxvGux/Dn4w6HYabqmoeGNSbSfEWhat4f1/SfF3hLxNot5slgOo+GvF3h/Q9dgstRtr3R9UOnnTNasL/AEm8vbOf+Uj4E/8ABA3/AILy/sWfDrxz+zr+xx/wWJ+Hfgn9nbXNQ1NfD+iax4R8VWutaNp99eajeXGpeEbPVPBvxKufgtrms3Wp3mpa8PhX43sjeatL/aM+q3t7FBdxgHxL/wAEO/jR/wAFFP8Agmb/AMFsdO/4IkftAfGbT/i18J9Us/Eeiav4WuvFHiLxx4O8CrpH7NuuftF/DjxX8D9R8Qxadq3g5tW8O2+haT4j8JLZ2vh97LVtQhvdGl1rQdG1Wz/0Pq/mE/4I1f8ABvLr3/BP39oTxN+23+1v+0/f/taftb674Vl8I+H9ba08QahovgSwvodS8Pa/qU/jf4i6lrfjXx/4g1vwZZ+DtF0jWJNO8DS+CtKtfEnhWGPxLo+sx3UH9PdABRRRQAUV/LJ+3V/wdUfsy/sBftYfGX9kX4r/ALLX7QniLxx8Gtf0/SNS8QeC9a+F174Z12x1zw9o/i3w/q+lyaj4q0/UIE1Dw9r+l3NzYX9nBeabeSXFhOrvbGWT5K/4jbf2Hv8Ao0L9qz/wP+EX/wA29AH9qFFfxX/8Rtv7D3/RoX7Vn/gf8Iv/AJt6P+I239h7/o0L9qz/AMD/AIRf/NvQB/ahRX8V/wDxG2/sPf8ARoX7Vn/gf8Iv/m3o/wCI239h7/o0L9qz/wAD/hF/829AH9qFFfxX/wDEbb+w9/0aF+1Z/wCB/wAIv/m3o/4jbf2Hv+jQv2rP/A/4Rf8Azb0Af2oUV/Ff/wARtv7D3/RoX7Vn/gf8Iv8A5t6P+I239h7/AKNC/as/8D/hF/8ANvQB/ahRX8V//Ebb+w9/0aF+1Z/4H/CL/wCbej/iNt/Ye/6NC/as/wDA/wCEX/zb0Af2oUV/Ff8A8Rtv7D3/AEaF+1Z/4H/CL/5t6P8AiNt/Ye/6NC/as/8AA/4Rf/NvQB/ahRX8V/8AxG2/sPf9GhftWf8Agf8ACL/5t6P+I239h7/o0L9qz/wP+EX/AM29AH9qFFfxX/8AEbb+w9/0aF+1Z/4H/CL/AObej/iNt/Ye/wCjQv2rP/A/4Rf/ADb0Af2oUV/Ff/xG2/sPf9GhftWf+B/wi/8Am3o/4jbf2Hv+jQv2rP8AwP8AhF/829AH9qFFfxX/APEbb+w9/wBGhftWf+B/wi/+bej/AIjbf2Hv+jQv2rP/AAP+EX/zb0Af2oUV/Ff/AMRtv7D3/RoX7Vn/AIH/AAi/+bej/iNt/Ye/6NC/as/8D/hF/wDNvQB/ahRX8V//ABG2/sPf9GhftWf+B/wi/wDm3o/4jbf2Hv8Ao0L9qz/wP+EX/wA29AH9qFFfxX/8Rtv7D3/RoX7Vn/gf8Iv/AJt6P+I239h7/o0L9qz/AMD/AIRf/NvQB/ahRX8V/wDxG2/sPf8ARoX7Vn/gf8Iv/m3o/wCI239h7/o0L9qz/wAD/hF/829AH9qFFfxX/wDEbb+w9/0aF+1Z/wCB/wAIv/m3o/4jbf2Hv+jQv2rP/A/4Rf8Azb0Af2oUV/Ff/wARtv7D3/RoX7Vn/gf8Iv8A5t6P+I239h7/AKNC/as/8D/hF/8ANvQB/ahRX8V//Ebb+w9/0aF+1Z/4H/CL/wCbej/iNt/Ye/6NC/as/wDA/wCEX/zb0Af2oUV/Ff8A8Rtv7D3/AEaF+1Z/4H/CL/5t6P8AiNt/Ye/6NC/as/8AA/4Rf/NvQB/ahRX8V/8AxG2/sPf9GhftWf8Agf8ACL/5t6P+I239h7/o0L9qz/wP+EX/AM29AH9qFFfm3/wTk/b+8Z/8FDPhrH8bYv2Ofjr+zP8ACDWbe7fwL4m+Pd/4O0jxB8QJLS5sIhf+HfAmjapqXiKPwneQ3N5Lpni7VY9P0zV30ycaQl/aSw3zFAG/8ZP+CUf/AATb/aF+JXij4x/G/wDYp/Z6+KPxT8az2F14t8e+Mvh5o+seJvENxpek2GhafNqup3EZnu5bPRtL07TYHkJMdnZW8IO2NRXmX/Djz/gkL/0jq/ZT/wDDVaD/APGq/VKigD8rf+HHn/BIX/pHV+yn/wCGq0H/AONUf8OPP+CQv/SOr9lP/wANVoP/AMar9UqKAPyt/wCHHn/BIX/pHV+yn/4arQf/AI1R/wAOPP8AgkL/ANI6v2U//DVaD/8AGq/VKigD8rf+HHn/AASF/wCkdX7Kf/hqtB/+NUf8OPP+CQv/AEjq/ZT/APDVaD/8ar9UqKAPyt/4cef8Ehf+kdX7Kf8A4arQf/jVH/Djz/gkL/0jq/ZT/wDDVaD/APGq/VKigD8rf+HHn/BIX/pHV+yn/wCGq0H/AONUf8OPP+CQv/SOr9lP/wANVoP/AMar9UqKAPyt/wCHHn/BIX/pHV+yn/4arQf/AI1R/wAOPP8AgkL/ANI6v2U//DVaD/8AGq/VKigD8rf+HHn/AASF/wCkdX7Kf/hqtB/+NUf8OPP+CQv/AEjq/ZT/APDVaD/8ar9UqKAPyt/4cef8Ehf+kdX7Kf8A4arQf/jVH/Djz/gkL/0jq/ZT/wDDVaD/APGq/VKigD8rf+HHn/BIX/pHV+yn/wCGq0H/AONUf8OPP+CQv/SOr9lP/wANVoP/AMar9UqKAPyt/wCHHn/BIX/pHV+yn/4arQf/AI1R/wAOPP8AgkL/ANI6v2U//DVaD/8AGq/VKigD8rf+HHn/AASF/wCkdX7Kf/hqtB/+NUf8OPP+CQv/AEjq/ZT/APDVaD/8ar9UqKAPyt/4cef8Ehf+kdX7Kf8A4arQf/jVH/Djz/gkL/0jq/ZT/wDDVaD/APGq/VKigD8rf+HHn/BIX/pHV+yn/wCGq0H/AONUf8OPP+CQv/SOr9lP/wANVoP/AMar9UqKAPyt/wCHHn/BIX/pHV+yn/4arQf/AI1R/wAOPP8AgkL/ANI6v2U//DVaD/8AGq/VKigD8rf+HHn/AASF/wCkdX7Kf/hqtB/+NUf8OPP+CQv/AEjq/ZT/APDVaD/8ar9Uq/J7/gqL/wAFkv2OP+CVPw01DXvjX41sfFHxo1TRL+9+Fv7N/hHUEuviX8Q9VhSAWSX6W1rqUHw88IyTXcMuoeOvGMNnpUdjDfL4ftvE+vw2vh2+AOT+Jn/BIz/gh98F/AniT4ofFz9iX9if4Z/DnwdYHVPFPjjxz4I8H+GPC+g2PmxW6XGqa1q72lhaie6nt7O1SSYS3l7cW1lapNdXEML/AOfJ/wAFi/8AgpH/AMEu/iBceMv2df8Aglv/AME7/wBljwN8OJVXStZ/bC1T4OR6Z8TfFsJXTbqY/Bfwtrek6NffCzTormPUNLn8WeKLLU/GGt2U4n0fSPAk1sl7qXw//wAFUf8Ags9+2D/wVf8AiGmsfGbxCPA/wb8N3mpH4bfs8eBdS1O3+HfhWxudVuL2x1DxFHJLD/wsDx7BYtZade+ONcsreSRLEf2Fo/hqyuJ9OP5H0AFFFFABRRX9B3/BIn/g3b/bF/4Kf6poXxH1zS9Q/Z4/ZGXULCbWPjh430uS21Txvpa3GiXWoaZ8F/CF55Wq+L9QvtA1Sa70jxndWdt8NYbi1nt5/Ed5qds2izAH4y/s5fsz/Hv9rn4r+Hvgf+zZ8K/Fvxg+KfigztpXhLwfpxvLtbOzjM1/q2q3kz2+l6DoOmwDz9U1/XL7TtG02HEl7fQKyk/6UP8AwSA/4NUf2bv2NR4a+OX7b8fhP9qj9pe0EOp6V4MntbjU/wBnv4T6krahGjaZ4d1e1sl+KviBbK6tjLrPj3Rn8PaTqkC3PhvwtDqOnWPiab9//wBg3/gnJ+yN/wAE2vhMvwi/ZQ+FuneCtOvhay+MvGmoGHWfiZ8S9Sspb+a01f4i+N5LaDUvEl3ZPqmoJpNoy22iaBa3Uth4e0nSdPItR9yUAIoCgKoCqoCqqjAUAYAAHAAHAA4AopaKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKoarqumaFpepa3reo2Gj6Lo9hearq+r6reW+n6XpWl6fbyXd/qWpX93JDaWNhY2kMt1eXl1LFb21vFJNNIkaMw+Rf22v2/v2TP8Agnh8Jrn4yftZfF3Qfhp4aYXEPhvRH83WPHnj/Vbd7SJtD+H3gXSlufEfizUo5tQ09NQk06xbSvD9veRar4n1LRNFS41KH/MJ/wCCv/8Awcjftcf8FN28RfCLwAt/+zJ+yBf+fp8vwg8La/Le+L/ifpZOnuJPjV42s4dNGv2c13p7ahbeBtCsdL8KabHfNpusN40ubC119wD+lD/gs9/wdmeBvgRfeIv2cf8AgmTdeE/i/wDFW0/tHR/Gv7Tuowyaz8LfhprWnau+nXui/DbQL7TU0z4u6/5NreSjxn/aEvw000S6ZcaSvxCW7vYdG/zvfi58YPin8fPiN4q+L3xr+IHi34pfFDxvfpqfi3x5441u+8Q+J9fvYrW3sLeXUNV1Gaa5lSz0+0tNOsLcOttYadaWmn2UMFnbQQR+cUUAFFFFABXpHwi+D3xU+PvxG8L/AAi+Cfw98XfFP4n+Nb59N8KeBPA2h3/iLxNrt3FbTXtwlhpenQz3MkdnY211f39yyrbWGn2t1fXs0FpbzTJ+sf8AwSn/AOCEf7bH/BVfX7PXPh74aPwk/ZutL57bxT+0x8R9NurbwTEYHv4bjTfh7oxmsdZ+K3iFLzTLvTLm08Kb9C0HUljtvGHiXwy1xafaf9RT/gmb/wAEk/2Pv+CVvwtTwN+zv4LXUPHms2EVr8Svj14xttMv/i78TpIr+81G3i1/XbOys7fTPD+my3rW+j+E/D1rpmhWVtbW09zbahrRvdYvQD+cn/gjD/waZ+BvgRfeHf2jv+Cm1t4T+L/xVtP7O1jwV+zFp00ms/C34aa1p2rpqNlrXxJ1+x1JNM+Luv8Ak2tnEfBn2CX4aaaJdTt9Wb4hLd2U2jf2waVpWmaFpem6Jomm2Gj6Lo9hZ6VpGkaVZ2+n6XpWl6fbx2lhpum2FpHDaWNhY2kMVrZ2drFFb21vFHDDGkaKov0UAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFfkB/wVt/b5/a3/AGM/hrpOi/sV/sB/tAftnfHT4kaTry+Fte8BeA9d8W/Bz4VS2UEloNc+Jc/g6LVvFF9q1nqV3o97o/gMad4atPGGmvqj23jrS5NGvoKKKAP8139sX9kP/gv/APt7fGTV/jp+1N+xn+3n8TvHWoNe22lLqX7PfxTh8O+CfD93q1/rMHg7wF4ci8Pf2Z4T8I6Zd6ldf2doumRJGgczXUt3eSTXUvyn/wAOaf8AgrL/ANI3/wBtP/xHT4o//M3RRQAf8Oaf+Csv/SN/9tP/AMR0+KP/AMzdH/Dmn/grL/0jf/bT/wDEdPij/wDM3RRQAf8ADmn/AIKy/wDSN/8AbT/8R0+KP/zN1/U7/wAENf8Ag1MtPiJ4b8IftZf8FPtC8a+GbR9e1S48HfsWa74c1vwL4kmHhDxamn2mt/Hq51qOx1u10HxJJoesPZ/DbSdKs31/wlqeg+JL3xjHaapJ4dJRQB/oCeBPAfgr4XeC/C/w5+HHhTQPA3gLwRoem+GfCHg7wrpVnofhzw14f0e2js9M0fRdI0+GCy0/T7G1ijht7a3hSNEXgZJJ6yiigAooooAKKKKACiiigD//2Q0KZW5kc3RyZWFtDQplbmRvYmoNCjE1IDAgb2JqDQo8PA0KL0ZpcnN0IDUzDQovTiA3DQovVHlwZSAvT2JqU3RtDQovRmlsdGVyIC9GbGF0ZURlY29kZQ0KL0xlbmd0aCA2NzENCj4+DQpzdHJlYW0NCnhevVRta9swEP4eyH/QH9AsWS+WIBSSdKVl61bWsBXKPujlnHqkdrCd0f37nRyHpKyFMUY/JHcn3d3z6JHOOWHTiSCcielEkpyh4Zxow9HmxKgcbdrmBh1JeG7Rmc2mk2z1awsku3Fr6DD6UMWO3AvCyJfvGC6bXd0TbJJ9ga7ZtQE6MpudnU0nuHQNsXKL5oncv2OMpJ+yKhkjczRYv08cYI6dXgKUB8A/e3LFBiMY2zfF0hawV55qnmEcOx+zxD4L8eseY8RSB6zTI6WCtgm30JP77Ob8gmRXj9hnMdrlaK9ItoKnPlXfffY/IPT7Wq+kdWBy6pgEKg0o6oX0VEUoIigIihmiRyrCqIKHqKmzjlPJvKfOO0sFF8ADt0VRelKMydLqyJWO1IhoqVSGUe+EoBj5gtnSBu2IGZO9yCVTHKiLUlBZFIyanGOyYUaXBkokQuyYbMCLEEpPg7cSaehALUSgwkeBHBw32JmzU5WzC1Rxf+IyCg+MKWp4GagUKlBvC025DCZCGYwKmuAT3GOVpfGRuZzqMuApgHOkKIDaHGKhgZugOD7KU6zT/5PbTQwwuN35fohX7Q7SDq4tXAcDwey6qZtu6wLeCF+s6KLZxJH8OXShrbZ90+KLGsldVG3XLx9cSwQOUfbRnUbfqtg/4KvRLE+3/r4OTazqNcGNel531WHh/1B9gaV8A5ZHvJHAJ/cIr8q4ceuOCDGmLoZppbpIU0pzoZPVWiRjBzPMddV1iD/wJMgy6dLD41c8nxz9y9G/6t2mCvN6vYH0UcuWbnsJ1fqhJ4ql+G6McpWieRfSmBc6BekcKaI4BkkjcIMKnFubOLinPX4xaDb/eUrn33V5VRIlnkki7V9KUhzcy737BoIM6C8J8hv18ahmDQplbmRzdHJlYW0NCmVuZG9iag0KMTYgMCBvYmoNCjw8DQovUm9vdCAxIDAgUg0KL0luZGV4IFswIDE3XQ0KL1NpemUgMTcNCi9UeXBlIC9YUmVmDQovVyBbMSAzIDFdDQovRmlsdGVyIC9GbGF0ZURlY29kZQ0KL0xlbmd0aCA3Nw0KPj4NCnN0cmVhbQ0KeF4dyjsNgEAURNE7yyd0gBUSPCBgPeAAGVhABT1mSOgRAPN2ilPcDN4nGEnQFxQktwUx3GbtzPaYM5v3Kr8qqIMmaKVpR5oPfmKECQINCmVuZHN0cmVhbQ0KZW5kb2JqDQoNCnN0YXJ0eHJlZg0KNzkyNjMNCiUlRU9GDQo=";

    let payLoadBase64DCP = "UHJpbnQgbWUh";

    let payLoadBase64Small =
      "JVBERi0xLjUNCiXi48/TDQoxIDAgb2JqDQo8PA0KL1R5cGUgL0NhdGFsb2cNCi9QYWdlcyAyIDAgUg0KPj4NCmVuZG9iag0KNSAwIG9iag0KPDwNCi9GaWx0ZXIgL0ZsYXRlRGVjb2RlDQovTGVuZ3RoIDU0NjQNCj4+DQpzdHJlYW0NCnhe7Z1tc9zHbcDfa0bf4d502nqG0AJY7AIvbdmxZatJaMlu8pKiqDQziR3ZmnbaT9/Fn+JRlmqbD+CpMGTTFEXf3d79dhdPiwVe3r/3T7uH/3Hy3V/OdqffH33/w/O/fnfy6mz343//+Ors77tX3+/+dvbi1YNX3/8D7t9DaG0Hr//b/4VY/I/d6d/9tZ7+cPLdj3/zl3j35X72JfbPX2/n+I3vBE1I5Y0n2Oj6xi+2J737mjRgvav1uCMDEtX5cw/8yevcfkzVvsYk6IMM7UqDPvj07D//enr29eef7B4++clfT3+8f6/t/N8f/nL/3vpfOvH0eTvjI+uiR/0529EzXD/NcXZ6gu3kxbPnuhMYw9bou6cv7t/75Km/xNMf7t/z8Z6evv7zv9a7am378X/Of4XAPE149/T5/XsDps2uu6eP79/7l4/+9Z/v3/vs6QWd2wCy9boy5zUB/ZYQMAM3blYZwQDF1rEwgt6AcU6ujIBBSZpURjDXIMSVxaE0ULZRWRxKB+6zUWUEE1Q698IIxhpk4BiVEXTQoVZZHA4FnuujF0YwEVTXVqiMQICtWWXrcCqoLfuwMAJdn7yJVNYIKmBIqpURGHQqbRwagdGUygrBBizrWCsrBDOwTpheIRwHhbebwVQbBw1vrzG1U2VZRMjLWTcpjWAsZ31U1slEbTnrvfRGIIb1EUpvBJrLWS9tnBK35ayX9tWJ+3LWS4cuiSdYa6U3QsflrGvpjdD7ctal9Eboupx1qhzAJlmP5FZ6I4gsb7302f7ySJe33iuHLmmsjy5opREImFhpN2nY+uijtJs0CWxyaetwDuiK6ZXicUzsEklhNHJLcVnNHekQwcttUPHgJXYYXZh2OeaghS1DZFqu6hzpEGAgAoE5POMuGYIvAxEY4MSeDsE3cQg6wVAPXiZD8PtABANQPXiJZcVht7UKZj4E4xe1Mq553XTq5UcQ4cG/hoPmAtc2U3WAmsyrGSkBg47NS1of3kz77tpzcPaCWV+MF0enfZwcdZN+dDLx5OiU+7OzZ897O30+wufA3lmGN0ewVFKfPR0CDESgy1FUSYegxyHwbC9pc1d4I3i215ZdkIvACCRg0NvMh2DGITAGIp2VpaFne+F2nNYyb4S37JIbm2m2/BUzO2y0YA26ViGmsw0/DzOPqRHMhvlc5U/uxDxGI2DxhHTuIGZ8nWV4q0Fty7WaoI37DebgvYiCFiYNcUlDPM+vyIUAAxEY8JR8CAItoyWM1LbbcbkQ2N3oxO7Zv3iOY0nmfh1hdJtB9cI0mzzkBnPwXhTCgztRCH66OZ3GAfXBNuaWdJlrFzyOEwRjyUKa+RB8EYdgLsNsbNdkcyH4OhCBQiPOZxft4hDoeiBtiVZ1EQg0HFwagYIgSWUEy0dqTWdpBALnlXTqErD1B1JhBLxcpG6zl0YgsFnilREYdN2ipWURIIGdX0qqi8ATDqm0LEADmzoqIyCG7q9bGcFYqwBLywJu0Mfk0ggYbLCURjChi5WWBb2BSXqFcBxzhHBZH3ttjkE8xpVwBAza2MXxAKU5cXftOdD/B2fKt0FgMGmapEPwdRiCteDIpuRD8Ic4BOuDtzl6OgT/HoZgK4HdvYJRMgRxSfheAhtN8onDB3EIJiwbfatslgvBwzAEWwnsjr2wOJQO1EQpHYLHcQiWUdq32yjJEDwKQ+AlsLdSdkdlNcJWApt7vo3wb3EIFLhpz2cXxBnIk0EWgXwIdnEIJjRD08IItG15ZZUJdMA2BlZGMGEgl14FhoB0nuNSFkGHsZ43KyNQwGUdWmEE2BBGZyqNQADPU53qIvBCNrOyZYRIgH7/oDKC9d5maaWI6IVsSrsISF7IZmJpBH4/sPfSCMxv6pbeCH6m2Kz0RuD1wjhKb4TegM77qdZFwDC59D7oE4i19D6QBrOXDhegHykKlyYwYY5WGsFAoDHT74PjN3546W//ZgmQqJ5+txYEIsJguRqNgDG3nr7Yurf6zpJ1+e07q/DmCJZ5Pls+BI/iEHhVr7HVnM2F4E+BCNYLy5yFEdAaculkK41gmefi7VwLI1j2eR+VZQGh2+dUWhZgdy+ttCzAZaCzlJYFtAx0Lm0XEHW/EDJKI1AP25WWBYxrFXg1n8IIZK2Cnk8ptkAE6qvAKiPoBEsxYmkEXuRvi9PURWB+nJdPKT6IQyAdeksYONsFIhiAs+XbCA/jEAzvK0xUeRUMhm49n1LEQATrhVElHQKKQzCbxwvyyQIORMAwLOEqCOySM+eyDtMBkDgA2tZ7w3zCMLAUvAroFMuuEo/f+OHlzc+4pznH1ndHqtC7N92+Co7bDbpWobEfpx+pX0lQSXqwfhsEyzzv20lOXQQTTMhbc5RFYOhVtfymbl0EfT1wK0dfF4F6nUUuTMCvptnWkKQwAoH1OMuOIMguGQpiveNB7ZLlJHLXVlkWLSdRlx2tlRFM4EHzN7cRsRm3y0/vv1BejtCv4EBeINlr478ukn81HgGj8tbQ1njZSJ53urvuLPBZ42dj4tGJnrmb9mIcnZyc6nLTTk5Z2/NTOj1bs8AqSuO6syAyZLlNPgsCsvR3w5+L290Cwlojy03IB4EiISwXlTzhKxsEuaMN6Qd8k/TAG3KNqpZxGiLXYrf17ryiTDYIIxCC0OsIWjYILRLCWFaCl4O2wttBbLMWd6VXwuDlNQgWh+A3pEZCmdADIbj/iCOhzSqREDo09MB2NgiRhrv7kDQTroS3zpuPo2xWZZBBh3Yi16hj8ck3DeNXpuFmkQ3iDt22CrkIfTs93V1jFm4+qHTvpIR9ebPCNzhwfS/xlKdhESVPzG3iZRWSIYgrzkx9rfbh58zJEDwMRNCXjbSlZOZC8FUgAl0yb7u/mAvBZ3EIlvfcVCi7OIxSSY1gkldhO2Qfobb0oLL8xgo73BTH1lBHPKrFBhNdV+/ufA7YvWb1bDwmMFTLMgcfhYkC7+IjW/gkN4KgZeilPrqfPB5wFfqYW1Cbqy5CNe9mN2n3YRH6t0tZ6OcdbbRDrMI3ZKF58ASTzsHLEFmYGkHQMvTuZovGPKgwXIP2JvkkQVz/nM5gzaakQxDXS6tP6GiaTyXGhWuWhza4maVDEJcD5bdHmVs+BN/GIZhg3asiJyPwcRgBb6iGbeZz0eLa6o0OawQ/0kuGIC5y6w3VrOXTB5+GEfCLk7MbFdYHU9Yi2FrNJkPwuzgECqbkRSWSIYizDZWg00xoGL3VW/E42kk7YKzg0knL5SfHmefe9rrPPtMhiOvw+YaTlgtBnHEqDYymXxlLhiAuWrE5aVuxq2QInsQhmLA+QT5p+McwAl7hpw8/y0yGIM4s8a7Xhl4AkqsqhOWkrUUwpbBZMNcDJzbd1dWJy0vrM6FVELgIzp20fPsgLl7jTpo1moUVgo71vXF62/A4xk9F1EWBDuunIk4wq2yZIXnKa2nzHEkAsefbiJ8HIlAQtHxKOc449e6aa4x8q2AXiEBAzPLJgqeBCAywcT67JM40w04wMN8ieBhIYFkg2vKleX0ViMBgtJZvH8TdgUBhWBJxZpeGx0H2+WjAE/mw9vkadEhCjYRxy3DwemTL56NQIIIJfavlkQyBxCGYDaxzvo3QAhF06NJ6aQRejXbOyggUoQ8pTaCDTSq9D7zc5rTS+8DWI7U2AfGeKVwawfrkLWF6SRwCv5otzbITOI5x0rYu7Dz0oE6aX86kQfnOtH/5VtxNe3d02a7tL4dtMiBO4mvMwS0GFZK27KIxgAnXbOyuOwfvpWHIu4lmN0agfpS3aGdDENdJTQh62/qkJEPwOA7B9O6ifkE/GYK4ZnJiy0/eWjUkQ/BJGILB6zvnk4YfxxFQby7qxzij6j6YCLOJJ9jUReBtJacHjJIhiOsxOwfoIiCFdaK2Ldku30b4Ig4BgzFJPp34y5fCboqDscOyEPigPooPqphwDuI6vDIqMPrdxGQIPo9DQEsti7vHyRDMQAQCNBMaJo8CESjM7U5UMgRP4hAwASnmE4ffBiIQmEPyOSkPAhEYyMB8Fnpc53futDQC5VsFb3nrxz+1zW4aTqcBxLIedzQb4GwHqaq2Ddo8v8QbhcxuWWL478aMbozA1utuyRXJEATWN1wfvG0FteoiGKBterHVZAjijlS9YsiS4yMdAo5DwGBomA/Bl3EIvDnMUE6H4JswBNJAefj1wGQIQqt38Bwtn1KMyz726h2CMgprBK+xKKpaGUFfq2BqPtNohCFY7kibcztVzYUg7obkJEBMuAjirqN49Q4008IKYSqoSa+sELx8R6eEllHcjRydMJCNC0tDtSULZs+3CuLsYyMwQstnH8fdljbvTsMJIyZxBbjNQNXbYmZDEBc9xcZe4i/fRvg4EMHw/IZReCMgNugD8ynFLwIReHsWzrcKAkvpoAHOhD5CnGmERDB0YmkEHYQon11AgQim13/OJw4D68sxgjTKFzR6GIhAoJ3nPeZC8FEgAm+pjLOyLOgMk0srBL8ZOdgqS8Nu0NiwsjQUclFglaWhDGjG+ezjwIKjYsss0HzGYWDEZPglxSaVxaHfUlTiykrRbymqjdIIxPubW2kECtp66Y2gBIxYeiOogGZML4hEMNd7SxguiDtY9spe2Hu+VTADEXQYnFAcQiAC9VWQz0COSzKhhjAyBo3uprbXG/eC1k+DludwFRy3H1QdzdEw6GNkmYIHcavQYBL1dATivDTmZZ5PkWwEHt7NNmz22mFzg1WYDrIN16DK3p9l+Uoik5NMQVyiFeHSAd2Dx7kI/CGQwACVdEsgrjcL0Xrm2AorpiKwCyTAoMPPk3MR+DaQwFym4MgGIC7JjLjB3LzDXAQeBxLowDrTEXgUSGCCUk+3Cz6NI9AZsLlbMquqgt6XReiNynIR+F0gAfXrB+nWQKBFKLjMAUpnEH0dSGACsnJhOSAK1lGzEQh0C8byw7Vj4V0wBEy8KU4uAoFW8RaZonTOcaBjNMkbhKUj8FkggQH5Pv9bftFxTJDSqxiRuJfonZLWP4eoIeY5vtNwiSFFWM+QLIHiOG3c1xN19FkZQQekrXluMgRxNRL6hMHCUhiBNDBivwKWDEGPQ9Cht4ziMC6JYXlnhuZmWTIEcUkMA6Ev+3xkRxBkmFwmMRzQMCEvK8st306My7f3LAbeylomQxCXb+9pDGQzn2HyxzvZiaLre/ejNOuwjJVxiDwGWctQ1CvG6FwL0uYuxxTEmcdjrcIhsycnEL4I/Y0MGlcr/X0ctAiNQbmp7VLOwcuIVZgbwR0swzUcHngV2vKVqCWdgpchqzA1gndWITZzc/PiXfsvlMV+jQeSAI52zqM1pKtZKAGjkp6Lgi7EN4ic8lnjZ2Pi0YmeefeFF+Po5ORUj0755JS1PT+l07M1CayiNK47CbJMk8XEJ0FA+vQ6W3fooVyGThEJtBntrjMJNxx0Hzp1AdSQs4Sv44TBZei0LoJ96DQZgri44WXotCyCy9BpMgR3ETpNhuAuQqfJENxF6DQ3giDD5DJ0ekDD5DJ0mmwO7iJ0mgzBXYROkyG4m9Dp5RUwT8C0SQc5xPBBh+/EvrzENQeYZA4CM373d8CSIQhM+cUBWxeSZAQCkx33t8CSIdgFIvBrYJJvFTwMROD3wJgqiwJuoOo93JMhCLwU7FfBcOZDEHgTyu+CWUJxGHgdri9HsUm+VRB4B6J3sOYxo153FVxcB+tlNcJyR4wmVdYIItAZ822EJ3EIRoPRZaZDEGggD+9P5iGbXlYWDAGdntKQDMFb4YLj8JDNBFPthwrZKI/zOBHaFSsFvf8pCHTT9hGbXAQC9+FF1Z5cAAI1MjXggekIBBrnHq7ZOkLlIhAZs3tdtScXgMD7yVusxoP3UlUTeKjG2igsBthgNHdNchEINMu3QA2l2wVHgQQGDBy98hq4CNPkIhDon2+HxpZuFwRWavAgTRuanMBxtGs6OrRp41Cu6Ryqmz9MyjNLeCCurPG5b+odi5MhCDRLN+fU2/UmQxAojd09FR2lESz/1OtnJSMQqJD2/mkuAoFRGvas85ZPGAbmebqHOrjyNvBcAiZLhyAwUrUvLJsMQaSH1sGQ8knDwFDNuZNK6RAEplNsuQRd0iEIDFkuNxWN8+nErwIR6JIFnSorxS2XwKiyOBzelK9xZaW4FZjlfKZRYLDAK8xOzLcRAr2kOTzTNB+Ct/KNj6Mjl7MBzqYHi1yWbhx+GbhMhiBQFnng0jDfKggMV3jg0nSWRuD3oLT0RqC5PFUuvRE8t0YkX+PwQB+FBzTr+bpmBxro+3tQyRAExiv296CSIfgiEEEH1YS90wNlwT52OcpuBEEYOvIpxbcCd8dBbppbitMjWCjA0trVas7eftDePLVAQaQjziRzELgTl3lq7HnfyRD8KRDBMk97s3QIAoXRdrLOpTcCd+ibhZ4MQWCOzTJPbZBVRuBH69OzPpMhCHRS/Gider5VEBi8dPNUZz5ZEBizkfVMpnwaITBmIwLCNkoj8BrfVnojDPKPXnojeOtWT/nPhuBu7kIgKYy2Vb09nKvqg4pivjmIuwuBTIDid5K0qjBC9vfW0xF4EkjAloGOpRF0giGs2cXxrRAMQNV8CH4fiMBLPrMUtktQ3FkXrbwRZLyuo5UMQdxtCBzrkYT54hW7QAQM57mvWncVTKAmlUXBbN4DgNIheBiIoMOSBKP0KpgwJaE0jLsVhOoFGxK6CIGusnaYw/IphLfugxwHRWyMAHs7bHKBD9pJMd0c4J3MwWUePk0Qpj4PMAfboLMvUUCuHL0jwy7FHMQZRlsivuuDZAQ+CyTwurplMgKBd+P25S2TIfg4EMFFO5JkCB4FInhdQCQZgbiYGTGBqLtIyRDsAhFsJS7nrrA03LLwNR2ByAz0ixqXVFYYnjcjyacP4s6RSBog8awsDb3M5aB8CiHQPzivc5lvIwQ6CF5AhNc7y4YgLnK6FRAhlMqrwLOctmN1KqsUhy3j0K8BJEMQFzm9LCCSDMHdRE4vo3bMMIjH4VrSmIcrBijNNBVMAn2UffmMZAgCnZSLLsLJCASaZvuyv8kQBHYmugzb5ULwTSCCi+oZyRDEJXpddhGmstLQ43Y6dVdYFuyrZ1BZWbCvnpEMQaBltO9OkwxBYNRq354mGYI/xyE4r/zbK4vDfRdhKisORUG329rJELzlIxyHO+vrB+wHK3VpW0sCJjBUyzIHX8Ytw30D2WQIAoXRvklPMgSBwmh562QJN0JkwOKi1mUyBIEpJp5k0zXfKgg8SDnvIzvSIfjz/6WVP3x9+Prw9eHrt/H1v+7cP+ANCmVuZHN0cmVhbQ0KZW5kb2JqDQoxMiAwIG9iag0KPDwNCi9GaXJzdCA2Ng0KL04gOQ0KL1R5cGUgL09ialN0bQ0KL0ZpbHRlciAvRmxhdGVEZWNvZGUNCi9MZW5ndGggNjM4DQo+Pg0Kc3RyZWFtDQp4Xu1WbWvbMBD+bvB/uD+gWe+SIRSSdKVj61a6sA3KPiiykhhSO9jO6P79JNlZkrWlo2yjjH646M5+pHvuQb4LBZwmDAhmacKBYpomErjgaaJAUp4mGlTuITkQLXSaEAyUsOAQoJyQNBmN0iSbfd84yC7N0rU+elsWLVwzwHD11YfTelt14KHZlWvrbWNdC6PRyUma+EcXrijNpL6F61cYQzCRi7BoTv3i9/fAmGZ/0n0J+S7h3TMlxmGhTPRn+p2N80fRsOUoxf7gPYr1KJ++6nzsU4ldqsOKwoamth9dB9fZ5ekZZDN32wXUmd/YA7QitsCOoZwLjXjBcjQn3lPSWUOwWcwLDXLI5xaM6YVcIMulQTwXHBlFDLKMz9284NgWEtQAZg6zuVQEGe0CeCGRMVZ7sLFM48JS60Af1nv4e1B7IOuDj9t5F+NZs3XhjX82Ma2LtWQXdVW3G2OdpmQyQ5N6XQx1nrrWNuWmqxvIB2pnZdN205VpgPmrlL0zh9HnsuhWXlKJaZDqdWXroqyW4F9U46otdw/+DNO7JP2NfgYsP0yv0ITgy7Kzq3tZkkdYUiGOacKLvdjfMCXFo5hfjWgsHwX9h/aUXrH/6oc28N7cuAc77tosW2BsgE7i1ENShWmHKJNx+EkWljwucT6Wbevzx2YRWIbu1LmbT0A4H/zzwX/TmXVpx9Vy7cI/hWxqNueuXK46EDjEX4aIihCNWxvmpZIhCHWECFEeO5UzUQVC8jxwMLd9fhX72PjbIZ2n6/KgJIIdScLz35RE7dzz3v0HgsTsTxfk7ijpBaHHgvBeDx2F8N9n1Edx8gwEwfFK/BTEk1MPKPIDuL2Odw0KZW5kc3RyZWFtDQplbmRvYmoNCjEzIDAgb2JqDQo8PA0KL1Jvb3QgMSAwIFINCi9JbmRleCBbMCAxNF0NCi9TaXplIDE0DQovVHlwZSAvWFJlZg0KL1cgWzEgMiAxXQ0KL0ZpbHRlciAvRmxhdGVEZWNvZGUNCi9MZW5ndGggNTMNCj4+DQpzdHJlYW0NCnheFcY7DQAhAETBt9zxKWgoSJCEfwlIwAAsxSQDHNEIVEwWxHz/7LdoybIV9Y3G4gJazAQADQplbmRzdHJlYW0NCmVuZG9iag0KDQpzdGFydHhyZWYNCjYzNzANCiUlRU9GDQo=";

    let payLoadBase64Large =
      "JVBERi0xLjUNCiXi48/TDQoxIDAgb2JqDQo8PA0KL1R5cGUgL0NhdGFsb2cNCi9QYWdlcyAyIDAgUg0KPj4NCmVuZG9iag0KNSAwIG9iag0KPDwNCi9GaWx0ZXIgL0ZsYXRlRGVjb2RlDQovTGVuZ3RoIDU0NjQNCj4+DQpzdHJlYW0NCnhe7Z1tc9zHbcDfa0bf4d502nqG0AJY7AIvbdmxZatJaMlu8pKiqDQziR3ZmnbaT9/Fn+JRlmqbD+CpMGTTFEXf3d79dhdPiwVe3r/3T7uH/3Hy3V/OdqffH33/w/O/fnfy6mz343//+Ors77tX3+/+dvbi1YNX3/8D7t9DaG0Hr//b/4VY/I/d6d/9tZ7+cPLdj3/zl3j35X72JfbPX2/n+I3vBE1I5Y0n2Oj6xi+2J737mjRgvav1uCMDEtX5cw/8yevcfkzVvsYk6IMM7UqDPvj07D//enr29eef7B4++clfT3+8f6/t/N8f/nL/3vpfOvH0eTvjI+uiR/0529EzXD/NcXZ6gu3kxbPnuhMYw9bou6cv7t/75Km/xNMf7t/z8Z6evv7zv9a7am378X/Of4XAPE149/T5/XsDps2uu6eP79/7l4/+9Z/v3/vs6QWd2wCy9boy5zUB/ZYQMAM3blYZwQDF1rEwgt6AcU6ujIBBSZpURjDXIMSVxaE0ULZRWRxKB+6zUWUEE1Q698IIxhpk4BiVEXTQoVZZHA4FnuujF0YwEVTXVqiMQICtWWXrcCqoLfuwMAJdn7yJVNYIKmBIqpURGHQqbRwagdGUygrBBizrWCsrBDOwTpheIRwHhbebwVQbBw1vrzG1U2VZRMjLWTcpjWAsZ31U1slEbTnrvfRGIIb1EUpvBJrLWS9tnBK35ayX9tWJ+3LWS4cuiSdYa6U3QsflrGvpjdD7ctal9Eboupx1qhzAJlmP5FZ6I4gsb7302f7ySJe33iuHLmmsjy5opREImFhpN2nY+uijtJs0CWxyaetwDuiK6ZXicUzsEklhNHJLcVnNHekQwcttUPHgJXYYXZh2OeaghS1DZFqu6hzpEGAgAoE5POMuGYIvAxEY4MSeDsE3cQg6wVAPXiZD8PtABANQPXiJZcVht7UKZj4E4xe1Mq553XTq5UcQ4cG/hoPmAtc2U3WAmsyrGSkBg47NS1of3kz77tpzcPaCWV+MF0enfZwcdZN+dDLx5OiU+7OzZ897O30+wufA3lmGN0ewVFKfPR0CDESgy1FUSYegxyHwbC9pc1d4I3i215ZdkIvACCRg0NvMh2DGITAGIp2VpaFne+F2nNYyb4S37JIbm2m2/BUzO2y0YA26ViGmsw0/DzOPqRHMhvlc5U/uxDxGI2DxhHTuIGZ8nWV4q0Fty7WaoI37DebgvYiCFiYNcUlDPM+vyIUAAxEY8JR8CAItoyWM1LbbcbkQ2N3oxO7Zv3iOY0nmfh1hdJtB9cI0mzzkBnPwXhTCgztRCH66OZ3GAfXBNuaWdJlrFzyOEwRjyUKa+RB8EYdgLsNsbNdkcyH4OhCBQiPOZxft4hDoeiBtiVZ1EQg0HFwagYIgSWUEy0dqTWdpBALnlXTqErD1B1JhBLxcpG6zl0YgsFnilREYdN2ipWURIIGdX0qqi8ATDqm0LEADmzoqIyCG7q9bGcFYqwBLywJu0Mfk0ggYbLCURjChi5WWBb2BSXqFcBxzhHBZH3ttjkE8xpVwBAza2MXxAKU5cXftOdD/B2fKt0FgMGmapEPwdRiCteDIpuRD8Ic4BOuDtzl6OgT/HoZgK4HdvYJRMgRxSfheAhtN8onDB3EIJiwbfatslgvBwzAEWwnsjr2wOJQO1EQpHYLHcQiWUdq32yjJEDwKQ+AlsLdSdkdlNcJWApt7vo3wb3EIFLhpz2cXxBnIk0EWgXwIdnEIJjRD08IItG15ZZUJdMA2BlZGMGEgl14FhoB0nuNSFkGHsZ43KyNQwGUdWmEE2BBGZyqNQADPU53qIvBCNrOyZYRIgH7/oDKC9d5maaWI6IVsSrsISF7IZmJpBH4/sPfSCMxv6pbeCH6m2Kz0RuD1wjhKb4TegM77qdZFwDC59D7oE4i19D6QBrOXDhegHykKlyYwYY5WGsFAoDHT74PjN3546W//ZgmQqJ5+txYEIsJguRqNgDG3nr7Yurf6zpJ1+e07q/DmCJZ5Pls+BI/iEHhVr7HVnM2F4E+BCNYLy5yFEdAaculkK41gmefi7VwLI1j2eR+VZQGh2+dUWhZgdy+ttCzAZaCzlJYFtAx0Lm0XEHW/EDJKI1AP25WWBYxrFXg1n8IIZK2Cnk8ptkAE6qvAKiPoBEsxYmkEXuRvi9PURWB+nJdPKT6IQyAdeksYONsFIhiAs+XbCA/jEAzvK0xUeRUMhm49n1LEQATrhVElHQKKQzCbxwvyyQIORMAwLOEqCOySM+eyDtMBkDgA2tZ7w3zCMLAUvAroFMuuEo/f+OHlzc+4pznH1ndHqtC7N92+Co7bDbpWobEfpx+pX0lQSXqwfhsEyzzv20lOXQQTTMhbc5RFYOhVtfymbl0EfT1wK0dfF4F6nUUuTMCvptnWkKQwAoH1OMuOIMguGQpiveNB7ZLlJHLXVlkWLSdRlx2tlRFM4EHzN7cRsRm3y0/vv1BejtCv4EBeINlr478ukn81HgGj8tbQ1njZSJ53urvuLPBZ42dj4tGJnrmb9mIcnZyc6nLTTk5Z2/NTOj1bs8AqSuO6syAyZLlNPgsCsvR3w5+L290Cwlojy03IB4EiISwXlTzhKxsEuaMN6Qd8k/TAG3KNqpZxGiLXYrf17ryiTDYIIxCC0OsIWjYILRLCWFaCl4O2wttBbLMWd6VXwuDlNQgWh+A3pEZCmdADIbj/iCOhzSqREDo09MB2NgiRhrv7kDQTroS3zpuPo2xWZZBBh3Yi16hj8ck3DeNXpuFmkQ3iDt22CrkIfTs93V1jFm4+qHTvpIR9ebPCNzhwfS/xlKdhESVPzG3iZRWSIYgrzkx9rfbh58zJEDwMRNCXjbSlZOZC8FUgAl0yb7u/mAvBZ3EIlvfcVCi7OIxSSY1gkldhO2Qfobb0oLL8xgo73BTH1lBHPKrFBhNdV+/ufA7YvWb1bDwmMFTLMgcfhYkC7+IjW/gkN4KgZeilPrqfPB5wFfqYW1Cbqy5CNe9mN2n3YRH6t0tZ6OcdbbRDrMI3ZKF58ASTzsHLEFmYGkHQMvTuZovGPKgwXIP2JvkkQVz/nM5gzaakQxDXS6tP6GiaTyXGhWuWhza4maVDEJcD5bdHmVs+BN/GIZhg3asiJyPwcRgBb6iGbeZz0eLa6o0OawQ/0kuGIC5y6w3VrOXTB5+GEfCLk7MbFdYHU9Yi2FrNJkPwuzgECqbkRSWSIYizDZWg00xoGL3VW/E42kk7YKzg0knL5SfHmefe9rrPPtMhiOvw+YaTlgtBnHEqDYymXxlLhiAuWrE5aVuxq2QInsQhmLA+QT5p+McwAl7hpw8/y0yGIM4s8a7Xhl4AkqsqhOWkrUUwpbBZMNcDJzbd1dWJy0vrM6FVELgIzp20fPsgLl7jTpo1moUVgo71vXF62/A4xk9F1EWBDuunIk4wq2yZIXnKa2nzHEkAsefbiJ8HIlAQtHxKOc449e6aa4x8q2AXiEBAzPLJgqeBCAywcT67JM40w04wMN8ieBhIYFkg2vKleX0ViMBgtJZvH8TdgUBhWBJxZpeGx0H2+WjAE/mw9vkadEhCjYRxy3DwemTL56NQIIIJfavlkQyBxCGYDaxzvo3QAhF06NJ6aQRejXbOyggUoQ8pTaCDTSq9D7zc5rTS+8DWI7U2AfGeKVwawfrkLWF6SRwCv5otzbITOI5x0rYu7Dz0oE6aX86kQfnOtH/5VtxNe3d02a7tL4dtMiBO4mvMwS0GFZK27KIxgAnXbOyuOwfvpWHIu4lmN0agfpS3aGdDENdJTQh62/qkJEPwOA7B9O6ifkE/GYK4ZnJiy0/eWjUkQ/BJGILB6zvnk4YfxxFQby7qxzij6j6YCLOJJ9jUReBtJacHjJIhiOsxOwfoIiCFdaK2Ldku30b4Ig4BgzFJPp34y5fCboqDscOyEPigPooPqphwDuI6vDIqMPrdxGQIPo9DQEsti7vHyRDMQAQCNBMaJo8CESjM7U5UMgRP4hAwASnmE4ffBiIQmEPyOSkPAhEYyMB8Fnpc53futDQC5VsFb3nrxz+1zW4aTqcBxLIedzQb4GwHqaq2Ddo8v8QbhcxuWWL478aMbozA1utuyRXJEATWN1wfvG0FteoiGKBterHVZAjijlS9YsiS4yMdAo5DwGBomA/Bl3EIvDnMUE6H4JswBNJAefj1wGQIQqt38Bwtn1KMyz726h2CMgprBK+xKKpaGUFfq2BqPtNohCFY7kibcztVzYUg7obkJEBMuAjirqN49Q4008IKYSqoSa+sELx8R6eEllHcjRydMJCNC0tDtSULZs+3CuLsYyMwQstnH8fdljbvTsMJIyZxBbjNQNXbYmZDEBc9xcZe4i/fRvg4EMHw/IZReCMgNugD8ynFLwIReHsWzrcKAkvpoAHOhD5CnGmERDB0YmkEHYQon11AgQim13/OJw4D68sxgjTKFzR6GIhAoJ3nPeZC8FEgAm+pjLOyLOgMk0srBL8ZOdgqS8Nu0NiwsjQUclFglaWhDGjG+ezjwIKjYsss0HzGYWDEZPglxSaVxaHfUlTiykrRbymqjdIIxPubW2kECtp66Y2gBIxYeiOogGZML4hEMNd7SxguiDtY9spe2Hu+VTADEXQYnFAcQiAC9VWQz0COSzKhhjAyBo3uprbXG/eC1k+DludwFRy3H1QdzdEw6GNkmYIHcavQYBL1dATivDTmZZ5PkWwEHt7NNmz22mFzg1WYDrIN16DK3p9l+Uoik5NMQVyiFeHSAd2Dx7kI/CGQwACVdEsgrjcL0Xrm2AorpiKwCyTAoMPPk3MR+DaQwFym4MgGIC7JjLjB3LzDXAQeBxLowDrTEXgUSGCCUk+3Cz6NI9AZsLlbMquqgt6XReiNynIR+F0gAfXrB+nWQKBFKLjMAUpnEH0dSGACsnJhOSAK1lGzEQh0C8byw7Vj4V0wBEy8KU4uAoFW8RaZonTOcaBjNMkbhKUj8FkggQH5Pv9bftFxTJDSqxiRuJfonZLWP4eoIeY5vtNwiSFFWM+QLIHiOG3c1xN19FkZQQekrXluMgRxNRL6hMHCUhiBNDBivwKWDEGPQ9Cht4ziMC6JYXlnhuZmWTIEcUkMA6Ev+3xkRxBkmFwmMRzQMCEvK8st306My7f3LAbeylomQxCXb+9pDGQzn2HyxzvZiaLre/ejNOuwjJVxiDwGWctQ1CvG6FwL0uYuxxTEmcdjrcIhsycnEL4I/Y0MGlcr/X0ctAiNQbmp7VLOwcuIVZgbwR0swzUcHngV2vKVqCWdgpchqzA1gndWITZzc/PiXfsvlMV+jQeSAI52zqM1pKtZKAGjkp6Lgi7EN4ic8lnjZ2Pi0YmeefeFF+Po5ORUj0755JS1PT+l07M1CayiNK47CbJMk8XEJ0FA+vQ6W3fooVyGThEJtBntrjMJNxx0Hzp1AdSQs4Sv44TBZei0LoJ96DQZgri44WXotCyCy9BpMgR3ETpNhuAuQqfJENxF6DQ3giDD5DJ0ekDD5DJ0mmwO7iJ0mgzBXYROkyG4m9Dp5RUwT8C0SQc5xPBBh+/EvrzENQeYZA4CM373d8CSIQhM+cUBWxeSZAQCkx33t8CSIdgFIvBrYJJvFTwMROD3wJgqiwJuoOo93JMhCLwU7FfBcOZDEHgTyu+CWUJxGHgdri9HsUm+VRB4B6J3sOYxo153FVxcB+tlNcJyR4wmVdYIItAZ822EJ3EIRoPRZaZDEGggD+9P5iGbXlYWDAGdntKQDMFb4YLj8JDNBFPthwrZKI/zOBHaFSsFvf8pCHTT9hGbXAQC9+FF1Z5cAAI1MjXggekIBBrnHq7ZOkLlIhAZs3tdtScXgMD7yVusxoP3UlUTeKjG2igsBthgNHdNchEINMu3QA2l2wVHgQQGDBy98hq4CNPkIhDon2+HxpZuFwRWavAgTRuanMBxtGs6OrRp41Cu6Ryqmz9MyjNLeCCurPG5b+odi5MhCDRLN+fU2/UmQxAojd09FR2lESz/1OtnJSMQqJD2/mkuAoFRGvas85ZPGAbmebqHOrjyNvBcAiZLhyAwUrUvLJsMQaSH1sGQ8knDwFDNuZNK6RAEplNsuQRd0iEIDFkuNxWN8+nErwIR6JIFnSorxS2XwKiyOBzelK9xZaW4FZjlfKZRYLDAK8xOzLcRAr2kOTzTNB+Ct/KNj6Mjl7MBzqYHi1yWbhx+GbhMhiBQFnng0jDfKggMV3jg0nSWRuD3oLT0RqC5PFUuvRE8t0YkX+PwQB+FBzTr+bpmBxro+3tQyRAExiv296CSIfgiEEEH1YS90wNlwT52OcpuBEEYOvIpxbcCd8dBbppbitMjWCjA0trVas7eftDePLVAQaQjziRzELgTl3lq7HnfyRD8KRDBMk97s3QIAoXRdrLOpTcCd+ibhZ4MQWCOzTJPbZBVRuBH69OzPpMhCHRS/Gider5VEBi8dPNUZz5ZEBizkfVMpnwaITBmIwLCNkoj8BrfVnojDPKPXnojeOtWT/nPhuBu7kIgKYy2Vb09nKvqg4pivjmIuwuBTIDid5K0qjBC9vfW0xF4EkjAloGOpRF0giGs2cXxrRAMQNV8CH4fiMBLPrMUtktQ3FkXrbwRZLyuo5UMQdxtCBzrkYT54hW7QAQM57mvWncVTKAmlUXBbN4DgNIheBiIoMOSBKP0KpgwJaE0jLsVhOoFGxK6CIGusnaYw/IphLfugxwHRWyMAHs7bHKBD9pJMd0c4J3MwWUePk0Qpj4PMAfboLMvUUCuHL0jwy7FHMQZRlsivuuDZAQ+CyTwurplMgKBd+P25S2TIfg4EMFFO5JkCB4FInhdQCQZgbiYGTGBqLtIyRDsAhFsJS7nrrA03LLwNR2ByAz0ixqXVFYYnjcjyacP4s6RSBog8awsDb3M5aB8CiHQPzivc5lvIwQ6CF5AhNc7y4YgLnK6FRAhlMqrwLOctmN1KqsUhy3j0K8BJEMQFzm9LCCSDMHdRE4vo3bMMIjH4VrSmIcrBijNNBVMAn2UffmMZAgCnZSLLsLJCASaZvuyv8kQBHYmugzb5ULwTSCCi+oZyRDEJXpddhGmstLQ43Y6dVdYFuyrZ1BZWbCvnpEMQaBltO9OkwxBYNRq354mGYI/xyE4r/zbK4vDfRdhKisORUG329rJELzlIxyHO+vrB+wHK3VpW0sCJjBUyzIHX8Ytw30D2WQIAoXRvklPMgSBwmh562QJN0JkwOKi1mUyBIEpJp5k0zXfKgg8SDnvIzvSIfjz/6WVP3x9+Prw9eHrt/H1v+7cP+ANCmVuZHN0cmVhbQ0KZW5kb2JqDQoxMiAwIG9iag0KPDwNCi9GaXJzdCA2Ng0KL04gOQ0KL1R5cGUgL09ialN0bQ0KL0ZpbHRlciAvRmxhdGVEZWNvZGUNCi9MZW5ndGggNjM4DQo+Pg0Kc3RyZWFtDQp4Xu1WbWvbMBD+bvB/uD+gWe+SIRSSdKVj61a6sA3KPiiykhhSO9jO6P79JNlZkrWlo2yjjH646M5+pHvuQb4LBZwmDAhmacKBYpomErjgaaJAUp4mGlTuITkQLXSaEAyUsOAQoJyQNBmN0iSbfd84yC7N0rU+elsWLVwzwHD11YfTelt14KHZlWvrbWNdC6PRyUma+EcXrijNpL6F61cYQzCRi7BoTv3i9/fAmGZ/0n0J+S7h3TMlxmGhTPRn+p2N80fRsOUoxf7gPYr1KJ++6nzsU4ldqsOKwoamth9dB9fZ5ekZZDN32wXUmd/YA7QitsCOoZwLjXjBcjQn3lPSWUOwWcwLDXLI5xaM6YVcIMulQTwXHBlFDLKMz9284NgWEtQAZg6zuVQEGe0CeCGRMVZ7sLFM48JS60Af1nv4e1B7IOuDj9t5F+NZs3XhjX82Ma2LtWQXdVW3G2OdpmQyQ5N6XQx1nrrWNuWmqxvIB2pnZdN205VpgPmrlL0zh9HnsuhWXlKJaZDqdWXroqyW4F9U46otdw/+DNO7JP2NfgYsP0yv0ITgy7Kzq3tZkkdYUiGOacKLvdjfMCXFo5hfjWgsHwX9h/aUXrH/6oc28N7cuAc77tosW2BsgE7i1ENShWmHKJNx+EkWljwucT6Wbevzx2YRWIbu1LmbT0A4H/zzwX/TmXVpx9Vy7cI/hWxqNueuXK46EDjEX4aIihCNWxvmpZIhCHWECFEeO5UzUQVC8jxwMLd9fhX72PjbIZ2n6/KgJIIdScLz35RE7dzz3v0HgsTsTxfk7ijpBaHHgvBeDx2F8N9n1Edx8gwEwfFK/BTEk1MPKPIDuL2Odw0KZW5kc3RyZWFtDQplbmRvYmoNCjEzIDAgb2JqDQo8PA0KL1Jvb3QgMSAwIFINCi9JbmRleCBbMCAxNF0NCi9TaXplIDE0DQovVHlwZSAvWFJlZg0KL1cgWzEgMiAxXQ0KL0ZpbHRlciAvRmxhdGVEZWNvZGUNCi9MZW5ndGggNTMNCj4+DQpzdHJlYW0NCnheFcY7DQAhAETBt9zxKWgoSJCEfwlIwAAsxSQDHNEIVEwWxHz/7LdoybIV9Y3G4gJazAQADQplbmRzdHJlYW0NCmVuZG9iag0KDQpzdGFydHhyZWYNCjYzNzANCiUlRU9GDQo=";

    let payLoadBase64SVG = "";

    switch (printType) {
      case "BTP":
        return payLoadBase64BTP;
      case "DCP":
        return payLoadBase64DCP;
      case "Small":
        return payLoadBase64Small;
      case "Large":
        return payLoadBase64Large;
      case "SVG":
        return payLoadBase64SVG;
      default:
        return "";
    }
  }

  public printDevices(printType: string, printText: string = undefined) {
    let payLoadBase64 = this.getPrintTypePayload(printType);
    this.printType = printType;

    if (printType === "DCP" && printText === "") {
      this.statusMessage = "Print Text is required";
      this.printStatus = 2;
      return;
    }
    this.printDocument(payLoadBase64);
  }

}
