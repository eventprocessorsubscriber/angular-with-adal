// tslint:disable
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Injectable, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { By } from '@angular/platform-browser';
//import { Observable } from 'rxjs/Observable';
//import 'rxjs/add/observable/of';
//import 'rxjs/add/observable/throw';

import { Component, Directive } from '@angular/core';
import { HomeComponent } from './home.component';
import { HomeService } from '../services/home.service';
import { ActivatedRoute } from '@angular/router';
import { ModalManager } from 'ngb-modal';
import { SignalRService } from '../services/signalR.service';
//import {HomeService, ActivatedRoute, ModalManager} from '@aspnet/signalr';

describe('HomeComponent', () => {
  let fixture;
  let component;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent
      ],
      providers: [
        HomeService,
        ActivatedRoute,
        ModalManager,
        SignalRService
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create a component', async () => {
    expect(component).toBeTruthy();
  });


  it('should run #GetPrintResponseMessgae() for DCP', async () => {
    const result = component.GetPrintResponseMessgae("DCP");
    expect(result).toBe("Print OK (Text)");
  });

  it('should run #GetPrintResponseMessgae() for Other print types', async () => {
    const result = component.GetPrintResponseMessgae("DCP");
    expect(result).not.toBe("Print OK (Text)");
  });

  it('should run #SignalRConnect()', async () => {
    // const result = component.SignalRConnect();
  });

  it('should run #scanDevicesFilter()', async () => {
    // const result = component.scanDevicesFilter(scanCapabilities);
  });

  it('should run #openModal()', async () => {
    // const result = component.openModal();
  });

  it('should run #closeModal()', async () => {
    // const result = component.closeModal();
  });

  it('should run #Disable()', async () => {
    // const result = component.Disable(location);
  });

  it('should run #GetSessionToken()', async () => {
    // const result = component.GetSessionToken(locationID);
  });

  it('should run #onTabChange()', async () => {
    // const result = component.onTabChange(e);
  });

  it('should run #change()', async () => {
    // const result = component.change(e, type);
  });

  it('should run #onClickAcquireStandard()', async () => {
    // const result = component.onClickAcquireStandard(selectedCheckList);
  });

  it('should run #onClickAcquireExclusive()', async () => {
    // const result = component.onClickAcquireExclusive(selectedCheckList);
  });

  it('should run #onClickRelease()', async () => {
    // const result = component.onClickRelease(selectedCheckList);
  });

  it('should run #onClickReleaseAll()', async () => {
    // const result = component.onClickReleaseAll(selectedCheckList);
  });

  it('should run #onClickEnable()', async () => {
    // const result = component.onClickEnable(selectedCheckList);
  });

  it('should run #onClickEnableShared()', async () => {
    // const result = component.onClickEnableShared(selectedCheckList);
  });

  it('should run #onClickDisable()', async () => {
    // const result = component.onClickDisable(selectedCheckList);
  });

  it('should run #onClickPrint()', async () => {
    // const result = component.onClickPrint(selectedCheckList);
  });

  it('should run #findIndexToUpdate()', async () => {
    // const result = component.findIndexToUpdate(type);
  });

  it('should run #EnableShared()', async () => {
    // const result = component.EnableShared(location);
  });

  it('should run #EnableExclusive()', async () => {
    // const result = component.EnableExclusive(location);
  });

  it('should run #Release()', async () => {
    // const result = component.Release(location);
  });

  it('should run #ReleaseAll()', async () => {
    // const result = component.ReleaseAll();
  });

  it('should run #doAcquire()', async () => {
    // const result = component.doAcquire(location, accessType);
  });

  it('should run #UpdateStatus()', async () => {
    // const result = component.UpdateStatus(checkedDevices, status);
  });

  it('should run #UpdateDeviceDefaultStatus()', async () => {
    // const result = component.UpdateDeviceDefaultStatus(printCapabilities, scanCapabilities);
  });

  it('should run #trimLocationName()', async () => {
    // const result = component.trimLocationName(str);
  });

  it('should run #ResetCheckBox()', async () => {
    // const result = component.ResetCheckBox();
  });

  it('should run #RefreshModel()', async () => {
    // const result = component.RefreshModel();
  });

  it('should run #printDocument()', async () => {
    // const result = component.printDocument(payLoadBase64);
  });

  it('should run #changeNoOfPrints()', async () => {
    // const result = component.changeNoOfPrints(printValue);
  });

  it('should run #printSmall()', async () => {
    // const result = component.printSmall();
  });

  it('should run #printLarge()', async () => {
    // const result = component.printLarge();
  });

  it('should run #printSvg()', async () => {
    // const result = component.printSvg();
  });

  it('should run #printBagTag()', async () => {
    // const result = component.printBagTag();
  });

  it('should run #printDCP()', async () => {
    // const result = component.printDCP(documentMessage);
  });

  it('should run #AddAPITag()', async () => {
    // const result = component.AddAPITag(locatorName);
  });

});
