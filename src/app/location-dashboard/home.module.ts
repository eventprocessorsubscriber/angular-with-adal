import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TabModule } from 'angular-tabs-component';
import { ModalModule } from 'ngb-modal';
import { HomeComponent } from './components/home.component';
import { routes } from './home.routing';
import { capabilitiesFilter } from '../shared/customFilter/capabilitiesFilter';
import { HomeService } from '../location-dashboard/services/home.service';
import { SignalRService } from './services/signalR.service';

@NgModule({
  imports: [
    CommonModule,
    routes,
    TabModule,
    ModalModule,
    FormsModule
  ],
  declarations: [HomeComponent, capabilitiesFilter],
  providers: [HomeService, SignalRService]
})
export class HomeModule { }
