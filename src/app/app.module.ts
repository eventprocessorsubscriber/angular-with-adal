import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { APP_CONFIG } from "./app.config";
import { HomeModule } from "./location-dashboard/home.module";
import { CoreModule } from "./core/core.module";
import { SharedModule } from "./shared/shared.module";
import { FrameRedirectModule } from "./frameredirect/frameredirect.module";
import { LocationModule } from "./home-location-search/locator.module";
import { AuthModule } from "./auth/auth.module";
import { AngularFontAwesomeModule } from "angular-font-awesome";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ModalModule } from 'ngb-modal';


/*

client_id:327b739c-1fc2-4ea9-b00e-f263abddd71c
client_secret:jM1N7iPej1FpEwDvmMfiqgr/c3eIFuK7wRRUpnfHOiY=
resource:dcade34b-1e58-4a4b-ba2a-f808ab61e1fe 
*/

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    HomeModule,
    CoreModule,
    FrameRedirectModule,
    LocationModule,
    AuthModule,
    AngularFontAwesomeModule,
      NgbModule,
      ModalModule
  ],

  providers: [
    {
      provide: APP_CONFIG,
      useValue: {
        apiEndpoint:
          "https://sita-bda-eastus-app-deviceservice.azurewebsites.net",
        clientId: "327b739c-1fc2-4ea9-b00e-f263abddd71c", //'b8534066-7f18-49c0-8345-e85f03094808',
        tenantId: "BOHDEV.onmicrosoft.com", //'66ef6009-80cf-460b-83f1-aaa2cbcb890b',
        resource: "dcade34b-1e58-4a4b-ba2a-f808ab61e1fe", //'644645ad-9314-4ab7-9016-5a16213fea9c',
        redirectUri: "http://localhost:4200/frameredirect" // AAD Client-App's RedirectUri
        //redirectUri: "https://sita-bda-eastus-app-rbaapp.azurewebsites.net/frameredirect" // AAD Client-App's RedirectUri

        // apiEndpoint: 'https://sita-bda-eastus-app-deviceservice.azurewebsites.net',// WebAPI endpoint for Values
        // clientId: '327b739c-1fc2-4ea9-b00e-f263abddd71c',   // ClientID from AAD Client-App
        // tenantId: 'BOHDEV.onmicrosoft.com',   // AAD TenantID
        // resource: 'dcade34b-1e58-4a4b-ba2a-f808ab61e1fe',   // ClientID from AAD Server-App
        // redirectUri: 'http://localhost:4200', // AAD Client-App's RedirectUri
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
