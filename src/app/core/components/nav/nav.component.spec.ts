import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { AdalService } from '../../../shared/services/adal.service';
import { NavComponent } from './nav.component';
import { AdalConfigService } from '../../../shared/services/adal-config.service';
import { APP_CONFIG, AppConfig } from "../../../app.config";
import { Injectable, Inject } from "@angular/core";
import { Observable, Subscriber } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { retry } from 'rxjs/operators';
import { adal } from 'adal-angular';

//declare var AuthenticationContext: adal.AuthenticationContextStatic;
let createAuthContextFn: adal.AuthenticationContextStatic// = AuthenticationContext;
export class AdalServiceMock {

  //public context: adal.AuthenticationContext;
  public context = {
    login: () => { }, logOut: () => { }, handleWindowCallback: () => { }, isCallback: (a) => { }, getLoginError: () => { },
    acquireToken: (a, b) => { }

  }
  constructor(private configService: ConfigServiceMock) {
    // this.context = new createAuthContextFn(configService.adalSettings);
  }
  userInfor = {
    isAuthenticated: true
  };

  login() {
    this.context.login();
  }
  logout() {
    this.context.logOut();
  }
  get authContext() {
    return this.context;
  }
  handleWindowCallback() {
    this.context.handleWindowCallback();
  }
  public get userInfo() {
    return this.userInfor;
  }
  public get accessToken() {
    return "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IndVTG1ZZnNxZFF1V3RWXy1oeFZ0REpKWk00USIsImtpZCI6IndVTG1ZZnNxZFF1V3RWXy1oeFZ0REpKWk00USJ9.eyJhdWQiOiJkY2FkZTM0Yi0xZTU4LTRhNGItYmEyYS1mODA4YWI2MWUxZmUiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC8zYjI3ZDAyZS0wYWQzLTRlMDItOTQ3ZC1kZDQ3Y2Y4NjYyNGYvIiwiaWF0IjoxNTQxNDU1MjAyLCJuYmYiOjE1NDE0NTUyMDIsImV4cCI6MTU0MTQ1OTEwMiwiYWlvIjoiNDJSZ1lQRExja3dWNmZ0N1VtZDJrbEdOd3F4UUFBPT0iLCJhcHBpZCI6IjMyN2I3MzljLTFmYzItNGVhOS1iMDBlLWYyNjNhYmRkZDcxYyIsImFwcGlkYWNyIjoiMSIsImlkcCI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0LzNiMjdkMDJlLTBhZDMtNGUwMi05NDdkLWRkNDdjZjg2NjI0Zi8iLCJvaWQiOiJkYjM1MjQzZS1mYzIwLTQ1MmUtODc4Ni0wYTEzZjEwZThjYTgiLCJyb2xlcyI6WyJLaW9zayJdLCJzdWIiOiJkYjM1MjQzZS1mYzIwLTQ1MmUtODc4Ni0wYTEzZjEwZThjYTgiLCJ0aWQiOiIzYjI3ZDAyZS0wYWQzLTRlMDItOTQ3ZC1kZDQ3Y2Y4NjYyNGYiLCJ1dGkiOiJ0UklORVl1VTVrV1ZYUVhyMkN4OEFBIiwidmVyIjoiMS4wIn0.jg8l4ytkOfUkZVjLBtiPzJVLMNS5F85Ay_TXWjW-CHnGqEqnNJCIJmDIbbdvD-C63S4w6wSmbm0hAQPfXHZgl-5qq9Zk2kD2ihK7_qJfgr-aql_hqPOtAeFWPQSdET46ajrmDxkRfLfF7yri9nxDKTBgSSuo4mRbvfI-lNpMtcxTsDEPLxQiQGkK0Cz_stluJg9UJyi_S8nunj1EaiReK91HOT2j-mTG5kLdBvXQ_uEz3Z0t5prXrHeP8HnOh1q6iaP8lCPEg6T0pJDIgHK5tml-HJlIUicDQVxX76rVfvE4Kc8wpJd3WNwW9Z1Xcgk9h4ZnV-cMzgt6GBDabTQdaw";
  }
  public get isAuthenticated() {
    return true;
  }

  public isCallback(hash: string) {
    return this.context.isCallback(hash);
  }

  public getLoginError() {
    return this.context.getLoginError();
  }

  public getAccessToken(endpoint: string, callbacks: (message: string, token: string) => any) {
    return "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IndVTG1ZZnNxZFF1V3RWXy1oeFZ0REpKWk00USIsImtpZCI6IndVTG1ZZnNxZFF1V3RWXy1oeFZ0REpKWk00USJ9.eyJhdWQiOiJkY2FkZTM0Yi0xZTU4LTRhNGItYmEyYS1mODA4YWI2MWUxZmUiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC8zYjI3ZDAyZS0wYWQzLTRlMDItOTQ3ZC1kZDQ3Y2Y4NjYyNGYvIiwiaWF0IjoxNTQxNDU1MjAyLCJuYmYiOjE1NDE0NTUyMDIsImV4cCI6MTU0MTQ1OTEwMiwiYWlvIjoiNDJSZ1lQRExja3dWNmZ0N1VtZDJrbEdOd3F4UUFBPT0iLCJhcHBpZCI6IjMyN2I3MzljLTFmYzItNGVhOS1iMDBlLWYyNjNhYmRkZDcxYyIsImFwcGlkYWNyIjoiMSIsImlkcCI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0LzNiMjdkMDJlLTBhZDMtNGUwMi05NDdkLWRkNDdjZjg2NjI0Zi8iLCJvaWQiOiJkYjM1MjQzZS1mYzIwLTQ1MmUtODc4Ni0wYTEzZjEwZThjYTgiLCJyb2xlcyI6WyJLaW9zayJdLCJzdWIiOiJkYjM1MjQzZS1mYzIwLTQ1MmUtODc4Ni0wYTEzZjEwZThjYTgiLCJ0aWQiOiIzYjI3ZDAyZS0wYWQzLTRlMDItOTQ3ZC1kZDQ3Y2Y4NjYyNGYiLCJ1dGkiOiJ0UklORVl1VTVrV1ZYUVhyMkN4OEFBIiwidmVyIjoiMS4wIn0.jg8l4ytkOfUkZVjLBtiPzJVLMNS5F85Ay_TXWjW-CHnGqEqnNJCIJmDIbbdvD-C63S4w6wSmbm0hAQPfXHZgl-5qq9Zk2kD2ihK7_qJfgr-aql_hqPOtAeFWPQSdET46ajrmDxkRfLfF7yri9nxDKTBgSSuo4mRbvfI-lNpMtcxTsDEPLxQiQGkK0Cz_stluJg9UJyi_S8nunj1EaiReK91HOT2j-mTG5kLdBvXQ_uEz3Z0t5prXrHeP8HnOh1q6iaP8lCPEg6T0pJDIgHK5tml-HJlIUicDQVxX76rVfvE4Kc8wpJd3WNwW9Z1Xcgk9h4ZnV-cMzgt6GBDabTQdaw";
  }

  public acquireTokenResilient(resource: string): Observable<any> {
    return new Observable<any>((subscriber: Subscriber<any>) =>
      this.context.acquireToken(resource, (message: string, token: string) => {
        if (token) {
          subscriber.next(token);
        } else {
          subscriber.error(message);
        }
      })
    ).pipe(retry(3));
  }

}
class ConfigServiceMock {

  get adalSettings() {
    return {
      tenant: 'ngpoffshoredev.onmicrosoft.com',
      clientId: '6e51203e-1213-430d-a6fd-2ce6904540d8',
      redirectUri: 'https://test.azurewebsites.net',
      postLogoutRedirectUri: window.location.origin + '/',
      navigateToLoginRequestUrl: true
    }
  }
  constructor(@Inject(APP_CONFIG) private config: AppConfig) {
  }
}


describe('NavComponent', () => {

  let component: NavComponent;
  //let  service: AdalService;
  //let serviceConfig: AdalConfigService;


  beforeEach(() => {

    TestBed.configureTestingModule({
      providers: [AdalService,
        {
          provide: AdalService, useClass: AdalServiceMock
        },
        {
          provide: AdalConfigService, useClass: ConfigServiceMock
        }
      ]
    });
    component = new NavComponent(this.AdalService)
  });


  it('should create the app', () => {
    // spyOn(service.isAuthenticated, 'isAuthenticated').and.returnValue(true);

    const fixture = TestBed.createComponent(this.component);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should be created', inject([NavComponent], (service: AdalService) => {
    expect(service).toBeTruthy();
  }));
});
