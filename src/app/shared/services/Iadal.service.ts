import { Injectable } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';
import { retry } from 'rxjs/operators';
import { AdalConfigService } from './adal-config.service';
import { adal } from "./adal";

export interface IAdalService {

    login(): void;

    logout(): void;

    authContext: adal.AuthenticationContext;

    handleWindowCallback(): void;

    userInfo: adal.User;

    accessToken: string;

    isAuthenticated: string;

    isCallback(hash: string): boolean;

    getLoginError(): string;

    getAccessToken(endpoint: string, callbacks: (message: string, token: string) => any): void;

    acquireTokenResilient(resource: string): Observable<any>;
} 
