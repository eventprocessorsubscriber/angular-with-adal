import { Pipe, PipeTransform } from '@angular/core';  
import { Capabilities } from '../../models/capabilities';  
  
@Pipe({
  name: 'myfilter',
  pure: false
})

export class capabilitiesFilter implements PipeTransform {
  transform(items: any[], filter: Capabilities): any {
    if (!items || !filter) {
      return items;
    }
    return items.filter(item => item.logicalType.indexOf(filter.logicalType) !== -1);
  }
}
