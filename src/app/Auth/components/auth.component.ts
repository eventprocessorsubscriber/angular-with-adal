import { Component, OnInit, Inject } from "@angular/core";
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AdalService } from '../../shared/services/adal.service';

@Component({
  selector: "app-location",
  templateUrl: "./auth.component.html",
  styleUrls: ["./auth.component.css"]
})
export class authComponent implements OnInit { 
  show: boolean = false;
  constructor(private route: ActivatedRoute, private router: Router, private adalService: AdalService) { }

  ngOnInit(): void {
    if (this.adalService.isAuthenticated) {
      this.show = true;
      this.router.navigate(['/locator']);
    }
  }
}

