import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { authComponent } from './components/auth.component';
import { routes } from './auth.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    routes,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [authComponent]
})
export class AuthModule { }
