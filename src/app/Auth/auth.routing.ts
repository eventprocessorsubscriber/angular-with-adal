import { RouterModule } from "@angular/router";
import { authComponent } from "./components/auth.component";
import { AdalAccessGuard } from "../shared/guards/adal-access.guard";

export const routes = RouterModule.forChild([
  {
    path: 'auth',
    component: authComponent,
    canActivate: [AdalAccessGuard]
  }
])
