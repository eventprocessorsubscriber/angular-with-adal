import { RouterModule } from "@angular/router";
import { locationComponent } from "./components/location.component";
import { AdalAccessGuard } from "../shared/guards/adal-access.guard";

export const routes = RouterModule.forChild([
  {
    path: 'locator',
    component: locationComponent,
    canActivate: [AdalAccessGuard]
  }
])
