import { Component, OnInit, Inject } from "@angular/core";
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
//import { Router } from '@angular/router-deprecated';

@Component({
  selector: "app-location",
  templateUrl: "./location.component.html",
  styleUrls: ["./location.component.css"]
})
export class locationComponent implements OnInit {
  locationID: string = "";
  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {

  }

  submitlocationId(form: NgForm) {  
    let locationID = form.value.locationId;

  
    this.router.navigate(['/locationId'], { queryParams: { id: locationID } });
  }

}

