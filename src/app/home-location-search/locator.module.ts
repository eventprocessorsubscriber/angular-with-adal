import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { locationComponent } from './components/location.component';
import { routes } from './locator.routing';
//import { ValueService } from './services/location.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    routes,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [locationComponent],
  //providers: [ValueService]
})
export class LocationModule { }
