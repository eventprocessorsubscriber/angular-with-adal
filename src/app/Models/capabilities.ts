export class Capabilities
{
  constructor(
    public locator: string,
    public status: string,
    public logicalType: string,
    public ReserveStatus:string,
    public checked: boolean
  ) { }

  static readonly Empty_Model = {

    locator: '',
    status: '',
    //logicalType:''
  };
}
