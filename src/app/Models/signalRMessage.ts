export class SignalRMessage {
  constructor(
    public PayloadAsJson: any,
    public PayloadType: string,
    public PayloadVersion: string,
  ) { }

  static readonly DefaultModel = {
    PayloadAsJson: {},
    PayloadType: '',
    PayloadVersion: ''
  };
}