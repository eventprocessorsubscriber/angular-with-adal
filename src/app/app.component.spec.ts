import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
//import { ApiService } from './core';
import { Router, NavigationError, NavigationCancel, NavigationStart, NavigationEnd } from '@angular/router';
import { AppComponent } from './app.component';
import { AdalService } from './shared/services/adal.service';
import { AdalServiceMock } from './core/components/nav/nav.component.spec';
//import { SignalRService } from './core';
//import { Observable } from 'rxjs/Observable';

const SignalRServiceMock = {
  start() {
    // Mock method
  }
};
export class ApiServiceMock {
  startTrackPage() {
  }
  stopTrackPage() {
  }
}
const routerMock = {
  //events: Observable.of({})
};

describe('AppComponent', () => {
  let startSpy: any;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        // { provide: SignalRService, useValue: SignalRServiceMock },
        //{ provide: ApiService, useClass: ApiServiceMock }
        { provide: AdalService, useClass: AdalServiceMock }
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    //const service = fixture.debugElement.injector.get(SignalRService);
    //startSpy = spyOn(service, 'start');
    expect(app).toBeTruthy();
    // expect(startSpy).toHaveBeenCalled();
  }));
});
